#!/usr/bin/env python

__author__ = 'ian'

import sys
import os
import subprocess
import time

import gtk

this_dir = os.path.dirname(__file__)


class Gui:
    def on_window1_destroy(self, widget, data=None):
        gtk.main_quit()

    def on_name_entry_editing_done(self, widget, data=None):
        self.state["project_name"] = widget.get_text()
        self.statusbar.push(self.statusbar.get_context_id('Entries'), 'Project name: %s' % self.state["project_name"])
        self.menu_saveas.set_sensitive(True)
        self.check_ready_to_execute()

    def on_config_filechooserbutton_file_set(self, widget, data=None):
        self.state["config_file"] = widget.get_filename()
        self.statusbar.push(self.statusbar.get_context_id('Entries'),
                            'Configuration filename: %s' % self.state["config_file"])
        self.load_config_file()
        self.display_configuration()
        self.check_ready_to_execute()

    def on_mapped_filechooserbutton_file_set(self, widget, data=None):
        self.state["rna_seq_mapping_dir"] = widget.get_filename()
        self.statusbar.push(self.statusbar.get_context_id('Entries'),
                            'RNA-Seq mapping directory: %s' % self.state["rna_seq_mapping_dir"])
        self.check_ready_to_execute()

    def on_transcript_filechooserbutton_file_set(self, widget, data=None):
        self.state["transcripts_file"] = widget.get_filename()
        self.statusbar.push(self.statusbar.get_context_id('Entries'),
                            'Transcripts filename: %s' % self.state["transcripts_file"])
        self.check_ready_to_execute()

    def on_rna_filechooserbutton_file_set(self, widget, data=None):
        self.state["rna_reads_file"] = widget.get_filename()
        self.statusbar.push(self.statusbar.get_context_id('Entries'),
                            'RNA-Seq reads filename: %s' % self.state["rna_reads_file"])
        self.check_ready_to_execute()

    def on_masked_filechooserbutton_file_set(self, widget, data=None):
        self.state["masked_genome_file"] = widget.get_filename()
        self.statusbar.push(self.statusbar.get_context_id('Entries'),
                            'Masked genome filename: %s' % self.state["masked_genome_file"])
        self.check_ready_to_execute()

    def on_genome_filechooserbutton_file_set(self, widget, data=None):
        self.state["genome_file"] = widget.get_filename()
        self.statusbar.push(self.statusbar.get_context_id('Entries'), 'Genome filename: %s' % self.state["genome_file"])
        self.genome_dir = os.path.dirname(self.state["genome_file"])
        self.masked_filechooserbutton.set_current_folder(self.genome_dir)
        self.mapped_filechooserbutton.set_current_folder(self.genome_dir)
        self.transcript_filechooserbutton.set_current_folder(self.genome_dir)
        self.rna_filechooserbutton.set_current_folder(self.genome_dir)
        self.check_ready_to_execute()

    def load_config_file(self):
        self.state["config"] = {}
        try:
            input_file = open(self.state["config_file"])
            for line in input_file:
                try:
                    comment = line.find('#')
                    if comment > -1:
                        line = line[:comment]
                    fields = line.strip().split('=')
                    if len(fields) == 2:
                        self.state["config"][fields[0].strip()] = fields[1].strip()
                except Exception, e:
                    self.statusbar.push(self.statusbar.get_context_id('Errors'),
                                        'Malformed configuration line: %s' % line.strip())
        except IOError, ioe:
            self.statusbar.push(self.statusbar.get_context_id('Errors'),
                                'Configuration file %s could not be read' % self.state["config_file"])

    def display_configuration(self):
        self.config_values_label.show()
        for i, (key, value) in enumerate(self.state["config"].items()):
            if i < len(self.config_labels):
                self.config_labels[i].set_text(key)
                self.config_labels[i].show()
                self.config_entries[i].set_text(value)
                self.config_entries[i].show()

    def blank_configuration(self):
        for i in range(25):
            self.config_labels[i].set_text('')
            self.config_labels[i].hide()
            self.config_entries[i].set_text('')
            self.config_entries[i].hide()
        self.config_values_label.hide()

    def update_config_value(self, i, value):
        if i < len(self.config_labels):
            key = self.config_labels[i].get_text()
            self.state["config"][key] = value
            self.statusbar.push(self.statusbar.get_context_id('Entries'),
                                'self.state["config"][%s] = %s' % (key, value))


    def on_config_entry1_activate(self, widget, data=None):
        i = 0
        self.update_config_value(i, widget.get_text())

    def on_config_entry2_activate(self, widget, data=None):
        i = 1
        self.update_config_value(i, widget.get_text())

    def on_config_entry3_activate(self, widget, data=None):
        i = 2
        self.update_config_value(i, widget.get_text())

    def on_config_entry4_activate(self, widget, data=None):
        i = 3
        self.update_config_value(i, widget.get_text())

    def on_config_entry5_activate(self, widget, data=None):
        i = 4
        self.update_config_value(i, widget.get_text())

    def on_config_entry6_activate(self, widget, data=None):
        i = 5
        self.update_config_value(i, widget.get_text())

    def on_config_entry7_activate(self, widget, data=None):
        i = 6
        self.update_config_value(i, widget.get_text())

    def on_config_entry8_activate(self, widget, data=None):
        i = 7
        self.update_config_value(i, widget.get_text())

    def on_config_entry9_activate(self, widget, data=None):
        i = 8
        self.update_config_value(i, widget.get_text())

    def on_config_entry10_activate(self, widget, data=None):
        i = 9
        self.update_config_value(i, widget.get_text())

    def on_config_entry11_activate(self, widget, data=None):
        i = 10
        self.update_config_value(i, widget.get_text())

    def on_config_entry12_activate(self, widget, data=None):
        i = 11
        self.update_config_value(i, widget.get_text())

    def on_config_entry13_activate(self, widget, data=None):
        i = 12
        self.update_config_value(i, widget.get_text())

    def on_config_entry14_activate(self, widget, data=None):
        i = 13
        self.update_config_value(i, widget.get_text())

    def on_config_entry15_activate(self, widget, data=None):
        i = 14
        self.update_config_value(i, widget.get_text())

    def on_config_entry16_activate(self, widget, data=None):
        i = 15
        self.update_config_value(i, widget.get_text())

    def on_config_entry17_activate(self, widget, data=None):
        i = 16
        self.update_config_value(i, widget.get_text())

    def on_config_entry18_activate(self, widget, data=None):
        i = 17
        self.update_config_value(i, widget.get_text())

    def on_config_entry19_activate(self, widget, data=None):
        i = 18
        self.update_config_value(i, widget.get_text())

    def on_config_entry20_activate(self, widget, data=None):
        i = 19
        self.update_config_value(i, widget.get_text())

    def on_config_entry21_activate(self, widget, data=None):
        i = 20
        self.update_config_value(i, widget.get_text())

    def on_config_entry22_activate(self, widget, data=None):
        i = 21
        self.update_config_value(i, widget.get_text())

    def on_config_entry23_activate(self, widget, data=None):
        i = 22
        self.update_config_value(i, widget.get_text())

    def on_config_entry24_activate(self, widget, data=None):
        i = 23
        self.update_config_value(i, widget.get_text())

    def on_config_entry25_activate(self, widget, data=None):
        i = 24
        self.update_config_value(i, widget.get_text())

    def on_changed_combobox1(self, combo, data=None):
        tree_iter = combo.get_active_iter()
        if tree_iter != None:
            model = combo.get_model()
            name = model[tree_iter][0]
            self.state["blast_method"] = name
            self.statusbar.push(self.statusbar.get_context_id('Entries'),
                                'BLAST search method: %s' % self.state["blast_method"])
        self.check_ready_to_execute()

    def on_output_filechooserbutton_file_set(self, widget, data=None):
        self.state["output_dir"] = widget.get_filename()
        self.statusbar.push(self.statusbar.get_context_id('Entries'), 'Output directory: %s' % self.state["output_dir"])
        self.check_ready_to_execute()
        self.menu_clear.set_sensitive(True)

    def on_activate_menu_quit(self, widget, data=None):
        gtk.main_quit()

    def on_activate_menu_saveas(self, widget, data=None):
        dialog = gtk.FileChooserDialog("Please choose a file", None,
                                       gtk.FILE_CHOOSER_ACTION_SAVE,
                                       (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                        gtk.STOCK_SAVE_AS, gtk.RESPONSE_OK))

        self.add_filters(dialog)

        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            self.save_filename = dialog.get_filename()
        dialog.destroy()

        self.save_state()
        self.menu_save.set_sensitive(True)

    def save_state(self):
        if self.save_filename:
            save_file = open(self.save_filename, 'w')
            # save the project state
            for key in self.state.keys():
                if key == "config":
                    continue
                print >> save_file, '%s=%s' % (key, self.state[key])
            print >> save_file, "#Configuration"
            if self.state.has_key("config"):
                config_dict = self.state["config"]
                for key in config_dict.keys():
                    print >> save_file, '%s=%s' % (key, config_dict[key])
            save_file.close()
            self.statusbar.push(self.statusbar.get_context_id('Entries'), 'Project saved to %s' % self.save_filename)
        else:
            self.statusbar.push(self.statusbar.get_context_id('Errors'),
                                "Can't save project until a filename is selected!")

    def load_state(self):
        if self.load_filename:
            input_file = open(self.load_filename)
            # load project state
            in_config = False
            for line in input_file:
                if line.startswith("#Configuration"):
                    in_config = True
                    self.state["config"] = {}
                else:
                    key, value = line.strip().split('=')[:2]
                    if in_config:
                        self.state["config"][key] = value
                    else:
                        self.state[key] = value
            input_file.close()
            self.statusbar.push(self.statusbar.get_context_id('Entries'), 'Project loaded from %s' % self.load_filename)
        else:
            self.statusbar.push(self.statusbar.get_context_id('Errors'),
                                "Can't load project until a filename is selected!")
        self.check_ready_to_execute()


    def add_filters(self, dialog):
        filter_proj = gtk.FileFilter()
        filter_proj.set_name("SnowyOwl project files")
        filter_proj.add_pattern("*.SOproj")
        dialog.add_filter(filter_proj)

        filter_any = gtk.FileFilter()
        filter_any.set_name("Any files")
        filter_any.add_pattern("*")
        dialog.add_filter(filter_any)


    def on_activate_menu_save(self, widget, data=None):
        self.save_state()

    def on_activate_menu_open(self, widget, data=None):
        dialog = gtk.FileChooserDialog("Please choose a file", None,
                                       gtk.FILE_CHOOSER_ACTION_OPEN,
                                       (gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL,
                                        gtk.STOCK_OPEN, gtk.RESPONSE_OK))

        self.add_filters(dialog)

        response = dialog.run()
        if response == gtk.RESPONSE_OK:
            self.load_filename = dialog.get_filename()
        dialog.destroy()

        self.load_state()
        self.show_state()
        self.save_filename = self.load_filename
        self.menu_save.set_sensitive(True)
        self.menu_saveas.set_sensitive(True)
        self.menu_clear.set_sensitive(True)

    def on_activate_menu_new(self, widget, data=None):
        self.clear()
        self.show_state()
        self.menu_save.set_sensitive(False)
        self.menu_saveas.set_sensitive(False)

    def on_activate_menu_clear(self, widget, data=None):
        cmd_list = [os.path.join(this_dir, 'snowyowl.pl')]
        cmd_list.extend(['-p', self.state['project_name']])
        cmd_list.extend(['-outdir', self.state['output_dir']])
        cmd_list.append('-clean')
        print ' '.join(cmd_list)
        parent_dir = os.path.dirname(self.state['output_dir'])
        progress = open(os.path.join(parent_dir, 'snowyowl.clean.stdout'), 'w')
        errors = open(os.path.join(parent_dir, 'snowyowl.clean.stderr'), 'w')
        self.statusbar.push(self.statusbar.get_context_id('Events'), "Cleaning SnowyOwl project output")
        proc = subprocess.Popen(cmd_list, stdin=subprocess.PIPE, stdout=progress, stderr=errors)
        proc.communicate('Y\n')
        progress.close()
        errors.close()
        self.statusbar.push(self.statusbar.get_context_id('Events'), "SnowyOwl clean has terminated")

    def check_ready_to_execute(self):
        try:
            cmd_list = self.construct_command_list()
            self.menu_execute.set_sensitive(True)
            self.statusbar.push(self.statusbar.get_context_id('Messages'),
                                "Select menu item Run|Execute to start gene prediction")
            return True
        except KeyError, ke:
            self.statusbar.push(self.statusbar.get_context_id('Errors'),
                                "Can't run pipeline because of missing parameter %s!" % str(ke))
            return False


    def construct_command_list(self):
        cmd_list = [os.path.join(this_dir, 'snowyowl.pl')]
        config_dict = self.state["config"]
        cmd_list.extend(['-c', self.state['config_file']])
        cmd_list.extend(['-p', self.state['project_name']])
        cmd_list.extend(['-g', self.state['genome_file']])
        cmd_list.extend(['-r', self.state['rna_reads_file']])
        cmd_list.extend(['-t', self.state['transcripts_file']])
        cmd_list.extend(['-m', self.state['rna_seq_mapping_dir']])
        cmd_list.extend(['-outdir', self.state['output_dir']])
        if self.state['blast_method'] == "Local tera-blast":
            cmd_list.append('-dc')
            for key in ['dc_blastx_targ', 'dc_blastp_targ', 'dcmach', 'dc_local_path']:
                cmd_list.extend(['-' + key, config_dict[key]])
        elif self.state['blast_method'] == "Remote tera-blast":
            cmd_list.append('-dcremote')
            for key in ['dc_blastx_targ', 'dc_blastp_targ', 'dcmach', 'dc_remote_path', 'dcserver', 'dcuser']:
                cmd_list.extend(['-' + key, config_dict[key]])
        else:
            for key in ['bdb', 'hdb', 'blast', 'cd_hit']:
                cmd_list.extend(['-' + key, config_dict[key]])
        for key in ['np', 'imin', 'imax', 'augustus', 'blat', 'exonerate', 'genemark']:
            cmd_list.extend(['-' + key, config_dict[key]])
        return cmd_list

    def on_activate_menu_execute(self, widget, data=None):
        try:
            cmd_list = self.construct_command_list()
        except KeyError, ke:
            self.statusbar.push(self.statusbar.get_context_id('Errors'),
                                "Can't run pipeline because of missing parameter %s!" % str(ke))
            return
        if self.state.has_key('masked_genome_file'):
            cmd_list.extend(['-n', self.state['masked_genome_file']])

        self.statusbar.push(self.statusbar.get_context_id('Events'), "Running SnowyOwl pipeline")
        time.sleep(0.5) # Let the main loop run

        if not os.path.isdir(self.state['output_dir']):
            os.mkdir(self.state['output_dir'])
        main_log = os.path.join(self.state['output_dir'], 'SnowyOwl.log')
        stdout_capture = os.path.join(self.state['output_dir'], 'snowyowl.stdout')
        cmd_list.extend(['&>', stdout_capture])
        cmd = ' '.join(cmd_list)
        print cmd

        try:
            gvim = subprocess.check_output(['/usr/bin/which', 'gvim'])
            monitor_progress = subprocess.Popen([gvim.strip(), '-geometry', '110x52', main_log])
        except subprocess.CalledProcessError:
            pass

        proc = subprocess.Popen(cmd, shell=True)
        retcode = proc.wait()

        if retcode == 0:
            self.statusbar.push(self.statusbar.get_context_id('Events'), "SnowyOwl pipeline completed successfully")
        else:
            self.statusbar.push(self.statusbar.get_context_id('Errors'), "SnowyOwl pipeline failed!")


    def show_state(self):
        if self.state.has_key('project_name'):
            self.name_entry.set_text(self.state['project_name'])
        else:
            self.name_entry.set_text('')
        if self.state.has_key('genome_file'):
            self.genome_filechooserbutton.set_filename(self.state['genome_file'])
        else:
            self.genome_filechooserbutton.set_filename('(None)')
        if self.state.has_key('masked_genome_file'):
            self.masked_filechooserbutton.set_filename(self.state['masked_genome_file'])
        else:
            self.masked_filechooserbutton.set_filename('(None)')
        if self.state.has_key('rna_reads_file'):
            self.rna_filechooserbutton.set_filename(self.state['rna_reads_file'])
        else:
            self.rna_filechooserbutton.set_filename('(None)')
        if self.state.has_key('transcripts_file'):
            self.transcript_filechooserbutton.set_filename(self.state['transcripts_file'])
        else:
            self.transcript_filechooserbutton.set_filename('(None)')
        if self.state.has_key('rna_seq_mapping_dir'):
            self.mapped_filechooserbutton.set_filename(self.state['rna_seq_mapping_dir'])
        else:
            self.mapped_filechooserbutton.set_filename('(None)')
        if self.state.has_key('config_file'):
            self.config_filechooserbutton.set_filename(self.state['config_file'])
        else:
            self.config_filechooserbutton.set_filename('(None)')
        if self.state.has_key('output_dir'):
            self.output_filechooserbutton.set_filename(self.state['output_dir'])
        else:
            self.output_filechooserbutton.set_filename('(None)')
        if self.state.has_key('blast_method'):
            active_index = ["Local non-accelerated", "Local tera-blast", "Remote tera-blast"].index(
                self.state['blast_method'])
        else:
            active_index = -1
        self.blast_combobox.set_active(active_index)
        if self.state.has_key('config'):
            self.display_configuration()
        else:
            self.blank_configuration()

    def on_activate_menu_about(self, widget, data=None):
        self.about_dialog.show()

    def on_aboutdialog_response(self, widget, data=None):
#        print 'aboutdialog response: widget %s, data %s' % (str(widget), str(data))
        if data == -6 :
            self.about_dialog.hide()

    def on_activate_menu_info(self, widget, data=None):
        self.help_dialog.show()

    def on_help_close_button_clicked(self, widget, data=None):
        # print 'help_close_button_clicked: widget %s, data %s' % (str(widget), str(data))
        self.help_dialog.hide()

    def __init__(self):
        builder = gtk.Builder()
        builder.add_from_file(os.path.join(this_dir, "SnowyOwl.glade"))

        self.clear()

        self.window = builder.get_object("window1")
        self.display_scrolledwindow = builder.get_object("display_scrolledwindow")
        self.statusbar = builder.get_object("statusbar")
        self.config_display = builder.get_object("config_display")
        self.name_entry = builder.get_object("name_entry")
        self.genome_filechooserbutton = builder.get_object("genome_filechooserbutton")
        self.masked_filechooserbutton = builder.get_object("masked_filechooserbutton")
        self.rna_filechooserbutton = builder.get_object("rna_filechooserbutton")
        self.transcript_filechooserbutton = builder.get_object("transcript_filechooserbutton")
        self.mapped_filechooserbutton = builder.get_object("mapped_filechooserbutton")
        self.config_filechooserbutton = builder.get_object("config_filechooserbutton")
        self.output_filechooserbutton = builder.get_object("output_filechooserbutton")
        self.config_values_label = builder.get_object("config_values_label")

        # setup blast choices
        self.blast_combobox = builder.get_object("combobox1")
        blast_store = gtk.ListStore(str)
        choices = ["Local non-accelerated", "Local tera-blast", "Remote tera-blast"]
        for choice in choices:
            blast_store.append([choice])
        self.blast_combobox.set_model(blast_store)
        renderer_text = gtk.CellRendererText()
        self.blast_combobox.pack_start(renderer_text, True)
        self.blast_combobox.add_attribute(renderer_text, "text", 0)
        self.state["blast_method"] = choices[0] # default value

        self.menu_open = builder.get_object("menu_open")
        self.menu_save = builder.get_object("menu_save")
        self.menu_saveas = builder.get_object("menu_saveas")
        self.menu_clear = builder.get_object("menu_clear")
        self.menu_execute = builder.get_object("menu_execute")

        self.config_entries = []
        self.config_labels = []
        for i in range(1, 26):
            self.config_entries.append(builder.get_object('config_entry%d' % i))
            self.config_labels.append(builder.get_object('config_label%d' % i))
        self.about_dialog = builder.get_object("aboutdialog")
        self.help_dialog = builder.get_object("help-dialog")
        self.help_text = builder.get_object("help-textbuffer")
        builder.connect_signals(self)

        # pre-load default configuration
        self.defaultCONFIG = os.path.join(this_dir, "CONFIG")
        self.config_filechooserbutton.set_filename(self.defaultCONFIG)
        self.state["config_file"] = self.defaultCONFIG
        self.load_config_file()
        self.display_configuration()


    def clear(self):
        self.load_filename = None
        self.save_filename = None
        self.state = {}


if __name__ == "__main__":
    gui = Gui()
    gui.window.show()
    gtk.main()
