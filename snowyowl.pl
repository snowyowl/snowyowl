#!/usr/bin/perl

# TODO Move files in Merge_files to top level
# TODO Merge main and scoring Config files

# Parallelization by Omar Zabaneh

my ($SO, $AUGUSTUS);

use warnings;
use strict;
use Getopt::Long qw (:config pass_through);
use Benchmark;
use File::Basename;
use File::Path qw(mkpath rmtree);
use File::Spec::Functions;
use Cwd 'abs_path';
use Forks::Super;

# set interrupt handler
$SIG{'INT' } = 'interrupt';  $SIG{'QUIT'} = 'interrupt';
$SIG{'HUP' } = 'interrupt';  $SIG{'TRAP'} = 'interrupt';
$SIG{'ABRT'} = 'interrupt';  $SIG{'STOP'} = 'interrupt';

my $Version = '1.7';

my $here = abs_path( dirname(__FILE__) );
$SO = $here;
$ENV{'SNOWYOWL'} = $SO;


# Configuration file to set defaults
my $config_file = "$here/CONFIG";

# Required arguments
my ($project_name, $genome_file, $rna_reads_file, $transcripts_file, $rna_seq_mapping_dir, $project_dir);

# Optional flags 
my ($clean_project, $decypher_local, $decypher_remote);

# Optional arguments and their default values
my %option2value = ();
my %option2default = ();

# Get arguments and check if all required ones are given
get_and_check_args();


# Set output root directory for the project
my $output_dir = "$ENV{SNOWYOWL}/Projects";
$output_dir = $option2value{outdir} if $option2value{outdir};
mkpath $output_dir unless -d $output_dir;

# Set output directory for the project
if ($option2value{outdir}) {
	$project_dir = $option2value{outdir};
	}
else {
	$project_dir = "$output_dir/$project_name";
	}
system "mkdir $project_dir" unless -d $project_dir;

# Print current argument settings
open(MAIN_LOG, ">$project_dir/SnowyOwl.log" );
echo_args();

# Abused variable storing current command to run
my $cmd;

# Set starting time
my $t0 = Benchmark->new;

# Check sequence formats and convert to FASTA if necessary
print_status("Checking sequence formats and converting to fasta\n", 1);
my ($genome_fasta, $rna_reads_fasta, $transcripts_fasta)
 = convert_to_fasta($genome_file, $rna_reads_file, $transcripts_file);

# If a masked genome sequence file was given, check and covert to FASTA if necessary
my $masked_genome_fasta = "";
if ($option2value{masked_genome_file}) {
  ($masked_genome_fasta) = convert_to_fasta($option2value{masked_genome_file})
}

# Collect all options in a string to give to make_SO_Project
my $setup_options = "";

# DeCypher options 
if ($decypher_local) {
  $setup_options = "-dc";
  $setup_options .= " -dcpath $option2value{dc_local_path}";
}
elsif ($decypher_remote) {
  $setup_options = "-dcremote";
  for my $option ("dcserver", "dcuser", "dcmach") {
    my $flag = $option;
    $flag =~ s/_//g;
    $setup_options .= " -$flag $option2value{$option}";
  }
  $setup_options .= " -dcpath $option2value{dc_remote_path}";
}
else {
  $setup_options = "-hdb $option2value{hdb} -np $option2value{np}";
}

# genome sequence options
$setup_options .= " -g $genome_fasta";
$setup_options .= " -n $masked_genome_fasta" if $masked_genome_fasta;

# Prefix label option
$setup_options .= " -label $option2value{label}" if $option2value{label};

# Project directory option
$setup_options .= " -pdir $project_dir";

# Check that required environment variables SNOWYOWL and AUGUSTUS_CONFIG_PATH 
# are set, create correct directory structures, and copy/customize template files.


$Forks::Super::MAX_PROC = $option2value{np} ;


my $log_file = "$project_dir/make_SO_Project.log";
$cmd = "$SO/make_SO_Project -p $project_name -m $rna_seq_mapping_dir -l $log_file $setup_options";
print "\n";
print_status("Making project\nDetails are in $log_file\n", 1);
open(LOG, ">$log_file");
print LOG "$cmd\n\n";
close(LOG);
#system $cmd;
my $setup_pid = fork { cmd => $cmd, callback =>
  { start => sub { print_status("Starting make_SO_project\nDetails are in $log_file\n", 1); },
    finish => sub { print_status("Finished making project\n", 1); }   },
    on_busy => "queue", queue_priority => 20
};

waitpid( $setup_pid, 0);
unless ( Forks::Super::status($setup_pid) == 0 ) {
  terminate("SnowyOwl ERROR: $0: make_SO_Project failed!\nAborting.\n\n");
};

# Create GeneMark training set.
$cmd = "$SO/make_GeneMark_training -pdir $project_dir";
$log_file = "$project_dir/GeneMark/make_GeneMark_training.log";
$cmd .= " -l $project_dir/GeneMark/make_GeneMark_training.log";
open(LOG3, "> $log_file");
print LOG3 "$cmd\n\n";
close LOG3;
my ( $genemark_stdout, $genemark_stderr );
my $genemark_pid = fork { cmd => $cmd, depend_on => [$setup_pid], callback =>
  { start => sub { print_status("Creating GeneMark training set\nDetails are in $log_file\n", 1); },
    finish => sub { print_status("Finished creating GeneMark training set\n", 1); }  },
   on_busy => "queue", queue_priority => 10
};

# Create contig training set from assembled RNA-Seq reads using Augustus.
my $parallel_blastx = !($decypher_remote || $decypher_local);
$log_file = "$project_dir/Contigs/make_Contig_training.log";
my $contig_pid;
if ($parallel_blastx) {
    # Do parallel_blastx first, followed by parallel blat
    my $blastx_procs = $option2value{np} - 1 ;
    $cmd = "$SO/make_Contig_training -p $project_name -pdir $project_dir -ats $transcripts_fasta -imin $option2value{imin} -imax $option2value{imax} -bdb $option2value{bdb} -bpath $option2value{blast} -np $blastx_procs -l $log_file";
    open(LOG2, ">$log_file");
    print LOG2 "$cmd\n\n";
    my $blastx_pid = fork { cmd => $cmd, depend_on => [$setup_pid], callback =>
  { start => sub { print_status("Running parallel blastx on training contigs\nDetails are in $log_file\n", 1); },
    finish => sub { print_status("Finished running blastx\n", 1); }   },
    on_busy => "queue", queue_priority => 10
    };

   waitpid( $blastx_pid, 0);

   $cmd = "$SO/finish_Contig_training.pl  -p $project_name -pdir $project_dir -l $log_file -trf $project_dir/Contigs/assembled_transcripts.RENAMED -btx $project_dir/Contigs/blast_result.blastx -imin $option2value{imin} -imax $option2value{imax}";
    print LOG2 "$cmd\n\n";
#    my ($contig_stdout, $contig_stderr);
    $contig_pid = fork { cmd => $cmd, depend_on => [$setup_pid], callback =>
        { start => sub { print_status("Creating contig training set\nDetails are in $log_file\n", 1); },
        finish => sub { print_status("Finished creating contig training set\n", 1); }   },
        on_busy => "queue", queue_priority => 10
    };


} else {

    if ($decypher_remote) {
	    $cmd = "$SO/make_Contig_training_dcremote -p $project_name -pdir $project_dir -ats $transcripts_fasta -imin $option2value{imin} -imax $option2value{imax} -dcserver $option2value{dcserver}  -dcuser $option2value{dcuser}  -dcmach $option2value{dcmach}  -dcpath $option2value{dc_remote_path} -dctarg $option2value{dc_blastx_targ}";
    } elsif ($decypher_local) {
        $cmd = "$SO/make_Contig_training_dc -p $project_name -pdir $project_dir -ats $transcripts_fasta -imin $option2value{imin} -imax $option2value{imax} -dcpath $option2value{dc_local_path} -dctarg $option2value{dc_blastx_targ}";
    }
    $cmd .= " -l $project_dir/Contigs/make_Contig_training.log";
    print( "\n");
    open(LOG2, ">$log_file");
    print LOG2 "$cmd\n\n";
#    my ($contig_stdout, $contig_stderr);
    $contig_pid = fork { cmd => $cmd, depend_on => [$setup_pid], callback =>
    { start => sub { print_status("Creating contig training set\nDetails are in $log_file\n", 1); },
        finish => sub { print_status("Finished creating contig training set\n", 1); }   },
        on_busy => "queue", queue_priority => 10
    };
};

# Generate RNA-Seq intron and exon hints.
my $masked_genome_sequence_exists = "";
$masked_genome_sequence_exists = "-n" if $masked_genome_fasta;
my $blat_procs = $option2value{np} - 2 ;
#my ($blat_stdout, $blat_stderr);
$log_file = "$project_dir/AugustusBlatHints/make_Augustus_hints.log";
$cmd = "$SO/make_Augustus_hints -p $project_name -pdir $project_dir -l $log_file -rna $rna_reads_fasta -imax $option2value{imax} -np $blat_procs ";
open(LOG4, ">$log_file");
print LOG4 "$cmd\n\n";
my $hints_pid = fork { cmd => $cmd, depend_on => [$setup_pid], callback =>
  { start => sub { print_status("Generating Augustus hints\nDetails are in $log_file\n", 1); },
   finish => sub { print_status("Finished generating Augustus hints\n", 1); } },
   on_busy => "queue", queue_priority => 8
   };

waitpid( $contig_pid, 0);
#if ($contig_stdout) {print LOG2 "\nStdout:\n$contig_stdout\n"; } ;
#if ($contig_stderr) {print LOG2 "\nStderr:\n$contig_stderr\n"; } ;
close(LOG2);
unless ( Forks::Super::status($contig_pid) == 0 ) {
  terminate("SnowyOwl ERROR: $0: make_Contig_training failed!\nAborting.\n\n");
};

waitpid( $genemark_pid, 0);
unless ( Forks::Super::status($genemark_pid) == 0 ) {
  terminate("SnowyOwl ERROR: $0: make_GeneMark_training failed!\nAborting.\n\n");
};

# Create consensus training set by extracting all identical models from the outputs 
# of make_Contig_training and make_GeneMark_training.
# Also trains Augustus.
$log_file = "$project_dir/make_Consensus_training.log";
$cmd = "$SO/make_Consensus_Training -p $project_name -pdir $project_dir -l $log_file";
open(LOG5, ">$log_file");
print LOG5 "$cmd\n\n";
close(LOG5);
my $consensus_pid = fork { cmd => $cmd, depend_on => [$contig_pid, $genemark_pid], callback =>
  { start => sub { print_status("Creating consensus training set\nDetails are in $log_file\n", 1); },
   finish => sub { print_status("Finished creating consensus training set", 1); } },
  on_busy => "queue", queue_priority => 10
  };

#
# Run all Augustus instances.
#

waitpid( $consensus_pid, 0);
unless ( Forks::Super::status($consensus_pid) == 0 ) {
  terminate("SnowyOwl ERROR: $0: make_Consensus_Training failed!\nAborting.\n\n");
};

my @augustus_pids = ();

if ( -s "$project_dir/Unhinted_Augustus_Models/Unhinted_Augustus_Models.gff3" ) { print "$project_dir/Unhinted_Augustus_Models/Unhinted_Augustus_Models.gff3 already exists\n";}
  else {
    $cmd = "augustus --noInFrameStop=true --uniqueGeneId=true --gff3=on --UTR=off --introns=on --protein=on --genemodel=complete --alternatives-from-sampling=true --sample=200 --minexonintronprob=0.02 --minmeanexonintronprob=0.05 --species=$project_name $project_dir/Sequences/Genome.fasta --outfile=$project_dir/Unhinted_Augustus_Models/Unhinted_Augustus_Models.gff3";
    $log_file = "$project_dir/Unhinted_Augustus_Models/Unhinted_Augustus.log";
#    $cmd .= " -l $log_file";
    open(LOG6, ">$log_file");
    print LOG6 "$cmd\n\n";
    close(LOG6);
    push @augustus_pids, fork { cmd => $cmd, depend_on => [$consensus_pid], callback =>
      { start => sub { print_status("Running Augustus without hints\nDetails are in $log_file\n", 1); },
    finish => sub { print_status("Finished running Augustus without hints", 1); } },
    on_busy => "queue", queue_priority => 5
    };
};

waitpid( $hints_pid, 0);
# if ($blat_stdout) { print LOG4 "\nStdout:\n$blat_stdout\n" ; };
# if ($blat_stderr) { print LOG4 "\nStderr:\n$blat_stderr\n"; };
close(LOG4);
unless ( Forks::Super::status($hints_pid) == 0 ) {
  terminate("SnowyOwl ERROR: $0: make_Augustus_hints failed!\nAborting.\n\n");
};

$log_file = "$project_dir/AugustusBlatHints/Hinted_Augustus.log";
opendir (CHROM_DIR, "$project_dir/Sequences/masked/") or die $!;
my @chrom = readdir(CHROM_DIR);
my ($chrom_fa, $chrom_name, $dirname, $suffix);
my %descriptions = ();
my $description = "";
foreach my $n (0,1,2,3,4,5,6) {
  foreach $chrom_fa (@chrom) {

    if ($chrom_fa =~ /\.fa/) { 
    ($chrom_name, $dirname, $suffix) = fileparse($chrom_fa, qr/\.[^.]*/);
    next unless -e "$project_dir/AugustusBlatHints/hints.$chrom_name.gff";

    if ( -s "$project_dir/AugustusBlatHints/Augustus_BlatHints.$chrom_name.${n}.gff3" ) { print "$project_dir/AugustusBlatHints/Augustus_BlatHints.$chrom_name.${n}.gff3 already exists\n";}
    else {
  $cmd = "augustus --extrinsicCfgFile=$SO/bin/extrinsic.M.RM.E.W.cfg.${n} --alternatives-from-evidence=true --hintsfile=$project_dir/AugustusBlatHints/hints.$chrom_name.gff --allow_hinted_splicesites=atac --species=$project_name --noInFrameStop=true --uniqueGeneId=true --gff3=on --UTR=off --introns=on --protein=on --genemodel=complete --alternatives-from-sampling=true --sample=200 --minexonintronprob=0.02 --minmeanexonintronprob=0.05 $project_dir/Sequences/masked/$chrom_fa --outfile=$project_dir/AugustusBlatHints/Augustus_BlatHints.$chrom_name.${n}.gff3";
#  $cmd .= " -l $log_file";
  open(LOG7, ">>$log_file");
  print LOG7 "$cmd\n\n";
  close(LOG7);
  $description = "$chrom_name with extrinsicCfgFile=extrinsic.M.RM.E.W.cfg.${n}";
  my $pid = fork { cmd => $cmd, depend_on => [$consensus_pid, $hints_pid], callback =>
    { start => sub { my $descr = defined($descriptions{$_[0]}) ? $descriptions{$_[0]} : $description  ; print_status("Running hinted Augustus on $descr\nDetails are in $log_file\n", 1); },
     finish => sub { print_status("Finished running hinted Augustus on $descriptions{$_[0]} ", 1); } },
   on_busy => "queue", queue_priority => 10
    } ;
    $descriptions{ $pid } = $description;
    push @augustus_pids, $pid;
   };
  };
 };
};

# Create pooled models by extracting all unique models from the output of
# run_Augustus_BlatHints and run_Augusutus_Unhinted.

foreach my $excfg (@augustus_pids) {
  waitpid( $excfg, 0);
  unless ( Forks::Super::status($excfg) == 0 ) {
    terminate("SnowyOwl ERROR: $0: Augustus process failed!\nAborting.\n\n");
  };
};

$cmd = "$SO/make_Pooled_models -pdir $project_dir";
$log_file = "$project_dir/make_Pooled_models.log";
$cmd .= " -l $log_file";
open(LOG8, ">$log_file");
print LOG8 "$cmd\n\n";
close(LOG8);
my $pooled_pid = fork { cmd => $cmd, depend_on => \@augustus_pids, callback =>
  { start => sub { print_status("Creating Augustus pooled models\nDetails are in $log_file\n", 1); },
   finish => sub { print_status("Finished creating Augustus pooled models\n", 1); } },
   on_busy => "queue", queue_priority => 10
  };


# Run scoring and selection procedure to identify accepted and imperfect gene predictions

waitpid( $pooled_pid, 0);
unless ( Forks::Super::status($pooled_pid) == 0 ) {
  terminate("SnowyOwl ERROR: $0: make_Pooled_models failed!\nAborting.\n\n");
};

my $msg;
if ($decypher_local) {
  $cmd = "$SO/run_Merge_Scoring -pdir $project_dir -dc";
  $msg = "Running model scoring and selection with local DeCypher boards";
} elsif ($decypher_remote) {
  $cmd = "$SO/run_Merge_Scoring -pdir $project_dir -dcremote";
  $msg = "Running model scoring and selection with remote DeCypher boards";
} else {
  $cmd = "$SO/run_Merge_Scoring -pdir $project_dir";
  $msg = "Running model scoring and selection without hardware acceleration";
}
$log_file = "$project_dir/Merge_files/model_scoring.log";
$cmd .= " -l $log_file";
open(LOG9, ">$log_file");
print LOG9 "$cmd\n\n";
close(LOG9);
my $score_pid = fork { cmd => $cmd, depend_on => $pooled_pid, callback =>
  { start => sub { print_status("$msg\nDetails are in $log_file\n", 1); },
   finish => sub { print_status("Finished model scoring and selection\n", 1); } },
  on_busy => "queue", queue_priority => 10
  };

#
# Wait for all children processes to finish
# Checking the exit status of each
#

waitpid( $score_pid, 0) ;
unless ( Forks::Super::status($score_pid) == 0 ) {
  terminate("SnowyOwl ERROR: $0: Model scoring failed!\nAborting.\n\n");
};
print_status("SnowyOwl has finished successfully\n", 1);
# Print the total processing time taken
print_run_time();
close(MAIN_LOG);

########## SUBROUTINES ##########

sub terminate {
  print STDERR "$_[0]";
  kill 'TERM', -getpgrp
}

sub interrupt {
  my($signal)=@_;
  terminate "Caught Interrupt\: $signal \nNow Exiting\n";
}

sub get_and_check_args {
  # Check for custom CONFIG file
  GetOptions(
  	"c=s"   => \$config_file
  	) ;

  # Set default values of some optional arguments
  set_defaults();
  
  # Get other arguments
  GetOptions(
    "p=s"	=> \$project_name,
    "g=s" 	=> \$genome_file,
    "n=s"	=> \$option2value{masked_genome_file},
    "r=s" 	=> \$rna_reads_file,
    "t=s" 	=> \$transcripts_file,
    "m=s"	=> \$rna_seq_mapping_dir,
    "dc"	=> \$decypher_local,
    "dcremote"	=> \$decypher_remote,
    "dcserver=s"=> \$option2value{dcserver},
    "dcuser=s"	=> \$option2value{dcuser},
    "dcmach=s"	=> \$option2value{dcmach},
    "dc_remote_path=s"	=> \$option2value{dc_remote_path},
    "dc_local_path=s"	=> \$option2value{dc_local_path},
    "dc_blastx_targ=s"	=> \$option2value{dc_blastx_targ},
    "dc_blastp_targ=s"	=> \$option2value{dc_blastp_targ},
    "bdb=s"	=> \$option2value{bdb},
    "hdb=s"	=> \$option2value{hdb},
    "imin=i"	=> \$option2value{imin},
    "imax=i"	=> \$option2value{imax},
    "np=i"	=> \$option2value{np},
    "clean"	=> \$clean_project,
    "outdir=s"	=> \$option2value{outdir},
    "label=s"	=> \$option2value{label},
    "augustus=s"	=> \$option2value{augustus},
    "genemark=s"	=> \$option2value{genemark},
    "blast=s"		=> \$option2value{blast},
    "cd_hit=s"		=> \$option2value{cd_hit},
    "exonerate=s"	=> \$option2value{exonerate},
    "blat=s"		=> \$option2value{blat},
    "python=s"		=> \$option2value{python}
  );

  # Check if all required arguments are present
  ($clean_project || ($project_name && $genome_file && $rna_reads_file && $transcripts_file && $rna_seq_mapping_dir))
    or die_with_usage("Please supply all required arguments.");
  unless ($clean_project) {
    # Check if necessary options are set either as defaults or by command line options
    for my $option ("imin", "imax", "np", "augustus", "genemark", "blast", "exonerate", "blat") {
      unless ($option2value{$option}) {
        die_with_usage("Please set \"$option\" in $config_file or on the command line.");
      }
    }
  } ;
  
  $option2value{label} = $project_name unless $option2value{label};
  $option2value{outdir} = abs_path($option2value{outdir}) if $option2value{outdir};
  
  if ($decypher_remote) {
    for my $option ("dcserver", "dcuser", "dcmach", "dc_remote_path", "dc_blastx_targ", "dc_blastp_targ") {
      unless ($option2value{$option}) {
	die_with_usage("Please set \"$option\" in $config_file or on the command line for remote DeCypher board access.");
      }
    }
  }
  elsif ($decypher_local) {
    for my $option ("dcmach", "dc_local_path", "dc_blastx_targ", "dc_blastp_targ") {
      unless ($option2value{$option}) {
	die_with_usage("Please set \"$option\" in $config_file or on the command line for remote DeCypher board access.");
      }
    }
  }
  else {
    for my $option ("hdb", "bdb", "blast","cd_hit") {
      unless ($option2value{$option}) {
	die_with_usage("Please set \"$option\" in $config_file or on the command line.");
      }
    }
  }

  # Set the path environment variables now that all program paths are set
  $ENV{PATH} = "$ENV{SNOWYOWL}:$option2value{augustus}/scripts:$option2value{augustus}/bin:$option2value{genemark}:$option2value{blast}:$option2value{exonerate}:$option2value{blat}:$option2value{cd_hit}:$ENV{PATH}";
  $ENV{AUGUSTUS_CONFIG_PATH} = "$option2value{augustus}/config";

  # If requested, clean up a project by removing all files generated for it
  if ($clean_project) {
    ($project_name)
      or die_with_usage("Please give the name of the project to clean up.");
    my $project_dir = "$ENV{SNOWYOWL}/Projects/$project_name";
    $project_dir = "$option2value{outdir}" if $option2value{outdir};
#    print "Do you want to remove all project directories and files in $project_dir ? ";
#    my $answer = <>;
#    if ($answer =~ /^[y|Y]/) {
      print_status("Cleaning up the project $project_name\n");
      my @remove_dirs = (
	$project_dir,
	"$ENV{AUGUSTUS_CONFIG_PATH}/species/$project_name",
	"$ENV{AUGUSTUS_CONFIG_PATH}/species/$project_name\_Contig"
      );
      for my $dir (@remove_dirs) {
        if (-d $dir) {
          print_status("Removing the directory $dir");
          rmtree $dir;
        }
      }
#    }
    exit;
  }
}


# Sets the default values specified in the default configuration file
sub set_defaults { 
  # Get the SnowyOwl home directory in which this script is run
	$ENV{SNOWYOWL} = abs_path(dirname(__FILE__));
  # Read defaults configuration file and set specified defaults
  open CONFIG, $config_file
    or die "Can't open the default configuration file CONFIG to read: $!";
  while (<CONFIG>) {
    # Ignore if this line starts with #
    next if /^#/;
    chomp;
    # Strip comment after # and white space in variable/value name string
    $_ =~ s/#.*|\s//g;
    # Set a default value if specified
    my @line = split /=/;
    if (@line == 2) {
      $option2default{$line[0]} = $line[1];
    }
  }
  close CONFIG;

  # If not already set, set the current user in case it's needed for remote DeCypher board access
  $option2default{dcuser} = getlogin() unless $option2default{dcuser};

  # Populate the option hash with defaults set so far
  %option2value = %option2default;
}


# Dies after printing usage
sub die_with_usage {
  my ($message) = @_;

  my %option2default_info;
  for my $option ("dcserver", "dcuser", "dcmach", "dc_local_path", "dc_remote_path", "dc_blastx_targ", "dc_blastp_targ", "bdb", "hdb", "imin", "imax", "np", "augustus", "genemark", "blast", "cd_hit", "exonerate", "blat", "python") {
    $option2default_info{$option} = "[default: ";
    if ($option2default{$option}) {
      $option2default_info{$option} .= "$option2default{$option}]";
    }
    else {
      $option2default_info{$option} .= "unspecified]";
    }
  }

  print STDERR <<USAGE;
Usage: snowyowl.pl <REQUIRED ARGUMENTS> [OPTIONS]

<REQUIRED ARGUMENTS>
  -p Project_name
	Name of the project (usually species abbreviation: e.g. Rhipu1)
  -g Genome_sequence_file
	FASTA file of sequences of the genome
  -r RNA-Seq_reads_file
	FASTA or FASTQ file of RNA-Seq reads
  -t Transcripts_file
	FASTA file of transcripts assembled from RNA-Seq reads
  -m RNA-Seq_mapping_directory
	Directory containing RNA-Seq read mapping junctions and coverage files

[OPTIONS]
  -c Custom configuration file
     The location of a customized CONFIG file, 
     other than the default location $ENV{SNOWYOWL}/CONFIG
  -n Masked_genome_sequence_file
  	FASTA file of the genome sequence with N's masking regions
	where gene predictions are not wanted (used for GeneMark and Augustus)
	[default: no masked genome sequence]
  -label Prefix_label
  	Prefix label for names of predicted genes (e.g. Aspni3p5)
	[default: project name as given by -p]
  -outdir Root_directory
  	Root directory of output files
	[default: $ENV{SNOWYOWL}/Projects]
  -dc
	Use DeCypher boards locally for blast searches
	[default: do not use DeCypher boards]
  -dcremote
	Use DeCypher boards remotely for blast searches
	[default: do not use DeCypher boards]
  -dcserver DeCypher_server
	DeCypher server to access DeCypher boards remotely (ignored for local access)
	$option2default_info{dcserver}
  -dcuser DeCypher_user
	User name on DeCypher server  (ignored for local access) 
	$option2default_info{dcuser}
  -dcmach DeCypher_machine_name
	DeCypher machine name known to server (ignored for local access)
	$option2default_info{dcmach}
  -dc_remote_path DeCypher_path
	DeCypher program path on remote server (ignored for local access)
	$option2default_info{dc_remote_path}
  -dc_local_path DeCypher_path
	DeCypher program path on local server (ignored for remote access)
	$option2default_info{dc_remote_path}
  -dc_blastx_targ DeCypher_target
	Name of target database for DeCypher blastx searches in contig training
	$option2default_info{dc_blastx_targ}
  -dc_blastp_targ DeCypher_target
	Name of target database for DeCypher blastp searches in model scoring
	$option2default_info{dc_blastp_targ}
  -bdb BLAST_DB
	FASTA file of BLAST database to use in contig training
	$option2default_info{bdb}
  -hdb Homology_DB
	FASTA  file of homology database (without .fasta extension) to use
	in homology scoring without Decypher boards
	$option2default_info{hdb}
  -imin Min_intron_size
	Smallest allowed intron size for alignment of transcripts or RNA-Seq reads to genome
	$option2default_info{imin}
  -imax Max_intron_size
	Largest allowed intron size for alignment of transcripts to genome
	$option2default_info{imax}
  -np   Num_threads
	Number of threads to use for BLAST
	$option2default_info{np}
  -clean
	Clean up project by removing all project directories
	(-p, with -outdir if previously used, is the only required argument with -clean)
  -augustus AUGUSTUS_path 
	$option2default_info{augustus}
  -genemark GeneMark_path
	$option2default_info{genemark}
  -blast BLAST_path
	$option2default_info{blast}
  - cd_hit Cd-hit path
    $option2default_info{blast}
  -exonerate Exonerate_path
	$option2default_info{exonerate}
  -blat BLAT_path
	$option2default_info{blat}
  -python Python_path
	$option2default_info{python}

USAGE

  die "$message\n";
}


# Echoes all required and optional arguments 
sub echo_args {
  my $status_string = "Starting Snowy Owl version $Version (process ID: $$)";
  $status_string .= "\n  Project name: $project_name";
  $status_string .= "\n  Genome sequence file: $genome_file";
  $status_string .= "\n  Masked genome sequence file: $option2value{masked_genome_file}" if $option2value{masked_genome_file};
  $status_string .= "\n  RNA-Seq reads file: $rna_reads_file";
  $status_string .= "\n  Transcripts file: $transcripts_file";
  $status_string .= "\n  RNA-Seq mapping directory: $rna_seq_mapping_dir";
  $status_string .= "\n  Configuration file: $config_file";
  $status_string .= "\n  Label: $option2value{label}";
  $status_string .= "\n  Output directory: $project_dir";
  if ($decypher_local) {
    $status_string .= "\n  Use DeCypher boards? yes (local access)";
    $status_string .= "\n    DeCypher path: $option2value{dc_local_path}" if $option2value{dc_local_path};
    $status_string .= "\n    DeCypher blastx target : $option2value{dc_blastx_targ}" if $option2value{dc_blastx_targ};
    $status_string .= "\n    DeCypher blastp target : $option2value{dc_blastp_targ}" if $option2value{dc_blastp_targ};
  }
  elsif ($decypher_remote) {
    $status_string .= "\n  Use DeCypher boards? yes (remote access)";
    $status_string .= "\n    DeCypher user: $option2value{dcuser}";
    $status_string .= "\n    DeCypher server: $option2value{dcserver}" if $option2value{dcserver};
    $status_string .= "\n    DeCypher machine: $option2value{dcmach}" if $option2value{dcmach};
    $status_string .= "\n    DeCypher path: $option2value{dc_remote_path}" if $option2value{dc_remote_path};
    $status_string .= "\n    DeCypher blastx target : $option2value{dc_blastx_targ}" if $option2value{dc_blastx_targ};
    $status_string .= "\n    DeCypher blastp target : $option2value{dc_blastp_targ}" if $option2value{dc_blastp_targ};
  }
  else {
    $status_string .= "\n  Use Decypher boards?: no";
  $status_string .= "\n  BLAST DB: $option2value{bdb}";
  $status_string .= "\n  Homology DB: $option2value{hdb}";
  $status_string .= "\n  BLAST path: $option2value{blast}";
  $status_string .= "\n  Cd-hit path: $option2value{cd_hit}";
  }
  $status_string .= "\n  Min intron size: $option2value{imin}";
  $status_string .= "\n  Max intron size: $option2value{imax}";
  $status_string .= "\n  Number of processes: $option2value{np}";
  $status_string .= "\n  AUGUSTUS path: $option2value{augustus}";
  $status_string .= "\n  GeneMark path: $option2value{genemark}";
  $status_string .= "\n  Exonerate path: $option2value{exonerate}";
  $status_string .= "\n  BLAT path: $option2value{blat}";
  $status_string .= "\n  Python path: $option2value{python}" if $option2value{python};

  print_status($status_string);
}


# Prints the time taken to run this project
sub print_run_time {
  print MAIN_LOG "\nSnowyOwl for $project_name took: ",
    timestr(timediff(Benchmark->new, $t0)), "\n";
}

# Prints the current status of the pipeline, preceded by the current time
sub print_status {
  my ($message, $timestamp) = @_;
  if ($timestamp) {
    print MAIN_LOG "\n[", get_time_string(), "] $message\n";
  }
  else {
    print MAIN_LOG "$message\n";
  }
}

# Sets the current human-readable time string 
sub get_time_string {
  my @months = qw(Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec);
  my @days = qw(Sun Mon Tue Wed Thu Fri Sat Sun);
  my ($sec, $min, $hour, $mday, $mon, $years_since_1900, $wday, $yday, $isdst) = gmtime;
  my $year = $years_since_1900 + 1900;
  my $tzone = "GMT";
  return sprintf("%s %s %d %02d:%02d:%02d %s %d", $days[$wday], $months[$mon], $mday, $hour, $min, $sec, $tzone, $year);
}


sub convert_to_fasta {
  my (@seq_files) = @_;
  my @fastas = ();
  my $cmd;
  my $fastaname;
  for my $seq_file (@seq_files) {
    my ($base, $path, $ext) = fileparse($seq_file, '\.[^.]+$');
    $fastaname = "$path$base.fa";
    if ($ext eq ".gz") {
      ($base, $path, $ext) = fileparse("$path$base", '\.[^.]+$');
      $fastaname = "$path$base.fa";
      if ( -s $fastaname ) {
        push @fastas, $fastaname;
      } else {
          if (is_fastq_extension($ext)) {
            $cmd = "gunzip -c $seq_file |$SO/bin/fq_all2std.pl fq2fa > $path$base.fa";
            push @fastas, "$path$base.fa";
          }
          elsif (is_fasta_extension($ext)) {
            $cmd = "gunzip -c $seq_file > $path$base$ext";
            push @fastas, "$path$base$ext";
          }
          else {
            $cmd = "gunzip -c $seq_file > $path$base$ext";
            push @fastas, "$path$base$ext";
          }
          print_status($cmd);
          system $cmd;
          unless ( $? == 0 ) {
            die "SnowyOwl ERROR: $0: $cmd failed!\n"
          };
      }
    }
    else {
      if (is_fastq_extension($ext)) {
          unless ( -s $fastaname ) {
            $cmd = "$SO/bin/fq_all2std.pl fq2fa $path$base$ext > $path$base.fa";
            print_status($cmd, 1);
            system $cmd;
            unless ( $? == 0 ) {
                die "SnowyOwl ERROR: $0: $cmd failed!\n"
            };
        }
        push @fastas, "$path$base.fa";
      }
      elsif (is_fasta_extension($ext)) {
        push @fastas, $seq_file;
      }
      else {
	    push @fastas, $seq_file;
      }
    }
  }
  return @fastas;
}


sub is_fasta_extension {
  my ($ext) = @_;
  if ($ext eq ".fa" || $ext eq ".fasta" || $ext eq ".fna" || $ext eq ".fas") {
    return 1;
  }
  else {
    return 0;
  }
}


sub is_fastq_extension {
  my ($ext) = @_;
  if ($ext eq ".fq" || $ext eq ".fastq" || $ext eq ".faq") {
    return 1;
  }
  else {
    return 0;
  }
}
