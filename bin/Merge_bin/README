                 GENE PREDICTION PIPELINE version 5

This set of bash and Python scripts implements the last stage of a pipeline that
predicts genes in fungal genomes. Given sets of GeneMark and Augustus Pooled gene models
generated in earlier pipeline stages and a set of read islands from RNA-Seq read 
mapping, it will produce a merged and selected set of the best gene predictions.

PREREQUISITES

A standard UNIX environment, with bash, sed, and sort is required. 
A recent version of Python 2 is needed; the package has been tested with
versions 2.6 and 2.7. 
The non-standard Python packages 
Biopython (http://biopython.org/wiki/Download), 
paramiko (http://www.lag.net/paramiko/), 
and pysam 0.5 (http://code.google.com/p/pysam/downloads/list), 
and the program suites 
Augustus (http://augustus.gobics.de/),
and tabix and SAMtools (both from http://samtools.sourceforge.net/)
should be installed.

samtools should be on your PATH.

You will also need an account on a server with TimeLogic hardware for accelerated searching. 
Copy your ssh public key onto the server so that you can login without a password prompt.



INSTALLATION

Extract the archive in a convenient location, and edit
pipeline/scripts/run_predict_genes.sh.template to make the variable PIPELINE
point to that location. Also adjust the AUGUSTUS variable to point to your local installation 
of Augustus.

Edit the file $PIPELINE/config/settings.config to contain information about your TimeLogic server,
and adjust the default parameters therein to your liking. If you wish, you can create a personal copy of the settings.config file and adjust the 'source $PIPELINE/config/settings.config' line in run_predict_genes.sh.template to point to it.

Then make pipeline/scripts/run_predict_genes.sh.template readonly.

Create a subdirectory blast in your TimeLogic server home directory and copy 
pipeline/config/tera-blastp.refseq into it.



USAGE

The pipeline needs several input files:

	Normally provided by earlier stages of the pipeline run in Calgary:
1.		A genome sequence in (multi-)fasta format
2.		A GeneMark predictions file in .gff or .gff3 format
3.		An Augustus Pooled predictions file in .gff or .gff3 format

4.	A Read-islands file in gff3 format (or a read-transcripts.gff3 file); these 
files will be generated from inputs 5 and 6 if they do not exist.
5.	A classified.juncs.gz file of intron splice junctions and its tabix index
classified.juncs.gz.tbi
6.	A tuque.coverage.wig.gz read coverage file and its tabix index tuque.coverage.wig.gz.tbi
(5 and 6 are products of splice junction discovery and read mapping with the 
Tuque package. If needed, they can be generated from mapped RNA-Seq reads with 
the auxiliary script BAM_to_juncs_and_coverage.sh; see below.)

	Needed by Augustus:
	(In the following, AUGUSTUS stands for the Augustus installation directory.)
7.		An initialized subdirectory in AUGUSTUS/config/species that contains 
training files for your target genome. Normally a suitable directory will be 
available from the earlier pipeline stages; if not, you can use a preinstalled 
directory for a closely related genome, or create a new directory with the script
AUGUSTUS/scripts/new_species.pl. The variable stopCodonExcludedFromCDS in the 
file AUGUSTUS/config/species/species_name/species_name_parameters.cfg should be 
set to false.
8.		An Augustus extrinsic configuration file; a reasonable default is
provided in the config subdirectory of this package.
		For comprehensive information on Augustus, see 
http://augustus.gobics.de/binaries/README.TXT

Edit a copy of pipeline/scripts/run_predict_genes.sh.template and change the 
variable values to match the genome and input files you wish to use.  Save the 
edited copy in the genome directory as e.g. run_predict_genes.sh, and then execute 
it. The script will create a subdirectory named SnowyOwl, set up the input 
files, and then call the workhorse script pipeline/scripts/predict_genes.sh. The 
output predictions representatives.gff3, accepted.gff3, and the predicted CDS 
and protein sequences will be put in Snowy_Owl/Predictions.

Pipeline progress is recorded in Snowy_Owl/predict_genes.log and errors are 
logged to Snowy_Owl/stderr.log. If the pipeline terminates without producing 
the expected output files, look at the end of stderr.log for an error message. 
Missing files are the most likely causes of errors.

The script is designed to make use of already-generated intermediate files if it
needs to be restarted. This means that any incomplete or erroneous files should
be deleted if the script crashes, before restarting.

AUXILIARY SCRIPTS

pipeline/scripts/BAM_to_juncs_and_coverage.sh reads.bam genome.fasta
can be used to generate the needed classified.juncs.gz and tuque.coverage.wig.gz files
from a set of mapped RNA-Seq reads in BAM format. The program 'wiggles'
(distributed with TopHat) must be on the PATH.

pipeline/scripts/map_training_CDS.sh training_CDS.fa genome_sequence.fa maps 
assembled read contigs to the genome for use in training Augustus. The 
program 'exonerate' must be on the path.

pipeline/scripts/combine_new_and_old_predictions.sh  old.representatives.gff3 \
new.representatives.gff3  start_num  genome.fasta
can be used to conservatively merge new predictions with existing predictions,
preserving the names of any existing models that are the same as new models. It
produces a non-redundant combined set of old and new models, and a list of old
models that have been replaced, along with their replacements.

pipeline/scripts/get_accepted_models.sh  models.gff3  genome.fasta  RNA_Seq_dir
can be used to filter imperfect models from a set of gene models. It will 
output a file named accepted.gff3 and a list of the frequencies of 
various flaws in the input models.

