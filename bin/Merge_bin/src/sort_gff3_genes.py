'''Sort the genes and their sub-parts in a gff3 file in increasing order of seqID and start coordinate

Created on Jul 26, 2010
@author: ian
'''
import sys, os
from gff3Iterator import GFF3Iterator

def usage():
    print 'Usage: python %s input.gff3 output.gff3' % sys.argv[0]
    sys.exit(1)

if __name__ == '__main__':
    try:
        input = sys.stdin if sys.argv[1] == '-' else open(sys.argv[1])
        output = sys.stdout if sys.argv[2] == '-' else open(sys.argv[2], 'w')
    except IndexError:
        usage()
    
    genes = []
    for gene in GFF3Iterator(input).genes():
        sort_key = '%s!%010d%010d' % (gene.get_seqID(), gene.get_start(),  gene.get_end())
        genes.append((sort_key, gene))
    genes.sort()
    for t in genes:
        gene = t[1]
        for transcript in gene.get_transcripts():
            exons = sorted(transcript.get_exons())
            for i, exon in enumerate(exons):
                transcript.add_exon(exon)
        print >> output, t[1]
    if sys.argv[2] != '-':
        output.close()
#    print >> sys.stderr,  sys.argv[0], 'done.'
