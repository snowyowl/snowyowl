"""Run local blastp searches

Created: 2011-12-15
Author: ian
"""

import sys, os,  re,  time,  subprocess,  socket

#LOGFILE = 'run_blastp.log'

def usage(msg=None):
    if msg:
        print >> sys.stderr,  msg
    print >> sys.stderr,  'Usage: python %s  input  output target_database num_threads log_file' % sys.argv[0]
    sys.exit(1)
    
if __name__ == '__main__':
    try:
        input = sys.argv[1]
        output = sys.argv[2]
        target  = sys.argv[3]
        num_threads = sys.argv[4]
        LOGFILE = sys.argv[5]
    except Exception,  e:
        usage(e)
    
    now = time.ctime()
    log = open(LOGFILE,  'a')
    print >> log,  'IN  %s %s -> %s' % (now,  sys.argv[1], sys.argv[3])
    log.close()

    try:
        blastp = subprocess.Popen(['/usr/bin/which', 'blastp'], stdout=subprocess.PIPE, stderr=subprocess.PIPE).communicate()[0].strip()
#        print blastp
        if not os.path.isfile(blastp):
            raise ValueError('Failed to find blastp')
    except Exception, e:
        print sys.stderr, e
        sys.exit(2)
        
    cmd_list = [ blastp, '-query', input, '-db', target, '-out', output, '-evalue', '1e-50', '-outfmt', '6 qseqid ppos sseqid sacc evalue nident positive length gaps qstart qend sstart send bitscore' , '-max_target_seqs', '1', '-num_threads', num_threads ]
    retcode = subprocess.call(cmd_list)
    if retcode == 0:
            now = time.ctime()
            log = open(LOGFILE,  'a')
            print >> log,  'OUT  %s %s' % (now,  sys.argv[2])
            log.close()

    sys.exit(retcode)
    
