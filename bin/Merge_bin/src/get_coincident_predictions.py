"""Find gene predictions from 2 or more predictors that agree exactly with one another

Created: 2010-12-13
Author: ian
"""

import sys, os,  re,  itertools
from collections import defaultdict
import pysam
from gff3Record import GFF3Gene
from gff3Iterator import GFF3Iterator
import argparse

DESCRIPTION = 'Find gene predictions from 2 or more predictors that agree exactly with one another'
VERSION = '5.0'

def are_alternate_start_forms(CDS1,  CDS2):
    if CDS1.get_seqID() != CDS2.get_seqID() or CDS1.get_CDS_stop() != CDS2.get_CDS_stop() or CDS1.get_strand() != CDS2.get_strand() :
        return False
    exons1 = list(sorted(CDS1.get_exons()))
    exons2 = list(sorted(CDS2.get_exons()))
    common = min(len(exons1),  len(exons2))
    if CDS1.get_strand() == '+':
        exons1.reverse()
        exons2.reverse()
        for exon1,  exon2 in zip(exons1[:common-1],  exons2[:common-1]):
            if exon1 != exon2 :
                return False
        if exons1[common-1].get_end() != exons2[common-1].get_end():
            return False
        if len(exons1) > common and exons1[common-1].get_start() > exons2[common-1].get_start():
            return False
        if len(exons2) > common and exons2[common-1].get_start() > exons1[common-1].get_start():
            return False
    else:
        for exon1,  exon2 in zip(exons1[:common-1],  exons2[:common-1]):
            if exon1 != exon2 :
                return False
        if exons1[common-1].get_start() != exons2[common-1].get_start():
            return False
        if len(exons1) > common and exons1[common-1].get_end() < exons2[common-1].get_end():
            return False
        if len(exons2) > common and exons2[common-1].get_end() < exons1[common-1].get_end():
            return False
    return True

def do_get_coincident_predictions(master,  accepted, mismatches,  others, args):
    """Output gene predictions in master that are also in others

    Args:
    master is an iterator over GFF3Gene objects
    accepted and mismatches are writable file-like objects
    others is an iterator over pysam.Tabixfiles that contain prediction from other predictors
    Returns:
        coincident_count - the number of predictions that are common to master and others
        candidate_count - the total number of predictions in master
    """
    overlap_count = coincidence_count = candidate_count = alternate_start_count = 0
    for gene in master:
        if len(gene.get_transcripts()) < 1:
            continue
        transcript = gene.get_transcripts()[0]
        region = '%s:%d-%d' % (gene.get_seqID(),  gene.get_start(),  gene.get_end())
        coincidence = True
        alternatively_started = True
        candidate_count += 1
        max_overlap = 0
        for predictor in others:
            coincident = False
            alternate_starts = False
            try:
                predictions = GFF3Iterator(predictor.fetch(region)).genes()
                for prediction in predictions:
                    for pred_CDS in prediction.get_transcripts():
#                    pred_CDS =  prediction.get_transcripts()[0]
                        if pred_CDS.CDS_matches(transcript):
                            coincident = True
                            max_overlap = 1.0
                            break
                        else:
                            if are_alternate_start_forms(pred_CDS,  transcript):
                                alternate_starts = True
                            overlap = float(min(transcript.get_end(),  pred_CDS.get_end()) - max(transcript.get_start(),  pred_CDS.get_start())) / max( len(transcript),  len(pred_CDS))
                            max_overlap = max(max_overlap,  overlap)
            except:
                pass
            coincidence = coincidence and coincident
            alternatively_started = alternatively_started and alternate_starts
        if max_overlap >= args.overlap:
            overlap_count += 1
        if coincidence:
            coincidence_count += 1
            print >> accepted,  gene
        elif alternatively_started:
            alternate_start_count += 1
            gene.add_attribute('alternate_start',  'True')
            print >> accepted,  gene
        else:
            gene.add_attribute('overlap',  max_overlap)
            print >> mismatches,  gene
    return coincidence_count,  candidate_count,  overlap_count,  alternate_start_count

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in',  '-i', dest='input',  type=argparse.FileType('r'),  nargs='?',  default = sys.stdin,  help='Path to the master input file of gene models in gff3 format; if omitted or -, input is read from stdin')
    argparser.add_argument('--matches',  '-m', required=True,  help='Path to the output file of matching gene models; required')
    argparser.add_argument('--query',  '-q', nargs='+',  help='Path to a tabix-indexed query file of gene models to be matched; at least one required; may be repeated')
    argparser.add_argument('--overlap', type=float,  default=0.98,  help='Models that overlap by at least this fraction will be counted')
    return  argparser.parse_args()

if __name__ == '__main__':
    args = get_args()
    master = GFF3Iterator(args.input).genes()
    accepted = open(args.matches,  'w')
    others = []
    for predictor in args.query:
        others.append(pysam.Tabixfile(predictor))
    mismatch_filename = '.'.join([os.path.splitext(args.matches)[0],  'mismatch', os.path.splitext(args.matches)[1][1:] ])
    mismatches = open(mismatch_filename,  'w')

    coincidence_count,  candidate_count,  overlap_count,  alternate_start_count = do_get_coincident_predictions(master,  accepted, mismatches, others,  args)
    accepted.close()
    mismatches.close()
    print coincidence_count,  '/',  candidate_count,  'genes were predicted by all predictors.'
    print alternate_start_count ,  '/',  candidate_count,  'genes had alternative start forms predicted by all predictors.'
    print overlap_count,  '/',  candidate_count,  'genes were overlapped >=%d%% by a prediction.' % int(args.overlap * 100)
    print 'Mismatched gene models were saved to',  mismatch_filename
#    print sys.argv[0],  'done.'
