""" Merge 2 or more sets of gene models and try to select the best one of overlapping models

Created: 2010-12-22
Author: ian
"""
import sys, os, re
from gff3Record import GFF3Gene
from gff3Iterator import GFF3Iterator
import argparse

DESCRIPTION = 'Merge 2 or more sets of gene models and try to select the best one of overlapping models'
VERSION = '5.0'

scaffold_num_pat = re.compile(r'(\D+)(\d+)')

class Sequence_node(object):
    def __init__(self, model, pseudoscore):
        object.__init__(self)
        self.model = model
        model_len = 0
        for exon in model.get_transcripts()[0].get_exons():
            model_len += len(exon)
        self.score = (float(model.get_score()) + pseudoscore) * model_len
        self.path_score = self.score # correct value for node with no predecessor
        self. predecessor = None

    def __repr__(self):
        return 'Sequence_node(%s), %d-%d, %f, %f' % (self.model.get_ID()[-10:],  self.get_start(), self.get_end(),   self.score,  self.path_score)

    def get_score(self):
        return self.score

    def get_path_score(self):
        return self.path_score

    def set_path_score(self,  path_score):
        self.path_score = path_score

    def get_model(self):
        return self.model

    def get_end(self):
        return self.model.get_end()

    def get_start(self):
        return self.model.get_start()

    def set_predecessor(self,  node):
        self.predecessor = node

    def get_predecessor(self):
        return self.predecessor

    def has_predecessor(self):
        return not (self.predecessor is None)


def cmp_seq_IDs(id1, id2):
    """Arrange that scaffold_9 < scaffold_10 """
    match1 = scaffold_num_pat.match(id1)
    if match1:
        match2 = scaffold_num_pat.match(id2)
        if match2 and match2.group(1) == match1.group(1):
             return cmp(int(match1.group(2)), int(match2.group(2)))
    return cmp(id1, id2)

def sum_scores(gene1,  gene2):
    try:
        gene1.set_score(float(gene1.get_score()) + float(gene2.get_score()))
        for t1,  t2 in zip(gene1.get_transcripts(),  gene2.get_transcripts()):
            t1.set_score(float(t1.get_score()) + float(t2.get_score()))
            for x1,  x2 in zip(t1.get_exons(),  t2.get_exons()):
                x1.set_score(float(x1.get_score()) + float(x2.get_score()))
    except ValueError:
        pass
    return gene1

def find_heaviest_path(sorted_models,  args):
    '''sorted_models should be in non-decreasing order of start position'''
    if len(sorted_models) < 2:
        return sorted_models
    ranked_nodes = [] # sorted 1st by increasing score and 2nd by decreasing start point
    curr_start = sorted_models[-1].get_start()

    # process models in reverse order so that starts are non-increasing
    for m in reversed(sorted_models):
        if m.get_start() > curr_start:
            raise ValueError('Models out of order, %d > %d' % (m.get_start(),  curr_start))
        curr_start = m.get_start()
        node = Sequence_node(m,  args.pseudoscore)
        for r in reversed(ranked_nodes):
            if r.get_start() > node.get_end(): # highest-scoring predecessor by construction
                node.set_path_score(r.get_path_score() + node.get_score())
                node.set_predecessor(r)
                break
        # now  insert node in ranked_nodes to maintain increasing order of score and decreasing order of start point
        # use increasing order so that insertions occur near end rather than near start
        for i  in reversed(range(len(ranked_nodes)))  :
            r = ranked_nodes[i]
            if (node.get_path_score() > r.get_path_score() or
                     (node.get_path_score() == r.get_path_score() and
                      node.get_start() <= r.get_start())) :
                ranked_nodes.insert(i+1,  node)
                break
        else:
            ranked_nodes.insert(0, node)

    # all models have been processed; recover the best chain of models
    chain = []
    r = ranked_nodes[-1]
    while r.has_predecessor():
        chain.append(r.get_model())
        r = r.get_predecessor()
    chain.append(r.get_model())
    return chain

def process_set(gene_list, args):
    if len(gene_list) < 2:
        return gene_list,  len(gene_list)
    selection = []

    consolidated = []
    for gene in gene_list:
        for con_gene in consolidated:
            if gene.CDS_matches(con_gene):
                con_gene = sum_scores(con_gene,  gene)
                if con_gene.get_strand() == '.':
                    con_gene.set_strand(gene.get_strand())
                break
        else:
            consolidated.append(gene)
    consolidated.sort()
    sequences = find_heaviest_path(consolidated,  args)
    chain_length = len(sequences)
    return sequences,  chain_length

def get_interval(current_set,  selection,  locus_num):
    locus = 'L%06d' % locus_num
    for gene in current_set:
        gene.add_attribute('locus',  locus)
    selected = ','.join([gene.get_name() for gene in selection])
    current_set.sort()
    first = current_set[0]
    end = max([gene.get_end() for gene in current_set])
    interval = [locus, '%s:%d..%d' % (first.get_seqID(),  first.get_start(),  end), '?' , ' ', selected,  'none']
    for gene in current_set:
        interval.append(gene.get_name())
    return '\t'.join(interval)

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in',  dest='input',  nargs='+',   help='Paths to input gff3 files to be merged; at least one is required; more are allowed in a space-delimited list')
    argparser.add_argument('--out', type=argparse.FileType('w'),  nargs='?',  default = sys.stdout,  help='Path to the output file; if omitted or -, output is written to stdout')
    argparser.add_argument('--intervals', type=argparse.FileType('w'),  required=True,  help='Path to an output file to record positions of overlapping input models; required')
    argparser.add_argument('--pseudoscore', type=float,  default=0.0001,  help='A small positive constant added to all scores to avoid merging problems')
    return  argparser.parse_args()

if __name__ == '__main__':
    args = get_args()
    inputs = [GFF3Iterator(open(f)).genes() for f in args.input]
    output = args.out
    intervals_out = args.intervals

    print >> intervals_out,  'Locus\tPosition\tSelection\tIntron_comments\tMerged\tCandidate Models'
    current_set = []
    current_scaffold = 'No_such_scaffold'
    current_start = 0
    current_end = 0
    next_genes = [input.next() for input in inputs]
    locus_num = 0
    gene_count = 0

    while any(next_genes):
        min_gene = None
        min_i = None
        for i,  gene in enumerate(next_genes):
            if gene and (not min_gene or gene < min_gene):
                min_gene = gene
                min_i = i
        try:
            next_genes[min_i] = inputs[min_i].next()
        except StopIteration:
            next_genes[min_i] = None
        if current_set and (min_gene.get_seqID() + '!' < current_scaffold + '!' or (min_gene.get_seqID() == current_scaffold and min_gene.get_end() < current_start)) :
            print >> sys.stderr,  "Genes out of order!",  current_scaffold,  current_start,  current_end
            print >> sys.stderr,  min_gene
            continue
        if not current_set:
            current_set = [min_gene]
            current_scaffold = min_gene.get_seqID()
            current_end = min_gene.get_end()
            current_start = min_gene.get_start()
        elif min_gene.get_seqID() == current_scaffold and min_gene.get_start() < current_end:
            current_set.append(min_gene)
            current_end = max(current_end,  min_gene.get_end())
            current_start = min(current_start,  min_gene.get_start())
        else:
            locus_num += 1
            selection,  gene_number = process_set(current_set,  args)
            gene_count += gene_number
            interval = get_interval(current_set,  selection,  locus_num)
            print >> intervals_out,  interval
            for gene in selection:
                print >> output,  gene
            current_set = [min_gene]
            current_scaffold = min_gene.get_seqID()
            current_end = min_gene.get_end()
            current_start = min_gene.get_start()
    # Flush last gene
    if current_set:
        locus_num += 1
        selection,  gene_number = process_set(current_set,  args)
        gene_count += gene_number
        interval = get_interval(current_set,  selection,  locus_num)
        print >> intervals_out,  interval
        for gene in selection:
            print >> output,  gene

    intervals_out.close()
    output.close()
#    print 'Estimated gene number =',  gene_count
#    print sys.argv[0],  'done.'
