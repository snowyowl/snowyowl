__author__ = 'ian'

import sys, os

this_dir = os.path.dirname(__file__)
src = os.path.dirname(this_dir)
sys.path.append(src)
import argparse
from Bio.Blast import NCBIXML

DESCRIPTION = 'Convert output from NCBI+ blastx into tabular format produced by tera-blastx'
VERSION = '0.1'

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,
        help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in', '-i', dest='input', type=argparse.FileType('r'), nargs='?', default=sys.stdin,
        help='Path to the input file; if omitted or -, input is read from stdin')
    argparser.add_argument('--out', '-o', type=argparse.FileType('w'), nargs='?', default=sys.stdout,
        help='Path to the output file; if omitted or -, output is written to stdout')
    return  argparser.parse_args()


if __name__ == '__main__':
    args = get_args()

    for blast_record in NCBIXML.parse(args.input):
        for alignment in blast_record.alignments:
            tstart = -1
            tend = -1
            qstart = -1
            qend = -1
            evalue = -1
            frame = 0
            aligned = 0
#            print alignment
            if alignment.hsps:
                frame = alignment.hsps[0].frame[0]
                tstart = min([hsp.sbjct_start for hsp in alignment.hsps])
                tend = max([hsp.sbjct_end for hsp in alignment.hsps])
                qstart = min([hsp.query_start for hsp in alignment.hsps])
                qend = max([hsp.query_end for hsp in alignment.hsps])
                evalue = max([hsp.expect for hsp in alignment.hsps])
                aligned = sum([0.5 * (hsp.identities + hsp.positives) for hsp in alignment.hsps])
            target_locus, target_description = alignment.hit_def.split(' ',1)
            percent_alignment = int(aligned * 300 / blast_record.query_length +0.5)
            print >> args.out, '\t'.join([blast_record.query.split()[0],
                                          target_locus, target_description,
                                          str(evalue), str(percent_alignment), str(frame),
                                          str(alignment.length), str(tstart), str(tend),
                                          str((blast_record.query_length + 2) / 3), str((qstart + 2) / 3), str((qend + 2) / 3)])


    args.out.close()
#    print >> sys.stderr, sys.argv[0], 'done.'
