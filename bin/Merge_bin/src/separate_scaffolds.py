'''Create a directory and fill it with individual fasta files for each chromosome in a genome

Created on Aug 26, 2010
@author: ian
'''

import sys, os
from Bio import SeqIO
from gff3Record import GFF3Record

def usage():
    print 'Usage: python %s genome.fasta' % sys.argv[0]
    sys.exit(1)

if __name__ == '__main__':
    try:
        input = open(sys.argv[1])
        working_dir = os.path.dirname(sys.argv[1])
    except IndexError:
        usage()
    
    scaffolds_dir = os.path.join(working_dir, 'scaffolds')
    gff3 = open(os.path.join(working_dir, 'scaffolds.gff3'), 'w')
    gff3.write('##gff-version 3\n')
    source = 'separate_scaffolds'
    so_type = 'scaffold'
    start = 1
    score = '.'
    strand = '.'
    phase = '.'
    
    try:
        os.mkdir(scaffolds_dir)
    except:
        pass
    
    scaffolds = SeqIO.parse(input, 'fasta')
    for scaffold in scaffolds:
        # make fasta file
        fasta_name = os.path.join(scaffolds_dir, scaffold.id + '.fasta')
        output = open(fasta_name, 'w')
        print >> output, scaffold.format('fasta')
        output.close()
        # make scaffold record
        seqID = scaffold.name
        end = len(str(scaffold.seq))
        attributes = {'Name':seqID}
        gff = GFF3Record(seqID, source, so_type, start, end, score, strand, phase, attributes)
        print >> gff3, gff
    gff3.close()
#    print sys.argv[0], 'done.'
