"""Run remote blastx searches on TimeLogic boards

Created: 2011-06-23
Author: ian
"""

import sys, os,  re,  time,  subprocess,  socket
from paramiko import SSHClient
from uuid import uuid1
import argparse

DESCRIPTION = 'Run remote blastx searches on TimeLogic boards'
VERSION = '5.0'

waiting_pat = re.compile(r"""The job is waiting""")
running_pat = re.compile(r"""The job has finished processing (\d+)% of the data""")
completed_pat = re.compile(r"""Completed: Output Ready""")

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in',  dest='input', required=True,  help='Path to the input protein sequence file in fasta format')
    argparser.add_argument('--out', required=True,  help='Path to the output file')
    argparser.add_argument('--log', '-l', default='run_blastx.log',  help='Name of a file for logging problems and progress')
    argparser.add_argument('--server', '-s', help='URL of the server hosting TimeLogic boards; required')
    argparser.add_argument('--username', '-u', help='Username on the server hosting TimeLogic boards; required')
    argparser.add_argument('--mach_name', help='Identifier of the TimeLogic board to be used; required')
    argparser.add_argument('--decypher_path', default='/export/data_new/programs/decypher/bin/', help='Path to the server directory containing Decypher software for the TimeLogic boards')
    argparser.add_argument('--target', default='uniprot_sprot_2010_11', help='Name of the database to search')
    argparser.add_argument('--template', default='blast/tera-blastx', help='Name of the Decypher search template')
    argparser.add_argument('--max_timeouts',type=int,  default=5, help='Maximum number of timeouts during file transfer')
    return  argparser.parse_args()

if __name__ == '__main__':
    args = get_args()
    input = args.input
    output = args.out
    LOGFILE = args.log
    MAX_TIMEOUTS = args.max_timeouts
    DC_PATH = args.decypher_path + '/'
    # Templates for Decypher commands
    SUBMIT = DC_PATH + 'dc_template'
    STATUS = DC_PATH + 'dc_status'
    RETRIEVE = DC_PATH + 'dc_retrieve'

    server = args.server
    username = args.username
    mach = '-mach ' + args.mach_name
    submit_pat = re.compile(r"""OK.*(SOC_%s_\d+)""" % username)
    queryfilename = 'blast/%s.faa' % uuid1()
    resultfilename = queryfilename[:-3] + 'blastx'
    now = time.ctime()
    log = open(LOGFILE,  'a')
    print >> log,  'IN  %s %s -> %s' % (now,  input,  queryfilename)
    log.close()

    scp_cmd_list =  ['scp', input, '%s@%s:%s' % (username,  server,  queryfilename) ]
    retcode = subprocess.call(scp_cmd_list)
    if retcode != 0 :
        print >> sys.stderr,  'Query file upload failed!'
        log = open(LOGFILE,  'a')
        print >> log,  '    Query file upload failed! Aborting.'
        log.close()
        sys.exit(1)

    client = SSHClient()
    client.load_system_host_keys()

    client.connect(server,  username=username)
    job_id = None
    r_in,  r_out,  r_errs = client.exec_command('%s %s -query %s -template %s -targ %s -priority 0' % (SUBMIT, mach,  queryfilename, args.template, args.target))
    for line in r_out:
        if submit_pat.match(line):
            job_id = submit_pat.match(line).group(1)
            break
    else:
        for line in r_errs:
            print >> sys.stderr,  line,
        log = open(LOGFILE,  'a')
        print >> log,  '    Job submission failed! Aborting.'
        log.close()
        sys.exit(2)
    client.close()
    log = open(LOGFILE,  'a')
    print >> log,  '    %s Submitted as job %s on %s' % (time.ctime(),  job_id,  mach[1:])
    log.close()

    # count contigs in input
    p0 = subprocess.Popen([ "grep", "^>", input], stdout=subprocess.PIPE)
    p1 = subprocess.Popen(["sort", '-u', '-k1,1'], stdin=p0.stdout, stdout=subprocess.PIPE)
    p2 = subprocess.Popen(['wc', '-l'], stdin=p1.stdout, stdout=subprocess.PIPE)
    p0.stdout.close()
    contig_count = int(p2.communicate()[0])

    wait_secs = 60
    completed = False
    timeouts = 0
    while not completed:
        try:
            client.connect(server,  username=username)
            timeouts = 0
            r_in,  r_out,  r_errs = client.exec_command('%s %s %s' % (STATUS, job_id,  mach))
            for line in r_out:
                if completed_pat.match(line):
                    r_in,  r_out,  r_errs = client.exec_command('%s %s %s > %s' % (RETRIEVE, job_id,  mach,  resultfilename))
                    r_in,  r_out,  r_errs = client.exec_command('ls -l %s' % resultfilename )
                    r_lines = []
                    for line in r_out:
                    	r_lines.append(line)
                    result_size = int(r_lines[-1].split()[4])
                    max_tries = 3
                    tries = received_size = 0
                    while received_size < result_size and tries < max_tries:
                    # retrieve result
                        scp_cmd_list =  ['scp', '%s@%s:%s' % (username,  server,  resultfilename),  output ]
                        retcode = subprocess.call(scp_cmd_list)
                        if retcode != 0:
                            print >> sys.stderr, 'Result file download error!'
                            log = open(LOGFILE,  'a')
                            print >> log,  '    Result download error! Retrying.'
                            log.close()
                            tries += 1
                            continue
                        # count blast results
                        received_size = os.stat(output).st_size
                        if received_size < result_size:
                            print >> sys.stderr, 'Result file download is incomplete!'
                            log = open(LOGFILE,  'a')
                            print >> log,  '    Result download incomplete! Retrying.'
                            log.close()
                            tries += 1
                    if tries >= max_tries:
                        print >> sys.stderr, 'Result file download failed!'
                        log = open(LOGFILE,  'a')
                        print >> log,  '    Result download failed! Aborting.'
                        log.close()
                        sys.exit(4)
                    # cleanup
                    client.exec_command('rm %s' % queryfilename)
                    client.exec_command('rm %s' % resultfilename)
                    completed = True
                    client.close()
                    log = open(LOGFILE,  'a')
                    print >> log,  'OUT %s %s -> %s' % (time.ctime(),  job_id,  output)
                    log.close()
                    sys.exit(0)
                elif running_pat.match(line):
                    wait_secs = 60
                    break
                elif waiting_pat.match(line):
                    wait_secs = 30
                    break
                else:
                    print >> sys.stderr,  line,
        except socket.error,  se:
            print >> sys.stderr,  se
            if 'timed out' in se:
                timeouts += 1
                if timeouts > MAX_TIMEOUTS:
                    print >> sys.stderr,  'Too many timeouts. Aborting'
                    log = open(LOGFILE,  'a')
                    print >> log,  '    %d consecutive timeouts. Aborting.' % timeouts
                    log.close()
                    sys.exit(5)

        client.close()
        time.sleep(wait_secs)


#print sys.argv[0],  'done.'
