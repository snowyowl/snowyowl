"""Change sequence numbers in names and IDs of gene models to a new range

Created: 2011-07-18
Author: ian
"""
import sys, os
from gff3Iterator import GFF3Iterator
import re
import argparse

DESCRIPTION = 'Change sequence numbers in names and IDs of gene models to a new range'
VERSION = '5.0'

name_pat = re.compile(r"""([^_]+)_(\d+)""")

def renumber(gene,  new_number):
    old_id = gene.get_ID()
    new_id = name_pat.sub('\\1_%06d' % new_number,  old_id)
    gene.set_ID(new_id)
    gene.set_name(new_id)
    transcript_num = 0
    for transcript in gene.get_transcripts():
        transcript_num += 1
        transcript_id = '%s.t%d' % (new_id,  transcript_num)
        transcript.set_ID(transcript_id)
        transcript.set_name(new_id)
        transcript.set_parents(new_id)
        exon_id = '%s.cds' % transcript_id
        for exon in transcript.get_exons():
            exon.set_ID(exon_id)
            exon.set_name(new_id)
            exon.set_parents(transcript_id)
    return gene

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in',  '-i', dest='input',  type=argparse.FileType('r'),  nargs='?',  default = sys.stdin,  help='Path to the input file containing gene models in gff3 format; if omitted or -, input is read from stdin')
    argparser.add_argument('--out',  '-o', type=argparse.FileType('w'),  nargs='?',  default = sys.stdout,  help='Path to the output file; if omitted or -, output is written to stdout')
    argparser.add_argument('--number',  '-n', type=int,  required=True,  help='Starting number')
    return  argparser.parse_args()


if __name__ == '__main__':
    args = get_args()
    input = GFF3Iterator(args.input).genes()
    output = args.out
    gene_num = args.number

    for gene in input:
        gene = renumber(gene,  gene_num)
        print >> output,  gene
        gene_num += 1
    output.close()
#    print sys.argv[0],  'done.'
