"""Select one or more representative gene models from every locus with predicted genes in a genome
Number representative models by position.

Created: 2011-03-09
Refactored for testability: 2012-04-22
Author: ian
"""
import sys, os
from gff3Iterator import GFF3Iterator
from gff3_rename import gene_rename

#PREFERRED_PREFIX_1 = 'Round2' # Genemark prediction
#PREFERRED_PREFIX_2 = 'csfg' # 2nd round Augustus prediction


def usage(msg=None):
    if msg:
        print >> sys.stderr,  msg
    print >> sys.stderr,  'Usage: python %s  input.merged.gff3 name_prefix  output.representatives.gff3' % sys.argv[0]
    sys.exit(1)

def get_representatives(gene_tuples,  output):
    current_set = None
    current_scaffold = None
    current_start = 0
    current_end = 0
    gene_count = 0
    representative = None
#    for gene in sorted(list(input)):
    for t in gene_tuples:
        gene = t[1]
        if current_scaffold and (gene.get_seqID() < current_scaffold or (gene.get_seqID() == current_scaffold and gene.get_end() < current_start)) :
            raise ValueError('Gene %s is out of order' % gene.get_ID()) # This should not happen, so abort if it does!
        # Ensure that representative is initialized
        if not representative:
            representative = gene
            max_score = float(representative.get_score())
            current_scaffold = gene.get_seqID()
            current_end = gene.get_end()
            current_start = gene.get_start()
        elif gene.get_seqID() == current_scaffold and gene.get_start() < representative.get_end():
            current_end = max(current_end,  gene.get_end())
            current_start = min(current_start,  gene.get_start())
            if float(gene.get_score()) > max_score:
                representative = gene
                max_score = float(representative.get_score())
        else: # Output current representative and initialize a new downstream representative
            gene_count += 1
            new_name = '%s%06d' % (prefix,  gene_count)
            print >> output,  gene_rename(representative,  new_name)
            representative = gene
            max_score = float(representative.get_score())
            current_scaffold = gene.get_seqID()
            current_end = gene.get_end()
            current_start = gene.get_start()
    # Output last representative
    if representative:
        gene_count += 1
        new_name = '%s%06d' % (prefix,  gene_count)
        print >> output,  gene_rename(representative,  new_name)
    print gene_count,  'representative models found.'

def do_get_representative_models(input,  output):
#     Make sure that genes are sorted by coordinate
    gene_tuples = []
    for gene in input:
        sort_key = '%s!%010d%010d' % (gene.get_seqID(), gene.get_start(),  gene.get_end())
        gene_tuples.append((sort_key, gene))
    gene_tuples.sort()
    get_representatives(gene_tuples,  output)


if __name__ == '__main__':
    try:
        input = GFF3Iterator(open(sys.argv[1])).genes()
        prefix = sys.argv[2]
        output = open(sys.argv[3],  'w')
    except Exception,  e:
        usage(e)

    do_get_representative_models(input,  output)
    output.close()
#    print sys.argv[0],  'done.'
