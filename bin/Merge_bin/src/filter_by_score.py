"""Filter gene models by score
Created: 2011-01-14
Author: ian
"""
import sys, os
from gff3Record import GFF3Gene
from gff3Iterator import GFF3Iterator
import argparse

DESCRIPTION = 'Filter gene models by score'
VERSION = '5.0'

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in',  '-i',  dest='input', type=argparse.FileType('r'),  nargs='?',  default = sys.stdin,  help='Path to the input file; if omitted or -, input is read from stdin')
    argparser.add_argument('--threshold',  '-t', type=float, required=True,  help='Score threshold')
    argparser.add_argument('--out',  '-o', type=argparse.FileType('w'),  nargs='?',  default = sys.stdout,  help='Path to the output file; if omitted or -, output is written to stdout')
    return  argparser.parse_args()

if __name__ == '__main__':
    args = get_args()
    input = args.input
    threshold = args.threshold
    output = args.out

    genes = GFF3Iterator(input).genes()
    for gene in genes:
        if float(gene.get_score()) >= threshold:
            print >> output,  gene
    output.close()
#    print sys.argv[0],  'done.'
