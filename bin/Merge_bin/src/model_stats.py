"""Calculate mean and median lengths for model features

Created: 
Author: ian
"""
import sys, os
from gff3Iterator import GFF3Iterator
from collections import defaultdict

def usage(msg=None):
    if msg:
        print >> sys.stderr,  msg
    print >> sys.stderr,  'Usage: python %s  input.gff3  ' % sys.argv[0]
    sys.exit(1)

def get_stats(length_distribution):
    count = sum(length_distribution.values())
    mean = float(sum([key * length_distribution[key] for key in length_distribution])) / count 
    so_far = 0
    for key in sorted(length_distribution.keys()):
        so_far += length_distribution[key]
        if so_far *2 >= count:
            break
    median = key
    return count,  mean,  median

    
if __name__ == '__main__':
    try:
        input = GFF3Iterator(open(sys.argv[1]))
    except Exception,  e:
        usage(e)
    
    genelengths = defaultdict(int)
    splicedlengths = defaultdict(int)
    exonlengths = defaultdict(int)
    intronlengths = defaultdict(int)
    
    for gene in input.genes():
        genelengths[len(gene)] += 1
        for transcript in gene.get_transcripts():
            transcript_len = 0
            exons = sorted(transcript.get_exons())
            for exon in exons:
                exonlengths[len(exon)] += 1
                transcript_len += len(exon)
            splicedlengths[transcript_len] += 1
            if len(exons) > 1:
                for donor,  acceptor in zip(exons[:-1],  exons[1:]):
                    intron_len = acceptor.get_start() - donor.get_end() - 1
                    intronlengths[intron_len] += 1
    
    print 'Feature             Count  Mean length  Median length'
    count,  mean,  median = get_stats(genelengths)
    print 'Genes              %6d   %7.1f        %5d' % ( count,  mean,  median)
    count,  mean,  median = get_stats(splicedlengths)
    print 'Spliced transcripts%6d   %7.1f        %5d' % ( count,  mean,  median)
    count,  mean,  median = get_stats(exonlengths)
    print 'Exons              %6d   %7.1f        %5d' % ( count,  mean,  median)
    count,  mean,  median = get_stats(intronlengths)
    print 'Introns            %6d   %7.1f        %5d' % ( count,  mean,  median)
#    print sys.argv[0],  'done.'
