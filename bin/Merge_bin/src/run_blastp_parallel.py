__author__ = 'ian'
__author__ = 'omar'

import sys, os

this_dir = os.path.dirname(__file__)
src = os.path.dirname(this_dir)
sys.path.append(src)
import argparse
import subprocess, multiprocessing, fileinput,tempfile
from Bio import SeqIO


DESCRIPTION = 'Run a blastp search on multiple processors'
VERSION = '0.1'

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,
        help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in', '-i', dest='input', required=True,
        help='Path to the input query file')
    argparser.add_argument('--out', '-o', type=argparse.FileType('w'), required = True,
        help='Path to the output file')
    argparser.add_argument('--target', '-t', required=True, help='Blast database filename')
    argparser.add_argument('--processes', '-p', type=int, default=2, help='Maximum number of processes to use')
    return  argparser.parse_args()

def split_fasta(fasta_filename, nfrags):
    fmt = 'fasta'
    recs = SeqIO.index(fasta_filename,  fmt)
    rec_list = [[len(recs[key]),  key] for key in recs.keys() ]
    rec_list.sort(reverse=True)

    output_files = ['%s.%d' % (fasta_filename,  i) for i in range(nfrags)]
    outputs = [open(f,'w') for f in output_files]
    lengths = [0] * nfrags

    for size, key in rec_list:
        i = lengths.index(min(lengths))
        outputs[i].write(recs[key].format(fmt))
        lengths[i] += size

    for o in outputs:
        o.close()
    return output_files

def check_process_count(nproc):
    return(max(1,min(nproc, multiprocessing.cpu_count())))

def run_blastp(query, target):
    result = query + '.blastp'
    cmd_list = [ 'blastp', '-query', query, '-db', target, '-out', result, '-evalue', '1e-50', '-outfmt',
                 '6 qseqid ppos sseqid sacc evalue nident positive length gaps qstart qend sstart send bitscore' ,
                 '-max_target_seqs', '3']
    retcode = subprocess.call(cmd_list)
    if retcode != 0:
        raise RuntimeError('Failure in blastp')
    return result

def run_blastx(query, target):
    result = query + '.blastx'
    cmd = 'blastx -db %s -query %s -out %s -num_descriptions 1 -num_alignments 1 -evalue 1e-10 -outfmt 5' % (target, query, result)
    retcode = subprocess.call(cmd.split())
    if retcode != 0:
        raise RuntimeError('Failure in blastx')
    return result
def do_parallel_blast(input, out, target, processes,func):
    nprocs = check_process_count(processes)
    results = []
    if nprocs > 1:
        splits = split_fasta(input, nprocs)
        pool = multiprocessing.Pool(processes=nprocs)
        for split in splits:
            pool.apply_async(func, (split,target), callback=results.append)
        pool.close()
        pool.join()
        for split in splits:
            os.unlink(split)
    else:
        results.append(run_blastp(input,target))

    for line in fileinput.input(results):
        out.write(line)
    for r in results:
        os.unlink(r)
    return

def do_parallel_blastp(input, out, target, processes):
    return do_parallel_blast(input, out, target, processes, run_blastp)

def do_parallel_blastx(input, out, target, processes):
    return do_parallel_blast(input, out, target, processes, run_blastx)

if __name__ == '__main__':
    args = get_args()
    do_parallel_blastp(args.input, args.out,args.target,args.processes)
    args.out.close()
#    print >> sys.stderr, sys.argv[0], 'done.'
