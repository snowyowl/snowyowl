#!/usr/bin/perl -w
my $SO;

BEGIN {
  $SO = $ENV{'SNOWYOWL'} || 
    die "SNOWYOWL is not a defined environment variable\n";
}

use strict;
use warnings;
use Getopt::Long;

my ($seq_file, $output_file, $dc_path, $dc_template, $dc_target);
$dc_path = "/usr/local/timelogic/bin";
$dc_template = "$SO/bin/Merge_bin/config/tera-blastp.refseq";
$dc_target = "Fungal_RefSeq";
GetOptions(
  "s=s"		=> \$seq_file,
  "o=s"		=> \$output_file,
  "dc=s"	=> \$dc_path,
  "tmpl=s"      => \$dc_template,
  "targ=s"      => \$dc_target
);

($seq_file && $output_file)
  or die "usage: $0 -s <sequence file> -o <output file> -dc <DeCypher exec path> -tmpl <Decypher template [tera-blastp.refseq]> -targ <Target database [Fungal_RefSeq]>\n";

#my @decyphers = ("coedc05", "coedc06", "coedc07", "coedc08");
my @decyphers = ("coedc05", "coedc06", "coedc08");
my %decypher2priority = (
  "coedc01" => 6,
  "coedc02" => 6,
  "coedc03" => 6,
  "coedc04" => 6,
  "coedc05" => 0,
  "coedc06" => 0,
  "coedc07" => 0,
  "coedc08" => 0
);

my ($machine, $priority) = pick_decypher(@decyphers);

my $cmd = "$dc_path/dc_template_rt -mach $machine -priority $priority -query $seq_file -template $dc_template -targ $dc_target > $output_file";
print STDERR "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: tera-blast failed!\nAborting.\n\n"
};


# Picks the most idle DeCypher board among given ones.
# Returns the board name and job priority on that board.
sub pick_decypher {
  my $min_queue_count = 1000000; # Million should be big enough
  my $min_queue_decypher = "";
  for my $decypher (@decyphers) {
    my $is_open_ok = 1;
    open QSTATUS, "$dc_path/dc_qstatus -mach $decypher |"
      or $is_open_ok = 0;
    if ($is_open_ok) {
      my $queue_count = 0;
      while (<QSTATUS>) {
        $queue_count++ if /SOC_\S+_\d+\.tl\d/;
      }
      close QSTATUS;
      #print STDERR "$decypher: current = $queue_count, queued = $queue_count\n";
      if ($queue_count < $min_queue_count) {
        $min_queue_count = $queue_count;
        $min_queue_decypher = $decypher;
      }
    }
  }

  if ($min_queue_decypher) {
    #print STDERR "Picked $min_queue_decypher: queued = $min_queue_count\n";
    return ($min_queue_decypher, $decypher2priority{$min_queue_decypher});
  }
  else {
    return ("coedc08", 0);
  }
}
