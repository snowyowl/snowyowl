'''Scoring routines for gene models
Created on Dec 11, 2010

@author: ian
'''

import sys, os
from gff3Record import GFF3Gene
from gff3Iterator import GFF3Iterator
from pysam import Tabixfile
from collections import namedtuple, defaultdict
from Bio import SeqIO, Alphabet
from Bio.Data.CodonTable import TranslationError
from gff32predictedTranscriptsAndProteins import get_transcript_sequence
from classify_juncs import ClassifiedSplice
from check_intron_structure import check_transcript_intron_structure
from math import sqrt
import argparse

DESCRIPTION = 'Scoring routines for gene models'
VERSION = '5.1'
# Added check for too many Ns in coding sequences

Coverblock = namedtuple('Coverblock', 'seqID start end depth')


class StrandError(ValueError):
    pass

class SpliceSet():
    '''Represent a set of alternative splice junctions. '''

    def __init__(self):
        self.splices = []
        self.min_left = None
        self.max_left = None
        self.min_right = None
        self.max_right = None
        self.type = None

    def __repr__(self):
        return repr(self.splices)

    def __len__(self):
        return len(self.splices)

    def add(self,  addend):
        '''Merge another SpliceSet into this one
        '''
        if self.type and addend.type != self.type:
            raise ValueError("Can't add splice of type %s" % str(addend.type))
        if self.type and self.type == 'regular' and self.splices:
            raise ValueError("Can't have more than one regular splice in set")
        if addend in self.splices:
            return False
        self.splices.append(addend)
        self.type = addend.type
        self.min_left = min(self.min_left,  addend.left) if self.min_left else addend.left
        self.max_left = max(self.max_left,  addend.left) if self.max_left else addend.left
        self.min_right = min(self.min_right,  addend.right) if self.min_right else addend.right
        self.max_right = max(self.max_right,  addend.right) if self.max_right else addend.right
        return True

    def get_type(self):
        return self.type

    def get_left(self):
        return (self.min_left,  self.max_left)

    def get_right(self):
        return (self.min_right,  self.max_right)

    def get_readthrough(self):
        if self.type == 'regular':
            return self.splices[0].readthrough
        # calculate weighted average of member splice readthrough values
        rt = float(0)
        weights = 0
        for s in self.splices:
            rt += s.readthrough * s.count
            weights += s.count
        if weights == 0:
            return rt
        else:
            return rt / weights

    def restrict(self,  condition):
        ''' returns the subset of members that satisfy condition
         condition is a function takes a ClassifiedSplice instance as input and return True or False
        '''
        result = SpliceSet()
        for s in self.splices:
            if condition(s):
                result.add(s)
        return result

# End class SpliceSet


def get_splices(gene,  juncs_tabix):
    all_juncs = [ClassifiedSplice(*j.strip().split('\t')) for j in juncs_tabix.fetch(gene.get_seqID(), gene.get_start(), gene.get_end())]
    objecters = [s for s in all_juncs if ((s.strand != gene.get_strand()) and (s.type != 'antisense') and (s.readthrough < 0.75 ))]
    splices = [s for s in all_juncs if s.strand == gene.get_strand()]
    if objecters and len(objecters) >= len(splices):
        raise StrandError('Observed splice orientation %s inconsistent with gene %s' % (str(objecters[0]),  '%s %s:%d..%d [%s]' % (gene.getID(),  gene.get_seqID(), gene.get_start(), gene.get_end(),  gene.get_strand())))
    return splices

def make_splice_sets(splices):
    sets = []
    for splice in splices:
        if splice.type == 'alternative' and sets and sets[-1].get_type() == 'alternative' and sets[-1].get_right()[1] > splice.left:
            pass
        else:
            sets.append(SpliceSet())
        sets[-1].add(splice)
    return sets

def get_next_splice(splice_iter):
    try:
        result = splice_iter.next()
    except StopIteration:
        result = None
    return result

def adjusted_readthrough(raw_readthrough):
    return 1.0 - sqrt(1.0 - raw_readthrough)

def check_splice_compatibility(exons, splices,  args):
    filtered_splices = [s for s in splices if s.readthrough < args.max_intron_readthrough]
    splice_sets = make_splice_sets(filtered_splices)
    splice_iter = iter(splice_sets)
    curr_splice = get_next_splice(splice_iter)

#    for i, exon in enumerate(exons):
    i = 0
    while i < len(exons):
        exon = exons[i]
        # check exon start
        while curr_splice and curr_splice.get_right()[1] < exon.get_start() - 1 :
            exon.add_attribute('status',  "Start mismatch")
            exon.set_score(float(exon.get_score()) * adjusted_readthrough(curr_splice.get_readthrough()))
            curr_splice = get_next_splice(splice_iter)
        if curr_splice and curr_splice.get_left()[0] < exon.get_start() - 1:
            if curr_splice.get_right()[0] > exon.get_start() - 1 :
                exon.add_attribute('status',  "Start mismatch")
                exon.set_score(float(exon.get_score()) * adjusted_readthrough(curr_splice.get_readthrough()))
            else:
                compatible_splice = curr_splice.restrict(lambda s : s.right == exon.get_start() - 1)
                if not compatible_splice:
                    exon.add_attribute('status',  "Start mismatch")
                    exon.set_score(0)
                else:
                    # Splice matches predicted intron; reward the exon
                    exon.set_score(float(exon.get_score()) * 2 * (1 - adjusted_readthrough(compatible_splice.get_readthrough())))
            curr_splice = curr_splice.restrict(lambda s : s.left > exon.get_start())
            if not curr_splice:
                curr_splice = get_next_splice(splice_iter)
        elif i > 0:
            exon.add_attribute('status',  "Start mismatch")
            exon.set_score(0)
            if curr_splice:
                curr_splice = curr_splice.restrict(lambda s : s.left > exon.get_start())
            if not curr_splice:
                curr_splice = get_next_splice(splice_iter)
        elif len(exons) == 1 and not curr_splice: # monoexonic transcript
            pass

        # handle contiguous exons, e.g UTR-CDS boundary pair
        while i < len(exons) - 1 and exon.get_end() + 1 == exons[i+1].get_start():
            exon = exons[i+1]
            i += 1

        # Now check exon middle and end against next splice
        while curr_splice and curr_splice.get_left()[1] < exon.get_end() - 1:
            if curr_splice.get_right()[1] <= exon.get_end():
                exon.add_attribute('status',  "Retained intron")
                exon.set_score(float(exon.get_score()) * adjusted_readthrough(curr_splice.get_readthrough()))
            elif not curr_splice.restrict(lambda s : s.left == exon.get_end() - 1):
                exon.add_attribute('status',  "End mismatch")
                exon.set_score(float(exon.get_score()) * adjusted_readthrough(curr_splice.get_readthrough()))
            curr_splice = get_next_splice(splice_iter)
        if curr_splice and curr_splice.get_left()[0] > exon.get_end() - 1 :
            exon.add_attribute('status',  "End mismatch")
            exon.set_score(float(exon.get_score()) * adjusted_readthrough(curr_splice.get_readthrough()))
        elif not curr_splice and i < len(exons) -1 :
            exon.add_attribute('status',  "End mismatch")
            exon.set_score(0)
        elif curr_splice:
            # only splices that match the end of this exon are eligible to match against the start of the next exon
            curr_splice = curr_splice.restrict(lambda s : s.left == (exon.get_end() - 1))
            if curr_splice :
                # exon end matches curr_splice; reward the exon
                exon.set_score(float(exon.get_score()) * 2 * (1 - adjusted_readthrough(curr_splice.get_readthrough())))
        i += 1

def check_exons_vs_splices(gene,  juncs_tabix, args):
    correct = True
    splices = []
    transcript = gene.get_transcripts()[0]
    exons = []
    try:
        splices = get_splices(gene,  juncs_tabix)
        exons = sorted(transcript.get_exons())
        check_splice_compatibility(exons,  splices,  args)
    except StrandError,  se:
        print >> sys.stderr,  se
        transcript.set_score(0)
        gene.set_score(0)
        gene.add_attribute('status',  "Strand error")
        return False,  gene
    except ValueError,  ve:
        print >> sys.stderr,  ve
    except IndexError,  ie:
        print >> sys.stderr,  ie
    transcript_score = 1.0
    if exons:
        for x in exons:
            transcript_score *= float(x.get_score())
    transcript.set_score(transcript_score)
    gene.set_score(transcript.get_score())
    correct = transcript_score > 0.5
    return correct, gene

def get_exon_cover(exon,  coverage_tabix):
    try:
        wiglines = list(coverage_tabix.fetch(exon.get_seqID(), exon.get_start(), exon.get_end()))
        cover = []
        for line in wiglines:
            fields = line.strip().split('\t')
            block = Coverblock(fields[0],  max(exon.get_start(), int(fields[1]) + 1),  min(exon.get_end(), int(fields[2])),  int(fields[3]))
            cover.append(block)
    except ValueError,  e:
        cover =[Coverblock(exon.get_seqID(), exon.get_start(), exon.get_end(),  0)]
    return cover


def summarize_cover_depth(cover):
    depths = defaultdict(int)
    for block in cover:
        depths[block.depth] += 1 + block.end - block.start
    length = area = 0
    for depth in sorted(depths.keys()):
        length += depths[depth]
        area += depth * depths[depth]
    average_depth = float(area) / length if length > 0 else 0
    so_far = 0
    median_depth = average_depth
    for depth in sorted(depths.keys()):
        so_far += depths[depth]
        if  2 * so_far >= length :
            median_depth = depth
            break
    return average_depth,  median_depth

def get_low_run(cover,  tolerance=1):
    low = False
    max_low_run = low_start = low_end = 0
    for block in cover:
        if not low:
            if block.depth < tolerance:
                low = True
                low_start = block.start
                low_end = block.end
        else:
            if block.depth > tolerance:
                low = False
                low_run = 1 + low_end - low_start
                max_low_run = max(max_low_run,  low_run)
            else:
                low_end = block.end
    low_run = 1 + low_end - low_start
    max_low_run = max(max_low_run,  low_run)
    return max_low_run


def check_cover(gene, coverage_tabix,  tolerance=0.5):
    """Check that all predicted exons in the gene model have consistent RNA-Seq coverage depth.

    Arguments:
        gene - an instance of GFF3Gene
        coverage_tabix - an open pysam.Tabixfile containing RNA-Seq coverage data as provided by wiggles
    Returns:
        The median coverage depth
        Length of the longest run with coverage depth < tolerance * median
    """
    exons = gene.get_transcripts()[0].get_exons()
    cover_exons = []
    exon_medians = []
    transcript_median_depth = 0
    max_low_run = 0
    for exon in exons:
        try:
            cover = get_exon_cover(exon,  coverage_tabix)
            cover_exons.append(cover)
            average_depth,  median_depth = summarize_cover_depth(cover)
            exon_medians.append(median_depth)
        except ValueError,  ve:
            print >> sys.stderr,  '[score_models.check_cover]',  ve

    transcript_cover = [block for exon_cover in cover_exons for block in exon_cover]
    if transcript_cover:
        transcript_average_depth,  transcript_median_depth = summarize_cover_depth(transcript_cover)
        low_limit = max(1, transcript_average_depth * tolerance)
        max_low_run = get_low_run(transcript_cover,  tolerance=low_limit)

    median_ratios = [float(exon_median) / transcript_median_depth for exon_median in exon_medians] if transcript_median_depth > 0 else [0]
    # Exclude terminal exons from minimum median ratio check because coverage tails off at transcript ends
    min_median_ratio = 1
    if len(median_ratios) > 2:
        min_median_ratio = min(median_ratios[1:-1])
    return transcript_median_depth,  max_low_run,  min_median_ratio,  max(median_ratios)

def change_score(gene,  new_score):
    gene.set_score(new_score)
    for transcript in gene.get_transcripts():
        transcript.set_score(new_score)
        for exon in transcript.get_exons():
            exon.set_score(new_score)
    return gene

def check_translatability(transcript,  genome_dict, max_unknown_bases):
    cds = transcript.extract_CDS()
    cds_record = get_transcript_sequence(cds,  genome_dict)
    try:
        unknowns = str(cds_record.seq).count('N')
        N_blocks = str(cds_record.seq).count('NNNN')
        if unknowns > max_unknown_bases or N_blocks > 0:
            raise TranslationError('Excessive Ns in CDS')
        protein_seq = str(cds_record.seq.translate(cds=True))
        return protein_seq
    except TranslationError,  te:
        print >> sys.stderr,  transcript.get_ID(),  te
        transcript.add_attribute('error',  str(te))
        return False

def score_transcript(transcript,  gene_copy,   genome_seq, coverage_tabix, depth_threshold,  juncs_tabix,  pass_count,  fail_count,  scored, args):
    gene_copy.set_transcripts([transcript])
    gene_copy.set_start(transcript.get_start())
    gene_copy.set_end(transcript.get_end())
    gene_copy.set_score(transcript.get_score())
    name = transcript.get_name()
    if not name or name == "None":
        name = transcript.get_parents()
        transcript.set_name(name)
    gene_copy.set_name(name)
    gene_copy.set_ID(transcript.get_ID() + 'g')
    transcript.set_parents(gene_copy.get_ID())
    for exon in transcript.get_exons():
        exon.set_name(name)
        if exon.getScore() == '.':
            exon.set_score(1.0)
    if gene_copy.getScore() == '.':
        gene_copy = change_score(gene_copy,  1.0)
    # Scoring
    introns_well_structured = check_transcript_intron_structure(transcript,  genome_seq, args)
    translation = check_translatability(transcript,  genome_seq, args.max_unknown_bases)
    short_monoexonic = False
    if introns_well_structured and translation and len(translation) < args.min_protein_length:
        if len(transcript.get_exons()) < 2 and float(transcript.get_score()) < 1.05 : # No introns and no homology support
            short_monoexonic = True
        elif len(translation) < 50 and float(transcript.get_score()) < 1.05: #Very short and no homology support
            short_monoexonic = True
    if introns_well_structured and translation and not short_monoexonic:
        transcript_median_depth,  max_low_run,  min_median_ratio,  max_median_ratio = check_cover(gene_copy, coverage_tabix,  tolerance=args.coverage_tolerance)
        gene_copy.add_attribute('depth',  str(transcript_median_depth))
        if transcript_median_depth < depth_threshold:
            pass_count += 1
            if gene_copy.getScore() == '.':
                gene_copy = change_score(gene_copy,  0.6)
            else:
                gene_copy.set_score(float(gene_copy.getScore()) * 0.6)
                transcript.set_score(float(transcript.getScore()) * 0.6)
            gene_copy.add_attribute('status',  'Low expression')
            print >> scored,  gene_copy
            return pass_count, fail_count
        if min_median_ratio < args.min_median_ratio  or max_median_ratio > args.max_median_ratio or max_low_run > args.max_low_run:
            fail_count += 1
            if min_median_ratio < args.min_median_ratio:
                gene_copy.add_attribute('status',  'Heterogeneous coverage depth;min_median_ratio=%.3f' % (min_median_ratio))
            elif max_median_ratio > args.max_median_ratio :
                gene_copy.add_attribute('status',  'Heterogeneous coverage depth;max_median_ratio=%.1f' % (max_median_ratio))
            elif max_low_run > args.max_low_run :
                gene_copy.add_attribute('status',  'Heterogeneous coverage depth;max_low_run=%d' % (max_low_run))
            gene_copy = change_score(gene_copy,  0)
            print >> scored,  gene_copy
            return pass_count, fail_count
        splices_correct,  annotated_gene_copy = check_exons_vs_splices(gene_copy,  juncs_tabix,  args)
        if splices_correct:
            pass_count += 1
            if annotated_gene_copy.getScore() == '.':
                annotated_gene_copy = change_score(annotated_gene_copy,  1.0)
            annotated_gene_copy.add_attribute('status',  'Matches RNA-Seq data')
            print >> scored,  annotated_gene_copy
        else:
            fail_count += 1
            if not splices_correct:
                annotated_gene_copy.add_attribute('status',  'Fails to match RNA-Seq data')
            print >> scored,  annotated_gene_copy
    else:
        if not introns_well_structured:
            status_msg = 'Bad intron structure'
        elif not translation:
            status_msg = 'Cannot be translated into protein'
        elif short_monoexonic:
            status_msg = 'Short predicted protein %d aa' % len(translation)
        fail_count += 1
        gene_copy.set_score(0)
        transcript.set_score(0)
        transcript.add_attribute('status',  status_msg)
        gene_copy.add_attribute('status',  status_msg)
        print >> scored,  gene_copy
    return pass_count, fail_count


def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in','-i',  dest='input', type=argparse.FileType('r'),  nargs='?',  default = sys.stdin,  help='Path to the input file; if omitted or -, input is read from stdin')
    argparser.add_argument('--genome',  '-g', type=argparse.FileType('r'),  required=True,  help='Path to the genome sequence file in fasta format; required')
    argparser.add_argument('--juncs',  '-j', required=True,  help='Path to the tabix-indexed splice junctions file; required')
    argparser.add_argument('--coverage',  '-c', required=True,  help='Path to the tabix-indexed read coverage file; required')
    argparser.add_argument('--threshold',  '-t',  type=int, required=True,  help='Threshold coverage depth below which models are classified "Low expression"; required')
    argparser.add_argument('--out','-o', type=argparse.FileType('w'),  nargs='?',  default = sys.stdout,  help='Path to the output file; if omitted or -, output is written to stdout')
    argparser.add_argument('--min_protein_length', type=int, default=150,  help='Shorter predicted proteins from monoexonic models with no homologs in database are penalized')
    argparser.add_argument('--min_intron_length', type=int, default=10,  help='Predicted introns shorter than this are rejected')
    argparser.add_argument('--max_intron_length', type=int, default=2000,  help='Predicted introns longer than this are rejected')
    argparser.add_argument('--min_median_ratio', type=float, default=0.15,  help='Transcripts containing an exon with a median coverage depth that is a smaller fraction of the transcript median depth are assigned a status of Heterogeneous coverage depth')
    argparser.add_argument('--max_median_ratio', type=float, default=3.0,  help='Transcripts containing an exon with a median coverage depth that is a larger multiple of the transcript median depth are assigned a status of Heterogeneous coverage depth')
    argparser.add_argument('--max_low_run', type=int, default=10,  help='Transcripts containing a longer run of coverage depth less than coverage_tolerance * transcript mean depth are assigned a status of Heterogeneous coverage depth')
    argparser.add_argument('--coverage_tolerance', type=float, default=0.05,  help='Fraction of transcript mean depth used to define a low run')
    argparser.add_argument('--max_intron_readthrough', type=float, default=0.8,  help='Splices with a higher readthrough ratio are ignored during scoring')
    argparser.add_argument('--max_unknown_bases', type=int, default=8,  help='Transcripts with more Ns in their sequences are assigned a status of Cannot be translated into protein')
    return  argparser.parse_args()

if __name__ == '__main__':
    args = get_args()
    save_protein = None
    genes = GFF3Iterator(args.input).genes()
    genome_seq = SeqIO.to_dict(SeqIO.parse(args.genome,  'fasta'))
    juncs_tabix = Tabixfile(args.juncs)
    coverage_tabix = Tabixfile(args.coverage)
    depth_threshold = args.threshold
    scored = args.out

    pass_count = fail_count = 0
    for gene in genes:
        gene_copy = GFF3Gene.fromRecord(gene)
        for transcript in gene.get_transcripts():
            pass_count, fail_count = score_transcript(transcript,  gene_copy,   genome_seq, coverage_tabix, depth_threshold,   juncs_tabix,  pass_count,  fail_count,  scored,  args)

    scored.close()
#    print >> sys.stderr,  sys.argv[0],  'done.'
