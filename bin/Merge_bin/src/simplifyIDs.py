"""Change names and IDs of gene models to shorter and simpler versions

Created: 2011-06-22
Author: ian
"""
import sys, os
from gff3Iterator import GFF3Iterator
import re

rt_pat = re.compile(r"""Locus(\d+)\.g(\d+)""")
gm_pat = re.compile(r"""([^_]+)((?:_v\d+)?)_cal_genemark_mRNA(\d+)_tg""")
pvr_pat = re.compile(r"""([^_]+)((?:_v\d+)?)_cal_aug[a-z]*PPXVRNA_(\S+)_g(\d+)""")
pvr_pat2 = re.compile(r"""([^_]+)((?:_v\d+)?)_cal_aug[a-z]*PPXVRNA(\S+)_g(\d+)""")
gm_pat2 = re.compile(r"""([^_]+)((?:_v\d+)?)_cal_genemark_gene(\d+)_t""")
gm_pat3 = re.compile(r"""gene(\d+)_t""")
gm_pat4 = re.compile(r"""([^_]+)_((?:v\d+)?)_cal_genemark_g_(\d+)""")
gm_pat5 = re.compile(r"""gene(\d+)_g""")
pvr_pat3 = re.compile(r"""(.+)_g(\d+)""")

def usage(msg=None):
    if msg:
        print >> sys.stderr,  msg
    print >> sys.stderr,  'Usage: python %s  input  output' % sys.argv[0]
    sys.exit(1)

def get_simple_ID(old_id):
    rt_match = rt_pat.match(old_id)
    if rt_match:
        new_id = 'rt_%06d.%d' % tuple([int(g) for g in rt_match.groups()[:2]])
    else:
        pvr_match = pvr_pat.match(old_id)
        if pvr_match:
            genome = pvr_match.group(1).title()
            if pvr_match.group(2):
                genome +=  pvr_match.group(2)[2:]
            new_id = '%s_pvr_%06d' % (genome,  int(pvr_match.group(4)))
        else:
            pvr_match = pvr_pat2.match(old_id)
            if pvr_match:
                genome = pvr_match.group(1).title()
                if pvr_match.group(2):
                    genome +=  pvr_match.group(2)[2:]
                new_id = '%s_pvr_%06d' % (genome,  int(pvr_match.group(4)))
            else:
                gm_match = gm_pat.match(old_id)
                if gm_match:
                    genome = gm_match.group(1).title()
                    if gm_match.group(2):
                        genome +=  gm_match.group(2)[2:]
                    new_id = '%s_gm_%06d' % (genome,  int(gm_match.group(3)))
                else:
                    gm_match = gm_pat2.match(old_id)
                    if gm_match:
                        genome = gm_match.group(1).title()
                        if gm_match.group(2):
                            genome +=  gm_match.group(2)[2:]
                        new_id = '%s_gm_%06d' % (genome,  int(gm_match.group(3)))
                    else:
                        gm_match = gm_pat3.match(old_id)
                        if gm_match:
                            new_id = 'gm_%06d' % ( int(gm_match.group(1)))
                        else:
                            gm_match = gm_pat4.match(old_id)
                            if gm_match:
                                genome = gm_match.group(1).title()
                                if gm_match.group(2):
                                    genome +=  gm_match.group(2)[2:]
                                new_id = '%s_gm_%06d' % (genome,  int(gm_match.group(3)))
                            else:
                                gm_match = gm_pat5.match(old_id)
                                if gm_match:
                                    new_id = 'gm_%06d' % ( int(gm_match.group(1)))
                                else:
                                    pvr_match = pvr_pat3.match(old_id)
                                    if pvr_match:
                                        new_id = 'pvr_%s_%06d' % (pvr_match.group(1),  int(pvr_match.group(2)))
                                    else:
                                        raise ValueError('[simplifyIDs.get_simple_ID] No pattern match for %s' % old_id)
    return new_id

def shorten_name_and_id(gene):
    old_id = gene.get_ID()    
    new_id = get_simple_ID(old_id)
    gene.set_ID(new_id)
    gene.set_name(new_id)
    transcript_num = 0
    for transcript in gene.get_transcripts():
        transcript_num += 1
        transcript_id = '%s.t%d' % (new_id,  transcript_num)
        transcript.set_ID(transcript_id)
        transcript.set_name(new_id)
        transcript.set_parents(new_id)
        exon_id = '%s.cds' % transcript_id
        for exon in transcript.get_exons():
            exon.set_ID(exon_id)
            exon.set_name(new_id)
            exon.set_parents(transcript_id)
    return gene

    
if __name__ == '__main__':
    try:
        input = GFF3Iterator(open(sys.argv[1])).genes()
        output = open(sys.argv[2],  'w')
    except Exception,  e:
        usage(e)
    
    for gene in input:
        gene = shorten_name_and_id(gene)
        print >> output,  gene
    output.close()
#    print sys.argv[0],  'done.'
