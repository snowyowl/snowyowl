"""Parse a .gff file from exonerate:est2genome and rewrite as standard gff3 with gene-mRNA-exon/CDS hierarchy and proper ID, Name, and Parent tags

Created: 2011-05-10
Author: ian
"""
import sys, os
from gff3Record import GFF3Exon,  GFF3Gene,  GFF3Record,  GFF3mRNA
from find_longest_ORFs import find_longest_ORF_in_transcript
from Bio import SeqIO, Seq, SeqRecord, Alphabet
import argparse

def output_curr_gene(curr_gene,  curr_strand,  output,  dna_index):
    if curr_gene:
        curr_gene.set_strand(curr_strand)
        for transcript in curr_gene.get_transcripts():
            transcript.set_strand(curr_strand)
            for exon in transcript.get_exons():
                exon.set_strand(curr_strand)
                exon.set_type('CDS')
            transcript = find_longest_ORF_in_transcript(transcript,  dna_index)
        if True:
            print >> output,  curr_gene.toString_with_start_and_stop_codons()
        else:
            print >> output,  curr_gene

def split_gene(curr_gene):
    new_gene = GFF3Gene.fromRecord(curr_gene)
    curr_ID = curr_gene.get_ID()
    new_ID = curr_ID + '_2'
    new_gene.set_ID(new_ID)
    mRNA = curr_gene.get_transcripts()[0]
    curr_exons = sorted(mRNA.get_exons())
    curr_end = curr_exons[-1].get_end()
    curr_start = curr_exons[0].get_start()
    curr_gene.set_start(curr_start)
    curr_gene.set_end(curr_end)
    mRNA.set_start(curr_start)
    mRNA.set_end(curr_end)
    if curr_end < new_gene.get_end() and curr_start == new_gene.get_start():
        # new gene is downstream
        new_start = curr_exons[-1].get_start()
        new_gene.set_start(new_start)
        new_mRNA = GFF3mRNA.fromRecord(new_gene)
        new_exon = GFF3Exon.fromRecord(curr_exons[-1])
    elif curr_end == new_gene.get_end() and curr_start > new_gene.get_start():
        #new gene is upstream
        new_end = curr_exons[0].get_end()
        new_gene.set_end(new_end)
        new_mRNA = GFF3mRNA.fromRecord(new_gene)
        new_exon = GFF3Exon.fromRecord(curr_exons[0])
    else:
        raise ValueError('Which way to go?')
    new_mRNA.set_ID(new_gene.getID() + '.t1')
    new_exon.clear_parents()
    new_mRNA.add_exon(new_exon)
    new_gene.add_transcript(new_mRNA)
    return curr_gene,  new_gene


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Parse a .gff file from exonerate:est2genome and rewrite as standard gff3 with gene-mRNA-exon/CDS hierarchy and proper ID, Name, and Parent tags')
    parser.add_argument('-s','--startstop', dest='startstop', action='store_true', default=False, help='force output of start and stop codons')
    parser.add_argument('--genome',  '-g',  required=True,  help='Path to genome sequence file in fasta format; required')
    parser.add_argument('input_file',  help = 'Input filename')
    parser.add_argument('output_file',  help = 'Output filename')
    args = parser.parse_args()

    input = open(args.input_file)
    output = open(args.output_file,  'w')
    dna_index = SeqIO.to_dict(SeqIO.parse(open(args.genome), 'fasta', Alphabet.generic_dna))

    curr_gene = None
    curr_strand = None
    for line in input:
        if line.startswith('#'):
            continue
        fields = line.strip().split('\t')
        if len(fields) != 9:
            continue
        try:
            attr_fields = fields[8].split(';')
            attrs = {}
            for attr_field in attr_fields:
                key,  val = attr_field.strip().split()[:2]
                if key in ['gene_id',  'gene_orientation']:
                    continue
                attrs[key] = val
            fields[8] = attrs
            rec = GFF3Record(*fields)
            if rec.has_attribute('sequence'):
                rec.add_attribute('Name',  rec.get_attribute('sequence'))
                rec.remove_attribute('sequence')
            if rec.getType() == 'gene':
                if curr_gene and curr_strand == '.':
                    raise ValueError('Ambiguous orientation in ' + curr_gene.get_ID())
                output_curr_gene(curr_gene,  curr_strand,  output,  dna_index)
                curr_gene = GFF3Gene.fromRecord(rec)
                curr_strand = '.'
                d_a = []
                curr_gene.add_attribute('ID',  curr_gene.get_name())
                mRNA = GFF3mRNA.fromRecord(curr_gene)
                mRNA.set_ID(curr_gene.getID() + '.t1')
                curr_gene.add_transcript(mRNA)
            elif curr_gene:
                if rec.getType() == 'exon':
                    exon = GFF3Exon.fromRecord(rec)
                    exon.set_name(curr_gene.get_name())
                    if mRNA:
                        mRNA.add_exon(exon)
                    d_a = []
                elif rec.getType() == 'start_codon':
                    if mRNA:
                        mRNA.add_start_codon(rec)
                elif rec.getType() == 'stop_codon':
                    if mRNA:
                        mRNA.add_stop_codon(rec)
                elif rec.getType() in ['splice3',  'splice5']:
                    ss = str(dna_index[rec.get_seqID()][rec.get_start()-1:rec.get_end()].seq)
                    if rec.getType() == 'splice3':
                        d_a.append(ss)
                    else:
                        d_a.insert(0,  ss)
                    if len(d_a) == 2:
                        if d_a in [['GT', 'AG'],  ['GC',  'AG'],  ['AT',  'AC'],  ['AG',  'GT'],  ['AG',  'GC'],  ['AC',  'AT']]:
                            strand = '+'
                        elif d_a in [['CT', 'AC'],  ['CT',  'GC'],  ['GT',  'AT'], ['AC', 'CT'], ['GC', 'CT'],  ['AT',  'GT']   ]:
                                strand = '-'
                        else:
                            raise ValueError('Unrecognized donor-acceptor pair: %s' % str(d_a))
                        d_a = []
                        if curr_strand in ['.',  strand]:
                            curr_strand = strand
                        else:
                            # Separate fused transcripts
                            curr_gene,  new_gene = split_gene(curr_gene)
                            output_curr_gene(curr_gene,  curr_strand,  output,  dna_index)
                            curr_gene = new_gene
                            curr_strand = strand
                            mRNA = curr_gene.get_transcripts()[0]
                elif rec.getType() == 'similarity': # alignment summary line
                    segments = []
                    for a_f in attr_fields:
                        if a_f.strip().startswith('Align'):
                            align_fields = a_f.strip().split()
                            if len(segments) == 0:
                                a_start = int(align_fields[1])
                                if a_start == curr_gene.get_start():
                                    a_strand = '+'
                                elif a_start == curr_gene.get_end() + 1:
                                    a_strand = '-'
                                elif curr_gene.get_ID().endswith('_2'):
                                    # alignment has been split; setting of curr_strand should be correct
                                    a_strand = curr_strand
                                else:
                                    a_strand = '.'
                                if curr_strand in [a_strand,  '.']:
                                    curr_strand = a_strand
                                else:
                                    raise ValueError('Strand conflict for ' + curr_gene.get_ID() )
                            seg_start = int(align_fields[2])
                            seg_len = int(align_fields[3])
                            seg_end = seg_start + seg_len
                            if segments and segments[-1][1] == seg_start:
                                segments[-1][1] = seg_end
                            else:
                                segments.append([seg_start,  seg_end])
                    if len(segments) < 1:
                        raise ValueError('No alignment information for ' + curr_gene.get_ID() )
                    elif len(segments) > 1:
                        raise ValueError('Discontinuous alignment for ' + curr_gene.get_ID() )
                    elif segments[0][0] > 1:
                        raise ValueError('Alignment missing start of ' + curr_gene.get_ID() )
        except Exception,  e:
            print >> sys.stderr,  e,  line.strip()
            curr_gene = None
    if curr_gene:
        print >> output,  curr_gene

    output.close()
#    print sys.argv[0],  'done.'
