"""Add homology scores derived from blastp to covered exons in gene models in gff3 format

Created: 2011-05-03
Author: ian
"""
import sys
from gff3Iterator import GFF3Iterator
import gzip
from collections import defaultdict
import argparse

DESCRIPTION = 'Add homology scores derived from blastp to covered exons in gene models in gff3 format'
VERSION = '5.1'

class Hit(object):
    def __init__(self, start, end, score):
        object.__init__(self)
        self._start = int(start)
        self._end = int(end)
        self._score = float(score)

    def __repr__(self):
        return 'Hit(%d, %d, %f)' % (self._start,  self._end,  self._score)

    def get_start(self):
        return self._start

    def get_end(self):
        return self._end

    def get_score(self):
        return self._score

    def set_start(self, value):
        self._start = value

    def set_end(self, value):
        self._end = value

    def set_score(self, value):
        self._score = value

    def del_start(self):
        del self._start

    def del_end(self):
        del self._end

    def del_score(self):
        del self._score

    start = property(get_start, set_start, del_start, "start of hit, in bases from the CDS start")
    end = property(get_end, set_end, del_end, "end of hit, in bases from the CDS start")
    score = property(get_score, set_score, del_score, "hit's score")

## end class Hit

def safe_get_score(gff3_rec,  default_score):
    try:
        return float(gff3_rec.get_score())
    except ValueError:
        return default_score

def calc_exon_hit_length(exon, offset, utr5, hit):
    return max(0, min(hit.end, len(exon) + offset) - utr5 - max(0, hit.start,  offset))

def makeHit(homolog, index, scale):
    fields = homolog.split('\t')
    start = 3 * int(fields[index['QUERYSTART']]) - 3
    end = 3 * int(fields[index['QUERYEND']])
    score = 1.5 * (int(fields[index['MATCHES']]) + int(fields[index['SIMILARITIES']])) / (end - start) * scale
    return Hit(start, end, score)

def make_index(header_line):
    index = {}
    for i, key in enumerate(header_line.strip().split('\t')):
        index[key] = i
    return index

def load_blastp_data(blastp):
    """
    blastp is an open blastp output file with fields named in header_line below
    returns:
        homologous - a dictionary of blastp fields keyed by QUERYLOCUS
        index - a dictionary of blastp field indices keyed by field name
    """
    header_line = 'QUERYLOCUS	STATUS	TARGETLOCUS	TARGETDESCRIPTION	SIGNIFICANCE	MATCHES	SIMILARITIES	QUERYLENGTH	TARGETLENGTH	QUERYSTART	QUERYEND	TARGETSTART	TARGETEND	SCORE'
    index = make_index(header_line)
    homologous = defaultdict(list)
    for line in blastp:
        # 'Warning' indicates no hits for query
        if line.startswith('#') or '\tWarning' in line:
            continue
        fields = line.strip().split('\t')
        homologous[fields[index['QUERYLOCUS']]].append(line.strip())
    return homologous, index


def score_transcript(default_score, transcript, homologous, index, scale, max_homologs):
    transcript.set_score(default_score)
    exons = transcript.get_exons()
    exons.sort()
    for exon in exons:
        exon.set_score(default_score)
    if transcript.get_ID() in homologous:
        hits = []
        for homolog in homologous[transcript.get_ID()]:
            hit = makeHit(homolog, index, scale)
            # do insertion sort in decreasing order of score
            if len(hits) == 0:
                hits.append(hit)
            else:
                for i in range(len(hits)):
                    if hit.score > hits[i].score:
                        hits.insert(i, hit)
                        break
                else:
                    hits.append(hit)

        CDS_start = min(transcript.get_CDS_start(), transcript.get_CDS_stop())
        CDS_end = max(transcript.get_CDS_start(), transcript.get_CDS_stop())
        # only use up to max_homologs best hits
        for hit in hits[:max_homologs]:
            offset = 0 # relative to CDS
            for exon in exons:
                if exon.get_start() >= CDS_end:
                    break
                if exon.get_end() <= CDS_start:
                    continue
                utr5 = max(0, CDS_start - exon.get_start())
                exon_hit_len = calc_exon_hit_length(exon, offset, utr5, hit)
                if exon_hit_len > 0:
                    exon_hit_score = hit.score * exon_hit_len / len(exon)
                    exon.set_score(safe_get_score(exon, default_score) + exon_hit_score)
                offset += len(exon) - utr5
            transcript_score = 1
            for exon in exons:
                transcript_score *= safe_get_score(exon, default_score)
            transcript.set_score(transcript_score)


def score_gene(gene, homologous, index, default_score=1.0, scale=0.333, max_homologs=3):
    """
    Add homolog scores to all transcripts of one gene
    :type gene: GFF3Gene
    homologous and index are dictionaries created by load_blastp_data()
    returns gene to emphasize that input parameter is modified by method
    """
    for transcript in gene.get_transcripts():
        score_transcript(default_score, transcript, homologous, index, scale, max_homologs)
    for transcript in gene.get_transcripts():
        gene.set_score(max(safe_get_score(gene, default_score), transcript.get_score()))
    return gene


def do_add_homology_scores(genes, blastp, out, default_score, max_homologs):
    """
    Add homology scores derived from blastp to covered exons in gene models in gff3 format
    genes is a stream of GFF3Genes containing GFF3mRNA and GFF3Exon instances
    blastp is an iterator over blastp output lines with fields
        QUERYLOCUS	STATUS	TARGETLOCUS	TARGETDESCRIPTION	SIGNIFICANCE	MATCHES	SIMILARITIES	QUERYLENGTH
        TARGETLENGTH	QUERYSTART	QUERYEND	TARGETSTART	TARGETEND	SCORE
    out is an open output file
    default_score is a float used as the starting value for all scores
    max_homologs is an int, the maximum number of homologs used for scoring each gene
    """
    # load blastp data
    homologous, index = load_blastp_data(blastp)
    # score models
    scale = float(default_score) / max_homologs
    for gene in genes:
        print >> out, score_gene(gene, homologous, index, default_score, scale, max_homologs)

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in',  dest='input', type=argparse.FileType('r'),  nargs='?',  default = sys.stdin,  help='Path to the gff3 file containing models to be scored; if omitted or -, input is read from stdin')
    argparser.add_argument('--blastp', required=True,   help='Path to the file containing blastp results; may be compressed and end in .gz')
    argparser.add_argument('--out', type=argparse.FileType('w'),  nargs='?',  default = sys.stdout,  help='Path to the output file for scored models; if omitted or -, output is written to stdout')
    argparser.add_argument('--default_score',  type=float,  default = 1.0,  help='Score initially assigned to all models')
    argparser.add_argument('--max_homologs',  type=int,  default = 3,  help='Maximum number of homologs used for scoring')
    return  argparser.parse_args()

if __name__ == '__main__':
    args = get_args()
    input = GFF3Iterator(args.input)
    if args.blastp.endswith('.gz'):
        blastp = gzip.GzipFile(args.blastp)
    else:
        blastp = open(args.blastp)

    do_add_homology_scores(input.genes(), blastp, args.out, args.default_score, args.max_homologs)

    blastp.close()
    args.out.close()
#    print >> sys.stderr, sys.argv[0],  'done.'
