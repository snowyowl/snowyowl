""" Scan through the gene models in a .gff3 file and check that all implied introns have legal acceptors and donors, and consistent orientation within each gene model

Created: 2011-08-01
Author: ian
"""

import sys, os, string
from Bio import SeqIO
from collections import defaultdict
from gff3Iterator import GFF3Iterator
import argparse

DESCRIPTION = 'Scan through the gene models in a .gff3 file and check that all implied introns have legal acceptors and donors, and consistent orientation within each gene model'
VERSION = '5.0'
DONOR_ACCEPTOR_PAIRS = [('GT',  'AG'), ('GC',  'AG'),  ('AT',  'AC') ]

D_A_count = defaultdict(int)

def orientIntrons(introns, genome_seq):
    strand = ['.'] * len(introns)
    for i,  intron in enumerate(introns):
        if intron and len(intron) > 2:
            seq = str(genome_seq[intron[0]][intron[1]:intron[2]].seq)
            if seq:
                seq = seq.upper()
                rc = seq.translate(string.maketrans('ACTG', 'TGAC'))[::-1]
                if (seq[:2],  seq[-2:])  in DONOR_ACCEPTOR_PAIRS:
                    strand[i]= '+'
                elif (rc[:2],  rc[-2:]) in DONOR_ACCEPTOR_PAIRS:
                    strand[i] = '-'
                else:
                    strand[i] = '.'
    return strand


def check_transcript_intron_structure(transcript,  genome_seq, args):
    exons = sorted(transcript.get_exons())
    if len(exons) == 1:
        return True
    introns = []
    strands = []
    for first,  second in zip(exons[:-1],  exons[1:]):
        if first.get_seqID() != second.get_seqID():
            if args.verbose > 0:
                print >> sys.stderr,  transcript.get_ID(), 'Mapping to more than one scaffold'
            transcript.add_attribute('error', 'Mapping to more than one scaffold' )
            return False
        elif second.get_start() - first.get_end() == 1:
            continue # not an intron; probably UTR-CDS boundary
        elif args.min_intron_length <= second.get_start() - first.get_end() - 1 <= args.max_intron_length:
            introns.append( [first.get_seqID(), first.get_end() , second.get_start() - 1])
            seq = str(genome_seq[first.get_seqID()][first.get_end() : second.get_start() - 1].seq)
            if seq:
                seq = seq.upper()
                rc = seq.translate(string.maketrans('ACTG', 'TGAC'))[::-1]
                if (seq[:2],  seq[-2:])  in DONOR_ACCEPTOR_PAIRS:
                    intron_strand = '+'
                    D_A_count[(seq[:2],  seq[-2:])] += 1
                elif (rc[:2],  rc[-2:]) in DONOR_ACCEPTOR_PAIRS:
                    intron_strand = '-'
                    D_A_count[(rc[:2],  rc[-2:])] += 1
                else:
                    intron_strand = '.'
                    d_a = (rc[:2],  rc[-2:]) if first.get_strand() == '-' else (seq[:2],  seq[-2:])
                    D_A_count[d_a] += 1
                    first.add_attribute('error',  'Unlikely donor-acceptor pair %s-%s' % d_a)
                    if args.verbose > 0:
                        print >> sys.stderr,  transcript.get_ID(), 'Unlikely donor-acceptor pair %s-%s' % d_a
                    return False
                strands.append(intron_strand)
        else:
            if args.verbose > 0:
                print >> sys.stderr,  first.get_ID(), 'Excessive gap length',  second.get_start() - first.get_end() - 1
            transcript.add_attribute('error', 'Excessive gap length')
            return False
        strand_sum = 0
        for s in strands:
            if s == '+':
                strand_sum += 1
            elif s == '-':
                strand_sum -= 1
        c_strand = '.'
        if strand_sum > 0 :
            c_strand = '+'
        elif strand_sum < 0:
            c_strand = '-'
        if transcript.get_strand() != c_strand:
            if args.verbose > 0:
                print >> sys.stderr,  'Inconsistent intron orientations (%s) in %s' % ( ','.join(strands), transcript.get_ID())
            transcript.add_attribute('error', 'Inconsistent intron orientations' )
            return False

        for strand_i in strands:
            if strand_i != c_strand:
                if args.verbose > 0:
                        print >> sys.stderr,  'Inconsistent intron orientations (%s) in %s' % ( ','.join(strands), transcript.get_ID())
                transcript.add_attribute('error', 'Inconsistent intron orientations' )
                return False
    return True

def do_check_gene(gene,  genome_seq,  accepts,  rejects,  args):
    accept = False
    for transcript in gene.get_transcripts():
        if check_transcript_intron_structure(transcript,  genome_seq,  args):
            accept = True
    if accept:
        print >> accepts,  gene
        return True
    else:
        print >> rejects,  gene
        return False


def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in',  dest='input', type=argparse.FileType('r'),  nargs='?',  default = sys.stdin,  help='Path to the input gff3 file; if omitted or -, input is read from stdin')
    argparser.add_argument('--genome',  '-g', type=argparse.FileType('r'),  required=True,  help='Path to the genome sequence file in fasta format; required')
    argparser.add_argument('--out', type=argparse.FileType('w'),  nargs='?',  default = sys.stdout,  help='Path to the output file for accepted models; if omitted or -, output is written to stdout')
    argparser.add_argument('--rejects', type=argparse.FileType('w'),  required=True,  help='Path to the output file for rejected models; required')
    argparser.add_argument('--min_intron_length', type=int, default=10,  help='Predicted introns shorter than this are rejected')
    argparser.add_argument('--max_intron_length', type=int, default=2000,  help='Predicted introns longer than this are rejected')
    return  argparser.parse_args()

if __name__ == '__main__':
    args = get_args()
    input = args.input
    genome_seq = SeqIO.to_dict(SeqIO.parse(args.genome,  'fasta'))
    accepts = args.out
    rejects = args.rejects

    accepted = rejected = 0
    for gene in GFF3Iterator(input).genes():
        ok = do_check_gene(gene,  genome_seq,  accepts,  rejects, args)
        if ok:
            accepted += 1
        else:
            rejected += 1
    accepts.close()
    rejects.close()
    print "Accepted",  accepted,  "models; rejected",  rejected,  'models'
    if args.verbose > 1:
        for d_a in D_A_count:
            print d_a,  D_A_count[d_a]
#    print sys.argv[0],  'done.'
