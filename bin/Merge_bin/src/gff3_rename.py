"""Change  gene names and IDs in a gff3 file to sequential numbers

Author: ian
Created: 2011-05-11
"""
import sys, os,  re
from gff3Iterator import GFF3Iterator
from gff3Record import GFF3Gene

transcript_number_pat = re.compile(r'(.+)\.(t\d+)')

def usage(msg):
    if msg:
        print >> sys.stderr,  msg
    print >> sys.stderr,  'Usage: python %s input.gff3 prefix output.gff3' % sys.argv[0]
    sys.exit(1)

def gene_rename(gene,  new_name):
    alias = gene.get_name()
    gene.set_name(new_name)
    gene.set_ID(new_name)
    gene.add_attribute('Alias',  alias)
    transcript_number = 0
    for transcript in gene.get_transcripts():
        transcript_number += 1
        transcript.set_ID('%s.t%d' % (new_name,  transcript_number))
        transcript.set_name(new_name)
        transcript.set_parents(gene.get_ID())
        for exon in transcript.get_exons():
            exon.set_name(new_name)
            exon.set_parents(transcript.get_ID())
            #if exon.has_attribute('ID'):
                #exon.remove_attribute('ID')
            exon.setID(transcript.get_ID()+'.cds')
    return gene

def change_seqID(gene,  new_seqID):
    gene.set_seqID(new_seqID)
    for transcript in gene.get_transcripts():
        transcript.set_seqID(new_seqID)
        for exon in transcript.get_exons():
            exon.set_seqID(new_seqID)
    return gene

def shift_coordinates(gene,  shift):
    gene.set_start(gene.get_start() + shift)
    gene.set_end(gene.get_end() + shift)
    for transcript in gene.get_transcripts():
        transcript.set_start(transcript.get_start() + shift)
        transcript.set_end(transcript.get_end() + shift)
        transcript.set_CDS_start(transcript.get_CDS_start() + shift)
        transcript.set_CDS_stop(transcript.get_CDS_stop() + shift)
        for exon in transcript.get_exons():
            exon.set_start(exon.get_start() + shift)
            exon.set_end(exon.get_end() + shift)
    return gene
    

if __name__ == '__main__':
    try:
        input = open(sys.argv[1])
        prefix = sys.argv[2]
        output = open(sys.argv[3],  'w')
    except Exception,  e:
        usage(e)
    
    gene_number = 0
    for gene in GFF3Iterator(input).genes():
        gene_number += 1
#        print >> output,  gene_rename(gene,  '%s%05d' % (prefix,  gene_number))
        print >> output,  gene_rename(gene,  gene.get_ID())
    
    output.close()
#    print sys.argv[0],  'done'
