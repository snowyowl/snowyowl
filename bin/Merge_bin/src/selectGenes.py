'''Select gene records from a .gff3 file
Created on Jun 26, 2009

@author: ian
'''

import sys
from gff3Record import GFF3Record
import argparse

DESCRIPTION = 'Select gene records from a .gff3 file'
VERSION = '5.0'

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in',  '-i', dest='input',  type=argparse.FileType('r'),  nargs='?',  default = sys.stdin,  help='Path to the input file containing a list of gene names; if omitted or -, input is read from stdin')
    argparser.add_argument('--genes',  '-g',  type=argparse.FileType('r'), required=True,  help='Path to the master genes.gff3 file; required')
    argparser.add_argument('--out',  '-o', type=argparse.FileType('w'),  nargs='?',  default = sys.stdout,  help='Path to the output file; if omitted or -, output is written to stdout')
    return  argparser.parse_args()

if __name__ == '__main__':
    args = get_args()
    pickfile = args.input
    genefile = args.genes
    out = args.out
    picks = set()
    for line in pickfile:
        try:
            picks.add(line.strip().split()[0])
        except:
            continue


    out.write('##gff-version 3\n')
    for line in genefile:
        if line.startswith('#'):
            continue
        linelist = line.rstrip('\n').split('\t')
        gff = GFF3Record(*linelist)
        number = gff.get_name()
        if number in picks:
            print >> out,  gff
    out.close()

