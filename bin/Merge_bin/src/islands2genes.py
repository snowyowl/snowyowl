"""Generate gene models from RNA-Seq read islands

Created: 2011-05-06
Author: ian
"""
import sys, os,  time,  tempfile,  subprocess, re
from gff3Iterator import GFF3Iterator
from gff3Record import GFF3Gene, GFF3Record
from pysam import Tabixfile
from collections import namedtuple
from cStringIO import StringIO
from Bio import SeqRecord, Seq, SeqIO
import argparse

DESCRIPTION = 'Generate gene models from RNA-Seq read islands'
VERSION = '5.0'

gnum_pat = re.compile('(g\d+)')
Splice = namedtuple('Splice', 'seqID start end strand score count')

def get_intron_hints(scaffold, start, end,  orientation,  juncs_tabix):
    splices = []
    try:
        all_juncs = [Splice._make(j.split('\t')[:6]) for j in juncs_tabix.fetch(scaffold, start, end)]
        if orientation in ['+',  '-']:
            splices = [s for s in all_juncs if s.strand == orientation]
        else:
            splices = all_juncs
    except ValueError,  ve:
        print >> sys.stderr,  ve
    intron_hints = StringIO()
    for junc in splices:
        intron_start = int(junc[1]) + 2
        intron_end = int(junc[2])
        intron_attrs = {'source': 'J','priority':'6'}
        intron_score = junc[4] if len(junc) > 4 else '0'
        intron_attrs['mult'] = intron_score
        if len(junc) > 5:
            intron_attrs['mult'] = junc[5]
        intron_hint = GFF3Record(junc[0], 'junc2hints', 'intron', intron_start, intron_end, intron_score, junc[3], '.', intron_attrs )
        if len(junc) > 5:
            intron_hint.set_score(1.0 - float(junc[4]))
        print >> intron_hints,  intron_hint
    return intron_hints.getvalue()

def runAugustus(hint_lines, transcript_start, transcript_end, strand, dna_seq_filename, max_end, training_species,augustus_path, extrinsicCfgFile, args):
    hint_file  = tempfile.NamedTemporaryFile(delete=False)
    hint_filename = hint_file.name
    for line in hint_lines:
        print >> hint_file, line
    hint_file.close()
    region_start = max(1, transcript_start - args.region_pad)
    region_end = min(max_end, transcript_end + args.region_pad)
    augustus_exec = os.path.join(augustus_path,'bin', 'augustus')
    augustus_config = os.path.join(augustus_path,'config')
    augustus_cmd = "%s --genemodel=atleastone --strand=%s --hintsfile=%s --extrinsicCfgFile=%s --AUGUSTUS_CONFIG_PATH=%s --allow_hinted_splicesites=gcag,atac --noInFrameStop=true --gff3=on --protein=on --predictionStart=%d --predictionEnd=%d --species=%s %s" %(augustus_exec, strand, hint_filename, extrinsicCfgFile, augustus_config, region_start, region_end, training_species, dna_seq_filename)
#    print 'Running Augustus...'
#    print augustus_cmd
    augustus_process = subprocess.Popen(augustus_cmd.split(),  stdout=subprocess.PIPE)
    augustus_response = augustus_process.communicate()
    augustus_output = augustus_response[0]
    os.remove(hint_filename)
    return augustus_output

def get_scaffold_lengths(scaffolds_gff3):
    scaffold_len = {}
    for line in open(scaffolds_gff3):
        if line.startswith('#'):
            continue
        fields = line.split('\t')
        if len(fields) > 4:
            scaffold_len[fields[0]] = int(fields[4])
    return scaffold_len

def rename_gff3_line(line,  old_name,  new_name):
    if old_name in line:
        line = line.replace(old_name,  new_name)
        if 'Name=' not in line:
            line += ';Name=%s' % new_name
    return line

def parse_augustus_output(augustus_output):
    gff3_lines = []
    proteins = []
    protein_sequence = ''
    in_protein = False
    for line in augustus_output.split('\n'):
        if not line.startswith('#'):
            gff3_lines.append(line)
        elif in_protein:
            protein_sequence += line.strip()[2:]
            if line.find(']') > -1:
                excess = len(line) - line.find(']')
                protein_sequence = protein_sequence[:-excess] # drop closing ]ff.
                proteins.append(protein_sequence)
                in_protein = False
        elif line.startswith('# protein sequence = ['):
            ps_start = line.index('[') + 1
            protein_sequence = line.strip()[ps_start:]
            in_protein = True
            if line.find(']') > -1:
                excess = len(line) - line.find(']')
                protein_sequence = protein_sequence[:-excess] # drop closing ]ff.
                in_protein = False
    return gff3_lines, proteins

def process_hint_lines(hint_lines, transcript_start, transcript_end, strand, dna_seq_filename, max_end, gene_name, training_species,augustus_path, extrinsicCfgFile, args):
    augustus_output = runAugustus(hint_lines, transcript_start, transcript_end, strand, dna_seq_filename, max_end, training_species,augustus_path, extrinsicCfgFile, args)
    gff3_lines, proteins = parse_augustus_output(augustus_output)
    renamed = []
    protein_records = []
    if proteins:
        for i in range(len(proteins)):
            augustus_name = 'g%d' % (i + 1)
            new_name = '%s.g%d' % (gene_name,  i + 1)
            for gff3 in gff3_lines:
                match = gnum_pat.search(gff3)
                if match and match.group(1) == augustus_name:
                    renamed.append(rename_gff3_line(gff3,  augustus_name, new_name))
            protein_seq = proteins[i]
            protein_rec = SeqRecord.SeqRecord(Seq.Seq(protein_seq), id=new_name+'.t1', description='Predicted protein')
            protein_records.append(protein_rec)
    return renamed, protein_records

def process_group(locus_num, exon_hints,  juncs_tabix,  dna_scaffolds_dir, training_species,augustus_path, extrinsicCfgFile, args):
    exon_hints.sort()
    scaffold = exon_hints[0].get_seqID()
    start = exon_hints[0].get_start()
    end = exon_hints[-1].get_end()
    orientation = '.'
    intron_hints = get_intron_hints(scaffold, start, end,  orientation,  juncs_tabix)
    hints = '\n'.join([str(hint) for hint in exon_hints]) + '\n' + intron_hints
#    print '%s - %d exon hints and %d intron hints' %(time.asctime(),  len(exon_hints),  len(intron_hints.splitlines()))
    strand = 'both'
    dna_seq_filename = '%s/%s.fasta' % (dna_scaffolds_dir, scaffold)
    gene_name = 'Locus%05d' % locus_num
    max_end = scaffold_len[scaffold]
    augustus_gff3, protein_rec = process_hint_lines(hints.splitlines(), start, end, strand, dna_seq_filename, max_end, gene_name, training_species,augustus_path, extrinsicCfgFile,  args)
#    print '%s - %d gene predictions' %(time.asctime(),  len(protein_rec))
    return augustus_gff3, protein_rec

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in',  dest='input', type=argparse.FileType('r'),  nargs='?',  default = sys.stdin,  help='Path to the islands.gff3 input file; if omitted or -, input is read from stdin')
    argparser.add_argument('--genome',  '-g', required=True,  help='Path to a directory containing individual genome scaffold sequence files in fasta format; required')
    argparser.add_argument('--juncs',  '-j', required=True,  help='Path to the tabix-indexed splice junctions file; required')
    argparser.add_argument('--augustus_name',  '-n', required=True,  help='Augustus species name; required')
    argparser.add_argument('--out',  '-o', type=argparse.FileType('w'),  nargs='?',  default = sys.stdout,  help='Path to the output file; if omitted or -, output is written to stdout')
    argparser.add_argument('--augustus_path', required=True,  help='Path to installed Augustus directory; required')
    argparser.add_argument('--extrinsicCfgFile', required=True,  help='Path to Augustus extrinsicCfgFile; required')
    argparser.add_argument('--max_intragene_gap', type=int,  default=500,  help='Any coverage gap longer than this causes a new locus to be started')
    argparser.add_argument('--region_pad', type=int,  default=0,  help='Regions for Augustus gene prediction are extended by this much at each end')
    return  argparser.parse_args()

if __name__ == '__main__':
    args = get_args()
    islands = GFF3Iterator(args.input).contigs()
    dna_seq_dir = args.genome
    juncs_tabix = Tabixfile(args.juncs)
    augustus_name = args.augustus_name
    output = args.out
    augustus_path = args.augustus_path
    extrinsicCfgFile = args.extrinsicCfgFile

    scaffolds_gff3 = os.path.join(os.path.split(dna_seq_dir)[0], 'scaffolds.gff3')
    scaffold_len = get_scaffold_lengths(scaffolds_gff3)

    exon_hints = []
    locus_num = 0
    prev_end = None
    curr_scaffold = None
    for island in islands:
        if not curr_scaffold or island.get_seqID() != curr_scaffold:
            prev_end = None
            curr_scaffold = island.get_seqID()
        if not prev_end or island.get_start() - prev_end > args.max_intragene_gap :
            locus_num += 1
            if exon_hints:
                augustus_gff3, protein_rec = process_group(locus_num, exon_hints,  juncs_tabix,  dna_seq_dir,  augustus_name, augustus_path, extrinsicCfgFile,  args)
                for line in augustus_gff3:
                    print >> output,  line
                print >> output,  '###'
                exon_hints = []

        for contig in island.get_transcripts():
            for match in contig.get_exons():
                match.set_type('exonpart')
                match.set_attributes({'source':'P', 'priority': 5, 'group': contig.get_name()})
                exon_hints.append(match)
        prev_end = island.get_end()

    if exon_hints:
        locus_num += 1
        augustus_gff3, protein_rec = process_group(locus_num, exon_hints,  juncs_tabix,  dna_seq_dir,  augustus_name, augustus_path, extrinsicCfgFile,  args)
        for line in augustus_gff3:
            print >> output,  line
        print >> output,  '###'

    output.close()
#    print sys.argv[0],  'done.'
