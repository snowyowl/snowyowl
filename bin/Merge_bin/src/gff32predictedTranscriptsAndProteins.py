""" From a .gff3 file, produce fasta files with predicted mRNA and protein sequences

Created on Oct 28, 2010
Created by ian
"""

import sys, os
from Bio import SeqIO, Seq, SeqRecord, Alphabet
from Bio.Data.CodonTable import TranslationError
from gff3Iterator import GFF3Iterator
import argparse

DESCRIPTION = 'From a .gff3 file, produce fasta files with predicted mRNA and protein sequences'
VERSION = '5.0'

def wrap_sequence(sequence, wrap_length=80):
    """sequence is a string.
    Returns a string containing newlines at intervals of wrap_length
    """
    lines = []
    for i in range(0, len(sequence), wrap_length):
        segment = sequence[i : i + wrap_length]
        lines.append(segment)
    return '\n'.join(lines)

def get_transcript_ID(fields):
    return fields[8]['transcript_id']

def get_transcript_sequence(transcript_gff3, genome_index):
    """Return a SeqRecord object with the DNA sequence of a GFFmRNA object."""
    record = transcript_gff3.get_transcript_sequence(genome_index)
    record.id = transcript_gff3.getID()
    record.name = transcript_gff3.get_name()
    record.description = 'Predicted transcript'
    return record

def get_CDS_sequence(transcript_gff3, genome_index):
    """Return a SeqRecord object with the coding DNA sequence of a GFFmRNA object."""
    cds = transcript_gff3.extract_CDS()
    record = cds.get_transcript_sequence(genome_index)
    record.id = transcript_gff3.getID()
    record.name = transcript_gff3.get_name()
    record.description = 'Predicted CDS'
    return record

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in', '-i',   dest='input', type=argparse.FileType('r'),  nargs='?',  default = sys.stdin,  help='Path to the input models.gff3 file; if omitted or -, input is read from stdin')
    argparser.add_argument('--genome',  '-g',  dest='genome', type=argparse.FileType('r'),  required=True, help='Path to the genome sequence fasta file; required')
    argparser.add_argument('--CDS', type=argparse.FileType('w'),  required=True,  help='Path to the transcript sequence output file; required')
    argparser.add_argument('--protein',  '-p', type=argparse.FileType('w'),  required=True,  help='Path to the protein sequence output file; required')
    return  argparser.parse_args()

if __name__ == '__main__':
    args = get_args()
    input = GFF3Iterator(args.input)
    genome_index = SeqIO.to_dict(SeqIO.parse(args.genome,  'fasta'))
    output_RNA = args.CDS
    output_AA = args.protein

    for gene in input.genes():
        for transcript in gene.get_transcripts():
            try:
                record = get_CDS_sequence(transcript, genome_index)
                output_RNA.write(record.format('fasta') + '\n')
                try:
                    protein_seq = str(record.seq.translate(cds=True))
                    protein_id = record.id.replace('.mRNA',  '.translation')
                    print >> output_AA,  '>%s Predicted protein product' % protein_id
                    print >> output_AA,  wrap_sequence(protein_seq)
                except TranslationError,  te:
                    print >> sys.stderr,  transcript.get_name(),  te
            except Exception, ex:
                print >> sys.stderr, gene.get_name(), ex

    output_RNA.close()
    output_AA.close()
#    print 'Done!'

