"""Process replacement.loci.txt into a table of replaced genes and their replacements

Created: 2012-04-25
Author: ian
"""
import sys, os
#from gff3Record import GFF3Record

def usage(msg=None):
    if msg:
        print >> sys.stderr,  msg
    print >> sys.stderr,  'Usage: python %s   loci.txt output.txt' % sys.argv[0]
    sys.exit(1)

def is_replaced(gene_name):
    return 'Aspni3' not in gene_name

if __name__ == '__main__':
    try:
        loci_file = open(sys.argv[1])
        output = open(sys.argv[2],  'w')
    except Exception,  e:
        usage(e)

    for line in loci_file:
        fields = line.strip().split('\t')
        if len(fields) > 6:
            position = fields[1]
            loci = fields[6:]
            replaced = []
            replacing = []
            for locus in loci:
                if is_replaced(locus):
                    replaced.append(locus)
                else:
                    replacing.append(locus)
            replaced.sort()
            replacing.sort()
            print >> output,  '%s\t%s\t%s' % (position, ', '.join(replaced),  ', '.join(replacing))

    output.close()
#    print sys.argv[0],  'done.'
