"""Find shortest prefix that distinguishes IDs in 1st input from names in 2nd input

Created: 2012-05-10
Author: ian
"""
import sys, os, re
from gff3Record import GFF3Record
import argparse

DESCRIPTION = 'Find shortest prefix that distinguishes IDs in 1st input from names in 2nd input'
VERSION = '5.0'

id_pat = re.compile(r'ID=([^;]+);')
def is_replaced(gene_name,  prefix):
    return gene_name.startswith(prefix)

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--older',  type=argparse.FileType('r'),  required=True,  help='Path to the input file containing replaced gene models in gff3 format; required')
    argparser.add_argument('--newer',  type=argparse.FileType('r'),  required=True,  help='Path to the input file containing replacing gene models in gff3 format; required')
    return  argparser.parse_args()

if __name__ == '__main__':
    args = get_args()
    old_line = args.older.readline()
    new_line = args.newer.readline()

    prefix = None

    id_match = id_pat.search(old_line)
    if id_match:
        old_id = id_match.group(1)
    else:
        old_id = ''

    id_match = id_pat.search(new_line)
    if id_match:
        new_id = id_match.group(1)
    else:
        new_id = ''

    if old_id and new_id:
        for i in range(1,  min(len(old_id),  len(new_id))):
            if not new_id.startswith(old_id[:i]):
                prefix = old_id[:i]
                break

    if prefix:
        print prefix


