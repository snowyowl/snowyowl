"""
Miscellaneous formatting functions
Created on Dec 10, 2009

@author: ian
"""

def commas(n):
    """ Convert an int to a string containing commas at every third position"""
    string = str(int(n))
    thous = []
    off = len(string) % 3
    if off > 0:
        thous = [string[:off]]
    for i in range(off,  len(string), 3):
        thous.append(string[i:i+3])
    return ','.join(thous)

def wrap_alignment(alignment, wrap_length=80):
    """alignment is a list of strings, usually of the same length with a relationship between corresponding positions in each string.
    Returns a list of lists of shorter (<= wrap_length) strings representing consecutive segments of the alignment.
    """
    result = []
    length = max((len(line) for line in alignment))
    for i in range(0, length, wrap_length):
        segment = [line[i : i + wrap_length] for line in alignment]
        result.append(segment)
    return result

def wrap_sequence(sequence, wrap_length=80):
    """sequence is a string.
    Returns a string containing newlines at intervals of wrap_length
    """
    lines = []
    for i in range(0, len(sequence), wrap_length):
        segment = sequence[i : i + wrap_length]
        lines.append(segment)
    return '\n'.join(lines)

