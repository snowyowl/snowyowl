"""Identify alternate and antisense splice junctions

Created:
Author: ian
"""
import sys, os
import argparse

DESCRIPTION = 'Identify alternate and antisense splice junctions'
VERSION = '5.0'

class ClassifiedSplice(object):
    def __init__(self, seqID, left, right, strand, readthrough=0.0,  count=0, anchor_diversity=0,  donor_acceptor='',  left_anchor=0,  right_anchor=0, type='regular'):
        object.__init__(self)
        self.seqID =  seqID
        self.left =  int(left)
        self.right =  int(right)
        self.strand =  strand
        self.readthrough =  float(readthrough)
        self.count =  int(count)
        self.anchor_diversity =  int(anchor_diversity)
        self.donor_acceptor = donor_acceptor
        self.left_anchor =  int(left_anchor)
        self.right_anchor = int(right_anchor)
        self.type = type

    def _get_field_list(self):
        return [self.seqID, self.left, self.right, self.strand, self.readthrough, self.count, self.anchor_diversity, self.donor_acceptor, self.left_anchor, self.right_anchor, self.type]

    def __str__(self):
        return '\t'.join([str(f) for f in self._get_field_list()])

    def __repr__(self):
        return 'ClassifiedSplice(%s)' % str(self)

    def __len__(self):
        return self.right - self.left - 1

    def get_seq_id(self):
        return self.__seqID


    def get_left(self):
        return self.__left


    def get_right(self):
        return self.__right


    def get_strand(self):
        return self.__strand


    def get_readthrough(self):
        return self.__readthrough


    def get_count(self):
        return self.__count


    def get_anchor_diversity(self):
        return self.__anchor_diversity


    def get_donor_acceptor(self):
        return self.__donor_acceptor


    def get_left_anchor(self):
        return self.__left_anchor


    def get_right_anchor(self):
        return self.__right_anchor


    def get_type(self):
        return self.__type


    def set_seq_id(self, value):
        self.__seqID = value


    def set_left(self, value):
        self.__left = value


    def set_right(self, value):
        self.__right = value


    def set_strand(self, value):
        self.__strand = value


    def set_readthrough(self, value):
        self.__readthrough = value


    def set_count(self, value):
        self.__count = value


    def set_anchor_diversity(self, value):
        self.__anchor_diversity = value


    def set_donor_acceptor(self, value):
        self.__donor_acceptor = value


    def set_left_anchor(self, value):
        self.__left_anchor = value


    def set_right_anchor(self, value):
        self.__right_anchor = value


    def set_type(self, value):
        self.__type = value


    def del_seq_id(self):
        del self.__seqID


    def del_left(self):
        del self.__left


    def del_right(self):
        del self.__right


    def del_strand(self):
        del self.__strand


    def del_readthrough(self):
        del self.__readthrough


    def del_count(self):
        del self.__count


    def del_anchor_diversity(self):
        del self.__anchor_diversity


    def del_donor_acceptor(self):
        del self.__donor_acceptor


    def del_left_anchor(self):
        del self.__left_anchor


    def del_right_anchor(self):
        del self.__right_anchor


    def del_type(self):
        del self.__type

    seqID = property(get_seq_id, set_seq_id, del_seq_id, "seqID's docstring")
    left = property(get_left, set_left, del_left, "left's docstring")
    right = property(get_right, set_right, del_right, "right's docstring")
    strand = property(get_strand, set_strand, del_strand, "strand's docstring")
    readthrough = property(get_readthrough, set_readthrough, del_readthrough, "readthrough's docstring")
    count = property(get_count, set_count, del_count, "count's docstring")
    anchor_diversity = property(get_anchor_diversity, set_anchor_diversity, del_anchor_diversity, "anchor_diversity's docstring")
    donor_acceptor = property(get_donor_acceptor, set_donor_acceptor, del_donor_acceptor, "donor_acceptor's docstring")
    left_anchor = property(get_left_anchor, set_left_anchor, del_left_anchor, "left_anchor's docstring")
    right_anchor = property(get_right_anchor, set_right_anchor, del_right_anchor, "right_anchor's docstring")
    type = property(get_type, set_type, del_type, "type's docstring")


def classify_last_2(splice_list):
    if len(splice_list) < 2:
        return splice_list
    if (splice_list[-2].left <= splice_list[-1].left <= splice_list[-2].right) or (splice_list[-2].left <= splice_list[-1].right <= splice_list[-2].right):
        if splice_list[-2].strand == splice_list[-1].strand:
            splice_list[-2].type = 'alternative'
            splice_list[-1].type = 'alternative'
        elif splice_list[-1].count < splice_list[-2].count:
            splice_list[-1].type = 'antisense'
        elif splice_list[-2].count < splice_list[-1].count:
            splice_list[-2].type = 'antisense'
        else:
            print >> sys.stderr, '[classify_juncs.main] Overlapping splices of unknown type', splice_list[-2], splice_list[-1]
    return splice_list

def classify_last_3(splice_list, args):
    if len(splice_list) > 2:
        if (splice_list[-2].strand != splice_list[-1].strand) and (splice_list[-2].strand != splice_list[-3].strand):
            if (min(splice_list[-1].left - splice_list[-2].right, splice_list[-2].left - splice_list[-3].right) < args.antisense_radius) and (splice_list[-2].readthrough > 0.5):
                splice_list[-2].type = 'antisense'
    return splice_list


def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in',  dest='input', type=argparse.FileType('r'),  nargs='?',  default = sys.stdin,  help='Path to the input observed junctions file; if omitted or -, input is read from stdin')
    argparser.add_argument('--out', type=argparse.FileType('w'),  nargs='?',  default = sys.stdout,  help='Path to the output classified junctions file; if omitted or -, output is written to stdout')
    argparser.add_argument('--antisense_radius', type=int, default=100,  help='Maximum distance between an antisense intron and a regular intron')
    return  argparser.parse_args()

if __name__ == '__main__':
    args = get_args()
    input = args.input
    output = args.out

    curr_seqID = None
    splices = []
    for line in input:
        try:
            next_splice = ClassifiedSplice(*line.strip().split('\t'))
            if next_splice.seqID != curr_seqID:
                if splices:
                    for splice in splices:
                        print >> output, splice
                splices = []
                curr_seqID = next_splice.seqID
            if splices and (next_splice.left < splices[-1].left):
                raise ValueError('Splices out of order: %s > %s' % (splices[-1], next_splice))
            splices.append(next_splice)
            if len(splices) > 1:
                splices = classify_last_2(splices)
            if len(splices) > 2:
                splices = classify_last_3(splices, args)
        except ValueError, ve:
            print >> sys.stderr, '[classify_juncs.main]', ve, line.strip()
    if splices:
        for splice in splices:
            print >> output, splice

    output.close()
#    print sys.argv[0],  'done.'
