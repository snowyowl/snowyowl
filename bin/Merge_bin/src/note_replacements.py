"""Annotate gff3 records of replaced gene models with the IDs of their replacements

Created: 2011-07-18
Author: ian
"""
import sys, os
from gff3Record import GFF3Record
import argparse

DESCRIPTION = 'Annotate gff3 records of replaced gene models with the IDs of their replacements'
VERSION = '5.0'

def is_replaced(gene_name,  prefix):
    return gene_name.startswith(prefix)

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--loci',  '-l',  type=argparse.FileType('r'), required=True,  help='Path to the input file containing a list of gene loci, as produced by merge_models.py; required')
    argparser.add_argument('--in',  '-i', dest='input',  type=argparse.FileType('r'),  nargs='?',  default = sys.stdin,  help='Path to the input file containing replaced gene models in gff3 format; if omitted or -, input is read from stdin')
    argparser.add_argument('--out',  '-o', type=argparse.FileType('a'),  nargs='?',  default = sys.stdout,  help='Path to the output file; if omitted or -, output is written to stdout')
    argparser.add_argument('--replaced_prefix',  '-p', required=True,  help='Prefix of names of genes from older model set; required')
    return  argparser.parse_args()

if __name__ == '__main__':
    args = get_args()
    loci_file = args.loci
    input = args.input
    output = args.out
    marker = args.replaced_prefix

    replacement = {}
    if args.verbose > 1:
        print >> sys.stderr,  'Loading replacements...'
    for line in loci_file:
        fields = line.strip().split('\t')
        if len(fields) > 6:
            loci = fields[6:]
            replaced = []
            replacing = []
            for locus in loci:
                if is_replaced(locus, marker):
                    replaced.append(locus)
                else:
                    replacing.append(locus)
            replaced.sort()
            replacing.sort()
            one_to_one = len(replaced) == len(replacing)
            for locus in replaced:
                if not replacing:
                    replacement[locus] = 'None'
                elif one_to_one:
                    replacement[locus] = replacing.pop(0)
                else:
                    replacement[locus] = ','.join(replacing)

    if args.verbose > 1:
        print >> sys.stderr, 'Processing replaced genes...'
    for line in input:
        if line.startswith('#'):
            continue
        try:
            gff3 = GFF3Record(*line.strip().split('\t'))
            if gff3.getType() == 'gene':
                replaced = gff3.getID()
                if replaced in replacement:
                    replacing = replacement[replaced]
                else:
                    replacing = 'None'
                gff3.add_attribute("Replacement",  replacing)
                print >> output,  gff3
        except Exception,  e:
            print >> sys.stderr, 'Not a gff3 record:',  line.strip()


    output.close()
#    print >> sys.stderr,  sys.argv[0],  'done.'
