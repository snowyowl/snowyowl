"""Find the longest ORFs in a set of gene models in GFF3 format

Created:
Author: ian
"""
import sys, os
from gff3Iterator import GFF3Iterator
from Bio import SeqIO,  Seq

def usage(msg=None):
    if msg:
        print >> sys.stderr,  msg
    print >> sys.stderr,  'Usage: python %s  input genome.fa  output' % sys.argv[0]
    sys.exit(1)

def find_longest_ORF_in_sequence(rna_seq):
    best_len = 0
    best_start = -1
    best_stop = -1
    for frame in range(3):
        aa = str(rna_seq[frame:].seq.translate())
        start = stop = 0
        while (-1 < start < len(aa)) and (stop < len(aa)):
            next_start = aa.find('M',  start)
            if next_start == -1:
                start = next_start
            else:
                next_stop = aa.find('*',  next_start)
                if next_stop == -1:
                    next_stop = len(aa) +1
                aa_len = next_stop - next_start
                if aa_len > best_len:
                    best_len = aa_len
                    best_start = 3 * next_start + frame
                    best_stop = 3 * next_stop + frame
                start = next_stop + 1
    return best_start,  best_stop

def find_longest_ORF_in_transcript(transcript,  dna_index):
    orf = transcript
    rna_seq = transcript.get_transcript_sequence(dna_index)
    start,  stop = find_longest_ORF_in_sequence(rna_seq)
    if start > -1 :
        orf.set_CDS_start(orf.get_genomic_coordinate(start))
    if stop > -1:
        orf.set_CDS_stop(orf.get_genomic_coordinate( stop+2))
    return orf

if __name__ == '__main__':
    try:
        input = GFF3Iterator(open(sys.argv[1]))
        dna = SeqIO.index(sys.argv[2],  'fasta')
        output = open(sys.argv[3],  'w')
    except Exception,  e:
        usage(e)

    for gene in input.genes():
        for transcript in gene.get_transcripts():
            transcript = find_longest_ORF_in_transcript(transcript,  dna)
        print >> output,  gene

    output.close()
#    print sys.argv[0],  'done.'
