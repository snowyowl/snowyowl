'''Extract the splice junctions in a SAM alignment file into a .juncs file
Created on Sep 25, 2010

@author: ian
'''

import sys, os,  re
from collections import defaultdict
from Bio.Seq import reverse_complement
from Bio import SeqIO
import pysam
from pysam import Samfile
from samRead import SAMRead, SAMHeader, SAMReadFileIterator
import argparse

DESCRIPTION = 'Extract the splice junctions in a SAM alignment file into a .juncs file'
VERSION = '5.0'

base_pat = re.compile('[ACGT]')

class IntronSupport(object):
    '''Summarizes evidence for existence of a splice junction
    '''
    def __init__(self,  scaffold,  left,  right,  strand):
        object.__init__(self)
        self.scaffold = scaffold
        self.left = int(left)
        self.right = int(right)
        self.strand = strand
        self.donor_seq = None
        self.acceptor_seq = None
        self.left_anchors = []
        self.right_anchors = []

    def add_anchors(self,  left_position,  right_position):
        self.left_anchors.append(int(left_position))
        self.right_anchors.append(int(right_position))

    def get_count(self):
        return len(self.left_anchors)

    def get_anchor_diversity(self):
        return len(set(self.left_anchors))

    def get_leftmost_anchor(self):
        return min(self.left_anchors)

    def get_rightmost_anchor(self):
        return max(self.right_anchors)

    def get_donor_acceptor(self, genome_seq=None):
        if not (self.donor_seq and self.acceptor_seq) and genome_seq:
            intron_seq = genome_seq[self.scaffold][self.left:self.right].seq
            if self.strand == '-':
                intron_seq = reverse_complement(intron_seq)
            self.donor_seq = intron_seq[1:3]
            self.acceptor_seq = intron_seq[-3:-1]
        return  '%s.%s' % (self.donor_seq,  self.acceptor_seq)

def get_segment_mapping_quality(sam,  cursor,  posn,  seg_len,  genomic_seq, args):
    if seg_len < args.min_anchor_score:
        return 0
    seg_score = 0
    # Save time by parsing MD tag if present
    md = None
    for tag in sam.tags:
        if tag.startswith('MD:Z:'):
            md = tag[5:]
            break
    if md:
        md_parts = base_pat.split(md)
        prefix = cursor
        length = seg_len
        for part in md_parts:
            part_len = int(part)
            if prefix >= part_len:
                prefix -= part_len
            else:
                matches = min(part_len - prefix,  length)
                seg_score += matches
                prefix = 0
                length -= matches
                if length < 1:
                    break
                seg_score -= args.anchor_mismatch_penalty
                length -= 1
    else:
        # Compare read sequence to genomic sequence
        ref_seq = genomic_seq[sam.rname][posn : posn + seg_len ].seq
        read_seg = sam.seq[cursor : cursor + seg_len]
        for i in range(seg_len):
            if read_seg[i] == ref_seq[i]:
                seg_score += 1
            else:
                seg_score -= args.anchor_mismatch_penalty
    return seg_score


def doSAM2juncs(sam_read_iterator, genomic_seq, juncs_file,  cover_tabix,  args):
    junction_counts = {}
    curr_rname = ''
    line_count = 0
    for sam in sam_read_iterator:
        line_count += 1
        if line_count % 500000 == 0:
            if args.verbose > 1:
                print line_count, len(junction_counts)
        try:
            if "N" in sam.cigar and not ("I" in sam.cigar or "D" in sam.cigar):
                residue = sam.cigar
                posn = sam.pos
                cursor = 0
                seg_score = 0
                while residue and 'N' in residue:
                    try:
                        parts = residue.partition('N')
                        subparts = parts[0].partition('M')
                        left = posn + int(subparts[0]) - 1 # 1 for backing up to last base before intron
                        right = left + int(subparts[2]) + 1
                        residue = parts[2]
                        strand = '.'
                        for tag in sam.tags:
                            if tag.startswith('XS:A:'):
                                strand = tag[-1]
                                break
                        # check segment mapping quality
                        if seg_score < args.min_anchor_score:
                            #need to check left segment mapping quality
                            seg_len = int(subparts[0])
                            seg_score = get_segment_mapping_quality(sam,  cursor,  posn,  seg_len,  genomic_seq,  args)
                            if seg_score < args.min_anchor_score:
                                break
                        # At this point, left segment has adequate mapping quality
                        # Now check mapping quality of right segment
                        cursor += int(subparts[0])
                        seg_len = int(residue.split('M')[0])
                        seg_score = get_segment_mapping_quality(sam,  cursor,  right,  seg_len,  genomic_seq, args)
                        if seg_score < args.min_anchor_score:
                            break
                        junction = (sam.rname, left, right, strand)
                        right_anchor = right + int(residue.split('M')[0])
                        if junction not in junction_counts:
                            junction_counts[junction] = IntronSupport(*junction)
                            junction_counts[junction].get_donor_acceptor(genomic_seq)
                        junction_counts[junction].add_anchors(posn, right_anchor)
                        posn = right
                    except Exception, e:
                        print >> sys.stderr, e, residue
                        break
        except Exception, e:
            print >> sys.stderr, e, sam

    if len(junction_counts) > 0 :
        junctions = junction_counts.keys()
        junctions.sort()
        for junc in junctions:
            support = junction_counts[junc]
            area = 0
            try:
                wiglines = cover_tabix.fetch(junc[0],  junc[1] +2,  junc[2])
                for line in wiglines:
                    block = line.strip().split('\t')
                    length = min(int(block[2]),  junc[2]) - max(int(block[1]),  junc[1]) + 1
                    area += length * int(block[3])
            except ValueError,  ve:
                print >> sys.stderr,  ve
            depth = float(area / (junc[2] - junc[1] - 1))
            readthrough = depth / (depth +  support.get_count())
            out_list = [str(f) for f in junc + (readthrough,  support.get_count(), support.get_anchor_diversity(),  support.get_donor_acceptor(),  support.left - support.get_leftmost_anchor(),  support.get_rightmost_anchor() - support.right)]
            print >> juncs_file, '\t'.join(out_list)


def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,  help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--sam',  '-s', required=True,  help='Path to input SAM/BAM file; required')
    argparser.add_argument('--genome',  '-g', type=argparse.FileType('r'),  required=True,  help='Path to the genome sequence file in fasta format; required')
    argparser.add_argument('--coverage',  '-c', required=True,  help='Path to the tabix-indexed read coverage file; required')
    argparser.add_argument('--out',  '-o', type=argparse.FileType('w'),  nargs='?',  default = sys.stdout,  help='Path to the output splice junctions file; if omitted or -, output is written to stdout')
    argparser.add_argument('--min_anchor_score', type=float, default=3.0,  help='Minimum anchor score for a spliced read to be used as evidence of a splice junction')
    argparser.add_argument('--anchor_mismatch_penalty', type=float, default=3.0,  help='Subtracted from anchor score for every base mismatch')
    return  argparser.parse_args()

if __name__ == '__main__':
    args = get_args()
    sam_filename = args.sam
    sam_iterator = SAMReadFileIterator(sam_filename).iterator()
    genomic_seq = SeqIO.to_dict(SeqIO.parse(args.genome, 'fasta'))
    cover_tabix = pysam.Tabixfile(args.coverage)
    juncs_file = args.out

    doSAM2juncs(sam_iterator, genomic_seq, juncs_file,  cover_tabix, args)

    juncs_file_name = juncs_file.name
    juncs_file.close()
    if args.verbose > 1:
        print "Saved  junctions to %s" % (juncs_file_name)
