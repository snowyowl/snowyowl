import glob

__author__ = 'ian'

import sys, os
this_dir = os.path.dirname(__file__)
src = os.path.dirname(this_dir)
sys.path.append(src)
import argparse, subprocess, tempfile,glob
from run_blast_parallel import do_parallel_blastp

DESCRIPTION = 'Accelerate blastp for families of similar proteins by selecting targets from preliminary search with family representatives'
VERSION = '0.1'

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,
        help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in', '-i', dest='input', required=True,
        help='Path to the input query file')
    argparser.add_argument('--out', '-o', type=argparse.FileType('w'), required = True,
        help='Path to the output file')
    argparser.add_argument('--target', '-t', required=True, help='Blast database filename')
    argparser.add_argument('--processes', '-p', type=int, default=2, help='Maximum number of processes to use')
    return  argparser.parse_args()


def cluster_proteins(input):
    cluster_name = get_temp_filename()
    cmd = 'cd-hit -i %s -o %s' % (input, cluster_name)
    cmd_list = cmd.split()
    retcode = subprocess.call(cmd_list)
    if retcode != 0:
        raise RuntimeError('Error in cd-hit')
    return cluster_name

def get_temp_filename():
    """
    Return the name of a new empty temporary file
    :return: filename
    :rtype: string
    """
    return tempfile.mkstemp()[1]

def extract_selected_targets(target, hit_ids):
    hits = get_temp_filename()
    blastdbcmd = 'blastdbcmd -db %s -dbtype prot -entry_batch %s -out %s' % (target, hit_ids, hits)
    retcode = subprocess.call(blastdbcmd.split())
    if retcode != 0:
        raise RuntimeError('Error in blastdbcmd')
    hit_db = get_temp_filename()
    cmd = 'makeblastdb -dbtype prot -out %s -in %s' % (hit_db, hits)
    retcode = subprocess.call(cmd.split())
    if retcode != 0:
        raise RuntimeError('Error in makeblastdb')
    os.unlink(hits)
    return hit_db

def do_2stage_blastp(input, target, out, processes):
    # cluster query proteins
    representatives = cluster_proteins(input)

    # search full database with cluster representatives
    selection = tempfile.NamedTemporaryFile(mode='w', delete=False)
    selected = selection.name
    do_parallel_blastp(representatives,selection,target,processes)
    selection.close()
    for fn in glob.iglob(representatives + '*'):
        os.unlink(fn)

    # copy targets hit by representatives into a new blast database
    hit_ids = set([line.split('\t')[3] for line in open(selected)])
    os.unlink(selected)
    id_filename = get_temp_filename()
    with open(id_filename,'w') as id_file:
        for id in hit_ids:
            print >> id_file, id
    hits = extract_selected_targets(target,id_filename)

    # search selected targets with all queries
    do_parallel_blastp(input, out, hits, processes)
    for fn in glob.iglob(hits + '*'):
        os.unlink(fn)
    return

if __name__ == '__main__':
    args = get_args()

    do_2stage_blastp(args.input, args.target, args.out, args.processes)

    args.out.close()
#    print >> sys.stderr, sys.argv[0], 'done.'
