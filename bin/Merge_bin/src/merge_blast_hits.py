"""Merge blastp hits from parallel search of a split database

created: 2011-12-15
author: ian
"""

import sys
from collections import defaultdict

def usage(msg=None):
    if msg:
        print >> sys.stderr, msg
    print >> sys.stderr, 'python %s output_filename input_filename_1 input_filename_2 ...' % (sys.argv[0])
    sys.exit(1)

if __name__ == '__main__':
    try:
        output = open(sys.argv[1],'w')
    except Exception, e:
        usage(e)

    hits = defaultdict(list)
    for input_name in sys.argv[1:]:
        input = open(input_name)
        for line in input:
            if line.startswith('#'):
                continue
            key = line.split('\t', 1)[0]
            hits[key].append(line.strip())
    
    for key in sorted(hits.keys()):
        best_score = 0
        best_hit = hits[key][0]
        for hit in hits[key][1:]:
            score = float(hit.rsplit('\t', 1)[1])
            if score > best_score:
                best_hit = hit
                best_score = score
        print >> output, best_hit
    
    output.close()
