__author__ = 'ian'

import unittest
from add_exon_homology_scores_to_gff3 import *
from gff3Record import GFF3Exon

class TestAddHomologyScores(unittest.TestCase):
    def test_Hit_init(self):
        self.hit = Hit(1, 101, 3.5)
        self.assertEqual(self.hit.start, 1)
        self.assertEqual(self.hit.end, 101)
        self.assertAlmostEqual(self.hit.score, 3.5, delta=0.01)

    def test_safe_get_score_good(self):
        exon = GFF3Exon('seq1', 'test', 1, 101, 0.5, '+', '.', '')
        default_score = 1.0
        score = safe_get_score(exon,default_score)
        self.assertAlmostEqual(score, 0.5,places=1)

    def test_safe_get_score_bad(self):
        exon = GFF3Exon('seq1', 'test', 1, 101, 'None', '+', '.', '')
        default_score = 1.0
        score = safe_get_score(exon,default_score)
        self.assertAlmostEqual(score, default_score,places=1)

    def test_load_blastp_data(self):
#        TODO Implement test for load_blastp_data
        pass

    def test_score_transcript(self):
    #        TODO Implement test for score_transcript
        pass


if __name__ == '__main__':
    unittest.main()
