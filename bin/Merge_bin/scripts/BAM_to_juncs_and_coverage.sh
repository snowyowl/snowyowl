#!/bin/bash
# Derive splice junctions and read coverage files from a mapped reads file

# 2011-09-09 coverage.wig changed to tuque.coverage.wig for compatibility with other scripts

set -o errexit
set -o nounset

# path to mapped reads file
BAM=$1
GENOME=$2

PIPELINE="$(dirname "$( cd "$( dirname "$0" )" && pwd )" )"
OUTPUT_DIR=$( dirname $BAM )

samtools view -h $BAM > "${OUTPUT_DIR}/reads.sam"
wiggles "${OUTPUT_DIR}/reads.sam" "${OUTPUT_DIR}/tuque.coverage.wig"
bgzip "${OUTPUT_DIR}/tuque.coverage.wig"
tabix -s 1 -b 2 -e 3 -S 1 -0 -f "${OUTPUT_DIR}/tuque.coverage.wig.gz"
python $PIPELINE/src/sam2juncs.py -s "${OUTPUT_DIR}/reads.sam" -g $GENOME -c ${OUTPUT_DIR}/tuque.coverage.wig.gz  -o ${OUTPUT_DIR}/observed.juncs
python $PIPELINE/src/classify_juncs.py --in "${OUTPUT_DIR}/observed.juncs" --out "${OUTPUT_DIR}/classified.juncs"
sort -k1,1 -k2,2n -k3,3 "${OUTPUT_DIR}/classified.juncs" | bgzip -c > "${OUTPUT_DIR}/classified.juncs.gz" 
tabix -s 1 -b 2 -e 3 -S 0 -0 -f "${OUTPUT_DIR}/classified.juncs.gz"
rm "${OUTPUT_DIR}/reads.sam"


