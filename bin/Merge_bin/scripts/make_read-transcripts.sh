#!/bin/bash
# generate read-transcripts using Augustus

#set -o errexit
set -o nounset

PROGNAME=$(basename $0)

function error_exit
{

#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------

    echo
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!! ABORT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	echo "!  ${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	exit 1
}

PIPELINE="$(dirname "$( cd "$( dirname "$0" )" && pwd )" )"
GENOME=$1
shift
RNA_SEQ=$1
shift
AUGUSTUS=$1
shift
AUGUSTUS_NAME=$1
shift
READ_ISLANDS=$1
shift
READ_TRANSCRIPTS=$1
shift
EXTRINSIC_CFG=$1
shift
MIN_COVER=$1
shift
MIN_TRANSCRIPT_LENGTH=$1
shift
MIN_EXON_LENGTH=$1
shift
MAX_READTHROUGH=$1
shift
ALPHA=$1
shift
MAX_INTRAGENE_GAP=$1
shift
REGION_PAD=$1
shift
LOG=$1

echo "Starting make_read-transcript.sh" >> $LOG
for v in GENOME RNA_SEQ AUGUSTUS AUGUSTUS_NAME READ_ISLANDS READ_TRANSCRIPTS EXTRINSIC_CFG ; do echo "$v = ${!v}" >> $LOG ; done
DIR=$( dirname $GENOME )

if ! [ -e $READ_ISLANDS ]
then
    echo "Generating $READ_ISLANDS" >> $LOG
    python $PIPELINE/src/find_read_islands.py $GENOME ${RNA_SEQ}/classified.juncs.gz ${RNA_SEQ}/tuque.coverage.wig.gz $READ_ISLANDS
    if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in find_read_islands.py" ; fi
fi

if ! [ -d ${DIR}/scaffolds ]
then
	echo "Generating a .fasta file for each scaffold in $GENOME" >> $LOG
	echo "python $PIPELINE/src/separate_scaffolds.py $GENOME" >> $LOG
	python $PIPELINE/src/separate_scaffolds.py $GENOME
	if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in separate_scaffolds.py" ; fi
fi

echo "Generating read-transcript models from $READ_ISLANDS" >> $LOG
echo "python $PIPELINE/src/islands2genes.py --in $READ_ISLANDS -g ${DIR}/scaffolds -j $RNA_SEQ/classified.juncs.gz -n $AUGUSTUS_NAME -o $READ_TRANSCRIPTS --augustus_path $AUGUSTUS --extrinsicCfgFile $EXTRINSIC_CFG --max_intragene_gap $MAX_INTRAGENE_GAP --region_pad $REGION_PAD" >> $LOG
python $PIPELINE/src/islands2genes.py --in $READ_ISLANDS -g ${DIR}/scaffolds -j $RNA_SEQ/classified.juncs.gz -n $AUGUSTUS_NAME -o $READ_TRANSCRIPTS --augustus_path $AUGUSTUS --extrinsicCfgFile $EXTRINSIC_CFG --max_intragene_gap $MAX_INTRAGENE_GAP --region_pad $REGION_PAD &> $( dirname $READ_TRANSCRIPTS )/islands2genes.log
if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in islands2genes.py" ; fi

echo "make_read-transcripts.sh is done." >> $LOG
