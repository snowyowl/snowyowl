#!/bin/bash
#set -o errexit

PROGNAME=$(basename $0)

function error_exit
{

#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------

    echo
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!! ABORT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	echo "!  ${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	exit 1
}

# Generate a second-round set of gene predictions from multiple sets of first-round predictions in .gff3 files
# Run this script from the directory where input files are located and output should be written
# List the input first-round predictions at the end of the command line
# Usage: predict_genes.sh genome.fa rna-seq_dir prediction_name_prefix coe01_username input1.gff3 input2.gff3 ...

LOG=predict_genes.log
echo  "                  =====================================" >> "$LOG"
echo $(date '+%F %T') "$0 $@" >> "$LOG"

PIPELINE="$(dirname "$( cd "$( dirname "$0" )" && pwd )" )"
GENOME=$1
GENOME_DIR=$( dirname $GENOME )
shift
RNA_SEQ=$1
SCAFFOLDS=${GENOME_DIR}/scaffolds
shift
LABEL=$1
shift
DEPTH_THRESHOLD=$1
shift
DEFAULT_SCORE=$1 #Score initially assigned to all models
shift
MAX_HOMOLOGS=$1 #Maximum number of homologs used for scoring
shift
MIN_PROTEIN_LENGTH=$1 #Shorter predicted proteins from monoexonic models with no homologs in database are penalized
shift
MIN_MEDIAN_RATIO=$1 #Transcripts containing an exon with a median coverage depth that is a smaller fraction of the transcript median depth are assigned a status of Heterogeneous coverage depth
shift
MAX_MEDIAN_RATIO=$1 #Transcripts containing an exon with a median coverage depth that is a larger multiple of the transcript median depth are assigned a status of Heterogeneous coverage depth
shift
MAX_LOW_RUN=$1 #Transcripts containing a longer run of coverage depth less than coverage_tolerance * transcript mean depth are assigned a status of Heterogeneous coverage depth
shift
COVERAGE_TOLERANCE=$1 #Fraction of transcript mean depth used to define a low run
shift
MAX_INTRON_READTHROUGH=$1 #Splices with a higher readthrough ratio are ignored during scoring
shift
MERGE_PSEUDOSCORE=$1 #A small positive constant added to all scores to avoid merging problems
shift
MIN_INTRON_LENGTH=$1 #Predicted introns shorter than this are rejected
shift
MAX_INTRON_LENGTH=$1 #Predicted introns longer than this are rejected
shift
MAX_UNKNOWN_BASES=$1 # Transcripts with more Ns in their sequences are assigned a status of Cannot be translated into protein
shift
INPUTS=$@


condense_model() {
	local MODEL
	MODEL=$1
	if ! [[  ${MODEL} =~ \.scored\+homology\.gff3 || -s ${MODEL%gff3}scored+homology.gff3 ]]
	then echo $(date '+%F %T') "$PIPELINE/scripts/score_models_with_homology_dc.sh $MODEL $GENOME $RNA_SEQ $DEPTH_THRESHOLD $DEFAULT_SCORE $MAX_HOMOLOGS $MIN_PROTEIN_LENGTH $MIN_MEDIAN_RATIO $MAX_MEDIAN_RATIO $MAX_LOW_RUN $COVERAGE_TOLERANCE $MAX_INTRON_READTHROUGH $MIN_INTRON_LENGTH $MAX_INTRON_LENGTH $MAX_UNKNOWN_BASES" >> "$LOG"
		$PIPELINE/scripts/score_models_with_homology_dc.sh $MODEL $GENOME  $RNA_SEQ $DEPTH_THRESHOLD $DEFAULT_SCORE $MAX_HOMOLOGS $MIN_PROTEIN_LENGTH $MIN_MEDIAN_RATIO $MAX_MEDIAN_RATIO $MAX_LOW_RUN $COVERAGE_TOLERANCE $MAX_INTRON_READTHROUGH $MIN_INTRON_LENGTH $MAX_INTRON_LENGTH $MAX_UNKNOWN_BASES
		if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in score_models_with_homology_dc.sh" ; fi
	else
		echo "${MODEL%gff3}scored+homology.gff3 already exists"
	fi
	if ! [[  ${MODEL} =~ \.condensed\. || -s  ${MODEL%gff3}condensed.scored+homology.gff3 ]]
	then if [[ $MODEL =~ .*augustus.* ]] # condense multiple transcripts in Augustus predictions
	    then echo $(date '+%F %T') "python $PIPELINE/src/merge_models.py --out ${MODEL%gff3}condensed.gff3.unsorted --intervals ${MODEL%gff3}condensed.loci.txt --in ${MODEL%gff3}scored+homology.gff3 --pseudoscore $MERGE_PSEUDOSCORE" >> "$LOG"
		python $PIPELINE/src/merge_models.py --out ${MODEL%gff3}condensed.gff3.unsorted --intervals ${MODEL%gff3}condensed.loci.txt --in ${MODEL%gff3}scored+homology.gff3 --pseudoscore $MERGE_PSEUDOSCORE
		if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in merge_models.py" ; fi
		echo $(date '+%F %T') "python $PIPELINE/src/sort_gff3_genes.py  ${MODEL%gff3}condensed.gff3.unsorted ${MODEL%gff3}condensed.scored+homology.gff3" >> "$LOG"
		python $PIPELINE/src/sort_gff3_genes.py  ${MODEL%gff3}condensed.gff3.unsorted ${MODEL%gff3}condensed.scored+homology.gff3
		if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in sort_gff3_genes.py " ; fi
		echo $(date '+%F %T') "rm  ${MODEL%gff3}condensed.gff3.unsorted" >> "$LOG"
		rm  ${MODEL%gff3}condensed.gff3.unsorted
		else echo $(date '+%F %T') "cp ${MODEL%gff3}scored+homology.gff3  ${MODEL%gff3}condensed.scored+homology.gff3" >> "$LOG"
		    cp ${MODEL%gff3}scored+homology.gff3  ${MODEL%gff3}condensed.scored+homology.gff3
			if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in cp ${MODEL%gff3}scored+homology.gff3" ; fi
		fi 
	else
		echo "${MODEL%gff3}condensed.scored+homology.gff3 already exists" 
	fi
}

for f in $INPUTS ; do
    b=$(basename $f )
    if !  [[ $b =~ .+\.condensed\.scored\+homology\.gff3 ]]; then
	condense_model  $f >> "$LOG" 2>> stderr.log &	
    fi
done
wait
if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in condense_model" ; fi

if ! [ -d Predictions ]
	then mkdir Predictions
fi
    
echo $(date '+%F %T') "python $PIPELINE/src/merge_models.py --out Predictions/merged.gff3 --intervals Predictions/merged.loci.txt --pseudoscore $MERGE_PSEUDOSCORE    --in *.condensed.scored+homology.gff3" >> "$LOG"
python $PIPELINE/src/merge_models.py --out Predictions/merged.gff3 --intervals Predictions/merged.loci.txt --pseudoscore $MERGE_PSEUDOSCORE  --in *.condensed.scored+homology.gff3 >> "$LOG" 2>> stderr.log
if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in merge_models.py" ; fi

echo $(date '+%F %T') "python $PIPELINE/src/get_representative_models.py Predictions/merged.gff3 ${GENOME_NAME}_ Predictions/representatives.gff3" >> "$LOG"
python $PIPELINE/src/get_representative_models.py Predictions/merged.gff3 ${LABEL}_ Predictions/representatives.gff3 >> "$LOG" 2>> stderr.log
if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in get_representative_models.py " ; fi

echo $(date '+%F %T') "bash $PIPELINE/scripts/get_accepted_representatives.sh Predictions/representatives.gff3 Predictions/accepted.gff3" >> "$LOG"
bash $PIPELINE/scripts/get_accepted_representatives.sh Predictions/representatives.gff3 Predictions/accepted.gff3  >> "$LOG" 2>> stderr.log
if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in get_accepted_representatives.sh " ; fi

echo $(date '+%F %T') "python $PIPELINE/src/gff32predictedTranscriptsAndProteins.py Predictions/accepted.gff3 $GENOME Predictions/accepted.CDS.fna Predictions/accepted.Proteins.faa" >> "$LOG"
python $PIPELINE/src/gff32predictedTranscriptsAndProteins.py --in Predictions/accepted.gff3 -g $GENOME --CDS Predictions/accepted.CDS.fna -p Predictions/accepted.Proteins.faa  >> "$LOG" 2>> stderr.log
 if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in gff32predictedTranscriptsAndProteins.py " ; fi
