#!/bin/bash
# 2011-05-04 08:17:51 
#set -o errexit
set -o nounset

PROGNAME=$(basename $0)

function error_exit
{

#	----------------------------------------------------------------
#	Function for exit due to fatal program error
#		Accepts 1 argument:
#			string containing descriptive error message
#	----------------------------------------------------------------

    echo
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!! ABORT !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	echo "!  ${PROGNAME}: ${1:-"Unknown Error"}" 1>&2
    echo "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
	exit 1
}

PIPELINE="$(dirname "$( cd "$( dirname "$0" )" && pwd )" )"
MODELS=$1 # Models to be scored, in .gff3 file
shift
GENOME=$1 # Name of a fasta file containing the genomic sequence
shift
RNA_SEQ=$1 # Directory containing RNA-Seq results, including classifed.juncs.gz
shift
TL_USER=$1 # User name on $TL_SERVER
shift
DEPTH_THRESHOLD=$1 #Minimum coverage depth for strict scoring
shift
DEFAULT_SCORE=$1 #Score initially assigned to all models
shift
MAX_HOMOLOGS=$1 #Maximum number of homologs used for scoring
shift
TL_SERVER=$1 #URL of the server hosting TimeLogic boards
shift
TL_MACH_NAME=$1 #Identifier of the TimeLogic board to be used
shift
DC_PATH=$1 #Path to the server directory containing Decypher software for the TimeLogic boards
shift
MAX_TIMEOUTS=$1 #Maximum number of timeouts during file transfer
echo "MAX_TIMEOUTS=$MAX_TIMEOUTS"
shift
MIN_PROTEIN_LENGTH=$1 #Shorter predicted proteins from monoexonic models with no homologs in database are penalized
shift
MIN_MEDIAN_RATIO=$1 #Transcripts containing an exon with a median coverage depth that is a smaller fraction of the transcript median depth are assigned a status of Heterogeneous coverage depth
shift
MAX_MEDIAN_RATIO=$1 #Transcripts containing an exon with a median coverage depth that is a larger multiple of the transcript median depth are assigned a status of Heterogeneous coverage depth
echo "MAX_MEDIAN_RATIO=$MAX_MEDIAN_RATIO"
shift
MAX_LOW_RUN=$1 #Transcripts containing a longer run of coverage depth less than coverage_tolerance * transcript mean depth are assigned a status of Heterogeneous coverage depth
shift
COVERAGE_TOLERANCE=$1 #Fraction of transcript mean depth used to define a low run
shift
MAX_INTRON_READTHROUGH=$1 #Splices with a higher readthrough ratio are ignored during scoring
echo "MAX_INTRON_READTHROUGH=$MAX_INTRON_READTHROUGH"
shift
MIN_INTRON_LENGTH=$1 #Predicted introns shorter than this are rejected
echo "MIN_INTRON_LENGTH=$MIN_INTRON_LENGTH"
shift
MAX_INTRON_LENGTH=$1 #Predicted introns longer than this are rejected
echo "MAX_INTRON_LENGTH=$MAX_INTRON_LENGTH"
shift
MAX_UNKNOWN_BASES=$1 # Transcripts with more Ns in their sequences are assigned a status of Cannot be translated into protein
echo "MAX_UNKNOWN_BASES=$MAX_UNKNOWN_BASES"

HOMOLOGY=${MODELS%%.gff*}.homology.gff3
UNSORTED=${MODELS%%.gff*}.unsorted.scored.gff3

PROTEINS=${MODELS%%gff3}proteins.faa
CDS=${MODELS%%gff3}CDS.fna
BLAST_OUT=${PROTEINS%%faa}blastp
REVISED=${MODELS%%.gff*}.scored+homology.gff3

if ! [ -s $BLAST_OUT ]
then
	if ! [ -s $PROTEINS ]
	then
		echo "Generating predicted protein sequences from $MODELS "
		echo "python $PIPELINE/src/gff32predictedTranscriptsAndProteins.py --in $MODELS --genome $GENOME --CDS $CDS --protein $PROTEINS"
		python $PIPELINE/src/gff32predictedTranscriptsAndProteins.py --in $MODELS --genome $GENOME --CDS $CDS --protein $PROTEINS
		if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in gff32predictedTranscriptsAndProteins.py" ; fi
	else
		echo "$PROTEINS already exists"
	fi
	echo "Searching for homologs of predicted proteins from $MODELS "
	echo "python $PIPELINE/src/run_remote_tera-blastp.py --in $PROTEINS --out $BLAST_OUT -u $TL_USER -s $TL_SERVER --mach_name $TL_MACH_NAME --decypher_path $DC_PATH --max_timeouts $MAX_TIMEOUTS"
	python $PIPELINE/src/run_remote_tera-blastp.py --in $PROTEINS --out $BLAST_OUT -u $TL_USER -s $TL_SERVER --mach_name $TL_MACH_NAME --decypher_path $DC_PATH --max_timeouts $MAX_TIMEOUTS
	if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in run_remote_tera-blastp.py" ; fi

else
	echo "$BLAST_OUT already exists"
fi

echo "Adding homology scores to $MODELS "
echo "python $PIPELINE/src/add_exon_homology_scores_to_gff3.py --in $MODELS --blastp $BLAST_OUT --out $HOMOLOGY --default_score $DEFAULT_SCORE --max_homologs $MAX_HOMOLOGS"
python $PIPELINE/src/add_exon_homology_scores_to_gff3.py --in $MODELS --blastp $BLAST_OUT --out $HOMOLOGY --default_score $DEFAULT_SCORE --max_homologs $MAX_HOMOLOGS
if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in add_exon_homology_scores_to_gff3.py" ; fi

if ! [ -s $REVISED ]
then 
	echo "Intron-scoring $HOMOLOGY "
	echo "python $PIPELINE/src/score_models.py --in $HOMOLOGY -g $GENOME -j ${RNA_SEQ}/classified.juncs.gz -c ${RNA_SEQ}/tuque.coverage.wig.gz -t $DEPTH_THRESHOLD --out $UNSORTED --min_protein_length $MIN_PROTEIN_LENGTH --min_median_ratio $MIN_MEDIAN_RATIO --max_median_ratio $MAX_MEDIAN_RATIO --max_low_run $MAX_LOW_RUN --coverage_tolerance $COVERAGE_TOLERANCE --max_intron_readthrough $MAX_INTRON_READTHROUGH --min_intron_length $MIN_INTRON_LENGTH --max_intron_length $MAX_INTRON_LENGTH --max_unknown_bases $MAX_UNKNOWN_BASES"
	python $PIPELINE/src/score_models.py --in $HOMOLOGY -g $GENOME -j ${RNA_SEQ}/classified.juncs.gz -c ${RNA_SEQ}/tuque.coverage.wig.gz -t $DEPTH_THRESHOLD --out $UNSORTED --min_protein_length $MIN_PROTEIN_LENGTH --min_median_ratio $MIN_MEDIAN_RATIO --max_median_ratio $MAX_MEDIAN_RATIO --max_low_run $MAX_LOW_RUN --coverage_tolerance $COVERAGE_TOLERANCE --max_intron_readthrough $MAX_INTRON_READTHROUGH --min_intron_length $MIN_INTRON_LENGTH --max_intron_length $MAX_INTRON_LENGTH --max_unknown_bases $MAX_UNKNOWN_BASES &
	wait
	if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in score_models.py" ; fi

	echo "python $PIPELINE/src/sort_gff3_genes.py $UNSORTED $REVISED"
	python $PIPELINE/src/sort_gff3_genes.py $UNSORTED $REVISED
	if [[  "$?" -ne 0 ]] ; then  error_exit "$LINENO: Failure in sort_gff3_genes.py" ; fi
	rm $UNSORTED
else
	echo "$REVISED already exists"
fi

echo "Done! Scored models are in $REVISED "

