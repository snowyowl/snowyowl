#!/bin/bash
set -o errexit
set -o nounset

# path to first model set
MODELS_1=$1
# path to second model set
MODELS_2=$2
# Genome sequence in (multi-)fasta format
GENOME=$3
# path to RNA-seq directory
RNA_SEQ=$4
# User name for Time Logic boards
UNAME=$5

PIPELINE="$(dirname "$( cd "$( dirname "$0" )" && pwd )" )"
WORK_DIR=$( dirname $MODELS_2 )
SCORED_1=${MODELS_1%%.gff*}.scored+homology.gff3
SCORED_2=${MODELS_2%%.gff*}.scored+homology.gff3

#Read configuration file
source $PIPELINE/config/settings.config.template
DEPTH_THRESHOLD=10

for m in "$MODELS_1" "$MODELS_2" ; do echo "Scoring $m" 
echo " $PIPELINE/scripts/score_models_with_homology_dcremote.sh $m $GENOME $RNA_SEQ $UNAME $DEPTH_THRESHOLD $DEFAULT_SCORE $MAX_HOMOLOGS $TL_SERVER $TL_MACH_NAME $DC_PATH $MAX_TIMEOUTS $MIN_PROTEIN_LENGTH $MIN_MEDIAN_RATIO $MAX_MEDIAN_RATIO $MAX_LOW_RUN $COVERAGE_TOLERANCE $MAX_INTRON_READTHROUGH $MIN_INTRON_LENGTH $MAX_INTRON_LENGTH $MAX_UNKNOWN_BASES"
$PIPELINE/scripts/score_models_with_homology_dcremote.sh "$m" "$GENOME" "$RNA_SEQ" $UNAME $DEPTH_THRESHOLD $DEFAULT_SCORE $MAX_HOMOLOGS $TL_SERVER $TL_MACH_NAME $DC_PATH $MAX_TIMEOUTS $MIN_PROTEIN_LENGTH $MIN_MEDIAN_RATIO $MAX_MEDIAN_RATIO $MAX_LOW_RUN $COVERAGE_TOLERANCE $MAX_INTRON_READTHROUGH $MIN_INTRON_LENGTH $MAX_INTRON_LENGTH $MAX_UNKNOWN_BASES & done
wait

MODELS_1_GZ=${SCORED_1}.gz
MODELS_2_GZ=${SCORED_2}.gz

[[ -s $MODELS_1_GZ ]] || sort -k1,1 -k4,4n -k5,5n $SCORED_1 | bgzip -c > $MODELS_1_GZ
[[ -e ${MODELS_1_GZ}.tbi ]] || tabix -p gff $MODELS_1_GZ
[[ -s $MODELS_2_GZ ]] || sort -k1,1 -k4,4n -k5,5n $SCORED_2 | bgzip -c > $MODELS_2_GZ
[[ -e ${MODELS_2_GZ}.tbi ]] || tabix -p gff $MODELS_2_GZ

echo "Merging two model sets into ${WORK_DIR}/superset.gff3"
python $PIPELINE/src/merge_models.py --out ${WORK_DIR}/superset.gff3 --intervals ${WORK_DIR}/superset.loci.txt  --in $SCORED_1  $SCORED_2
rm ${WORK_DIR}/superset.loci.txt
$PIPELINE/scripts/get_accepted_representatives.sh  ${WORK_DIR}/superset.gff3 ${WORK_DIR}/accepted.superset.gff3

echo
echo "Looking for accepted superset models in $MODELS_1"
python $PIPELINE/src/get_coincident_predictions.py -i ${WORK_DIR}/accepted.superset.gff3 -m ${MODELS_1%%.gff*}.in_superset.gff3 -q $MODELS_1_GZ

echo
echo "Looking for accepted superset models in $MODELS_2"
python $PIPELINE/src/get_coincident_predictions.py -i ${WORK_DIR}/accepted.superset.gff3 -m ${MODELS_2%%.gff*}.in_superset.gff3 -q $MODELS_2_GZ

