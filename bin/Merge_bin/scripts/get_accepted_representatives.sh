#! /bin/bash

set -o errexit
set -o nounset

PIPELINE="$(dirname "$( cd "$( dirname "$0" )" && pwd )" )"
REPS=$1
ACCEPTED_NAMES=${REPS%gff3}accepted_names.txt
ACCEPTED_REPS=$2
GENES=${REPS%gff3}genes.gff3
grep '	gene	' $REPS > $GENES
echo
echo 'Summary:'
echo $( cat $GENES | wc -l ) 'Representative models'
echo
for s in 'Bad intron structure' 'Cannot be translated into protein' 'Short predicted protein' 'Heterogeneous coverage depth' 'Fails to match RNA-Seq data' ; do echo $( grep -c "$s" $GENES) $s ; done
echo
for s in 'Matches RNA-Seq data' 'Low expression' ; do echo $( grep -c "$s" $GENES) $s ; done
grep 'Matches RNA-Seq data' $GENES | sed -e 's/.*Name=\([^;][^;]*\).*/\1/' > $ACCEPTED_NAMES
grep 'Low expression' $GENES | sed -e 's/.*Name=\([^;][^;]*\).*/\1/' >> $ACCEPTED_NAMES
python $PIPELINE/src/selectGenes.py -i $ACCEPTED_NAMES -g $REPS  -o $ACCEPTED_REPS
echo '----------------------'
echo $(cat $ACCEPTED_NAMES | wc -l ) " Accepted models "
echo



