#!/bin/bash
set -o errexit
set -o nounset

# path to old representatives.gff3
OLD=$1
# path to new representatives.gff3
NEW=$2
# highest gene number in OLD + 1
START_NUM=$3
# Genome sequence in (multi-)fasta format
GENOME=$4

PIPELINE="$(dirname "$( cd "$( dirname "$0" )" && pwd )" )"
WORK_DIR=$( dirname $NEW )
OLD_GZ=${OLD}.gz
NEW_GZ=${NEW}.gz

[[ -s $OLD_GZ ]] || sort -k1,1 -k4,4n -k5,5n $OLD | bgzip -c > $OLD_GZ
[[ -e ${OLD_GZ}.tbi ]] || tabix -p gff $OLD_GZ
[[ -s $NEW_GZ ]] || sort -k1,1 -k4,4n -k5,5n $NEW | bgzip -c > $NEW_GZ
[[ -e ${NEW_GZ}.tbi ]] || tabix -p gff $NEW_GZ

echo 'Looking for conserved models'
python $PIPELINE/src/get_coincident_predictions.py -i $OLD -m ${WORK_DIR}/conserved.gff3 -q $NEW_GZ

echo 'Looking for novel models'
python $PIPELINE/src/get_coincident_predictions.py -i $NEW -m ${WORK_DIR}/not_novel.gff3  -q $OLD_GZ

echo 'Renumbering novel models'
python $PIPELINE/src/renumber_models.py -i ${WORK_DIR}/not_novel.mismatch.gff3 -o ${WORK_DIR}/replacing.gff3 -n $START_NUM

echo "Combining conserved and novel models into ${WORK_DIR}/combined.gff3"
python $PIPELINE/src/merge_models.py --out ${WORK_DIR}/combined.gff3 --intervals ${WORK_DIR}/combined.loci.txt --in ${WORK_DIR}/conserved.gff3 ${WORK_DIR}/replacing.gff3
rm ${WORK_DIR}/combined.loci.txt

echo "Matching replaced and replacing models"
python $PIPELINE/src/merge_models.py --out  ${WORK_DIR}/replacement.gff3 --intervals ${WORK_DIR}/replacement.loci.txt --in ${WORK_DIR}/conserved.mismatch.gff3 ${WORK_DIR}/replacing.gff3
grep  '	gene	' ${WORK_DIR}/conserved.mismatch.gff3 > ${WORK_DIR}/replaced.genes.gff3
grep  '	gene	' ${WORK_DIR}/replacing.gff3 > ${WORK_DIR}/replacing.genes.gff3
PREFIX=$( python $PIPELINE/src/find_distinguishing_prefix.py --older ${WORK_DIR}/replaced.genes.gff3 --newer ${WORK_DIR}/replacing.genes.gff3 )
echo $PREFIX
python $PIPELINE/src/note_replacements.py -l ${WORK_DIR}/replacement.loci.txt -i ${WORK_DIR}/replaced.genes.gff3 -o ${WORK_DIR}/replaced.gff3 -p $PREFIX

echo 'Cleaning up'
rm ${WORK_DIR}/conserved.mismatch.gff3
rm ${WORK_DIR}/not_novel.gff3
rm ${WORK_DIR}/not_novel.mismatch.gff3
rm ${WORK_DIR}/replaced.genes.gff3
rm ${WORK_DIR}/replacing.genes.gff3

echo 'Post-processing combined models'
python $PIPELINE/src/filter_by_score.py -i ${WORK_DIR}/combined.gff3 -t 1.0 -o ${WORK_DIR}/confident.combined.gff3
python $PIPELINE/src/gff32predictedTranscriptsAndProteins.py --in ${WORK_DIR}/combined.gff3 -g $GENOME --CDS ${WORK_DIR}/combined.CDS.fna -p ${WORK_DIR}/combined.proteins.faa


