#!/bin/bash
awk 'BEGIN { FS = "\t"}; {printf "%s\t%d\n", $0, $5*(1+$12-$11)}' $1  | sort -t$'\t' -k2,2 -k12nr | sort -u -k2,2 | sort -k1,1n > $2