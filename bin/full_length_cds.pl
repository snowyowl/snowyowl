#!/usr/bin/perl

# Modified by Ian Reid to take project_dir as a parameter,
# and to skip empty lines in input

my $SO;

BEGIN {
  $SO = $ENV{'SNOWYOWL'} || die "SNOWYOWL is not a defined environment variable\n";
}



use File::Basename;
use Bio::Range;
use Bio::Seq;

$SPECIES = "$ARGV[1]_TRAINING_CDS";
$MIN_PROT_LENGTH = 70;
$index = 0;

open(FILE, $ARGV[0]) || die;
@input_data = <FILE>;
close(FILE);

$seq_dirname =  dirname $ARGV[0];
open(SEQUENCEFILE, ">$seq_dirname/training_cds.txt");

foreach (@input_data) {
	chop;
	next if (length($_) < 10);
	($seqid,$seq,$seqid2,$hit_id,$hit_description,$eval,$pid,$query_frame,$hit_length,$hit_beg,$hit_end,$query_length,$que_beg,$que_end) = split(/\t/, $_);

#print "-> " . "$seqid,$seqid2,$hit_id,$hit_description,$eval,$pid,$query_frame,$hit_length,$hit_beg,$hit_end,$query_length,$que_beg,$que_end" . "\n";

	next if (length($seq) < 1) ;

        $sequence = Bio::Seq->new( -seq => $seq, -id => $seqid);

        if($query_frame > 0) {
                $translated_seq = $sequence->translate( -frame => $query_frame - 1 );
        }
        if($query_frame < 0) {
                $translated_seq = $sequence->revcom->translate( -frame => -$query_frame - 1 );
        }

	$cds = "";
		
        @start_codons = ();
       	while (1)
       	{
               	$pos = index($translated_seq->seq(), "M", $pos);
               	last if($pos < 0);
               	push(@start_codons, ++$pos);
       	}

       	@stop_codons = ();
       	while (1)
       	{
               	$pos = index($translated_seq->seq(), "*", $pos);
               	last if($pos < 0);
               	push(@stop_codons, ++$pos);
       	}


	$inside = 0;
	$predicted_length = 0;
	$max_predicted_length = 0;
	STARTS: foreach $start (@start_codons) {
		$protein_start = 0;
		$protein_stop = 0;
              	$too_close = 0;
               	foreach $stop (@stop_codons) {
                       	if(abs($stop - $start) < $MIN_PROT_LENGTH && $stop > $start) {
                               	$too_close = 1;
                       	}
                       	if($stop - $start >= $MIN_PROT_LENGTH && $too_close == 0) {
                               	$protein_start = $start;
                               	$protein_stop = $stop;
				$predicted_protein_seq = substr($translated_seq->seq(), $protein_start - 1, $protein_stop - $protein_start), "\t";
				$predicted_length = length($predicted_protein_seq);
#				print "$seqid\t$protein_start\t$protein_stop\n";
#				print "$predicted_protein_seq\n";
                                $alignment_range = Bio::Range->new( -start => $que_beg, -end => $que_end );
                                $potential_orf_range = Bio::Range->new( -start => $protein_start, -end => $protein_stop );
                                if($potential_orf_range->contains($alignment_range) && $predicted_length > $max_predicted_length) {
                                        $inside = 1;
					$longest_protein_start = $protein_start;
					$longest_protein_stop = $protein_stop;
					$max_predicted_length = $predicted_length;	
				}
				next STARTS;
                       	}
               	}
        }
	process_orf() if($inside);
}

close(SEQUENCEFILE);

sub process_orf {

	$predicted_protein_seq = substr($translated_seq->seq(), $longest_protein_start - 1, $longest_protein_stop - $longest_protein_start), "\t";
        $contig_start = 3 * $longest_protein_start + abs($query_frame) - 3;
        $contig_stop = 3 * $longest_protein_stop + abs($query_frame) - 1;
        if($query_frame > 0) {
                $cds = $sequence->subseq($contig_start, $contig_stop);
        }
        if($query_frame < 0) {
                $cds = $sequence->revcom->subseq($contig_start, $contig_stop);
        }

	$index++;

	print SEQUENCEFILE "\>", $SPECIES, "_";
	printf SEQUENCEFILE '%05s', $index;
	print SEQUENCEFILE "\n$cds\n";

}


