#!/usr/bin/perl

open(FILE, "$ARGV[0]") || die;
@lines1 = <FILE>;
close(FILE);

open(FILE, "$ARGV[1]") || die;
@lines2 = <FILE>;
close(FILE);

foreach $line1 (@lines1) {
	chop $line1;
	@hits = grep(/$line1\s+/, @lines2);
	print "$hits[0]";
}
print "\n";
