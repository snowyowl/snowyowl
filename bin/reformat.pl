#!/usr/bin/perl

use Bio::SearchIO;
use POSIX;

my $file = $ARGV[0];
my $MINevalue = 1.0e-10;

my $in = new Bio::SearchIO(
    -format => 'blast',
    -file   => "$file"
);

while ( my $result = $in->next_result )
{
  while (my $hit = $result->next_hit)
  {
    my $tstart = -1;
    my $tend = -1;
    my $qstart = -1;
    my $qend = -1;
    my $evalue = -1;
    my $frame = 0;
    
    while (my $hsp = $hit->next_hsp)
    {
      next if $hsp->evalue > $MINevalue;
      $frame = ($hsp->query->frame + 1) * $hsp->query->strand;
      
      $tstart = $hsp->sbjct->start if ($tstart > $hsp->sbjct-start || $tstart == -1);
      $tend = $hsp->sbjct->end if ($tend < $hsp->sbjct->end);
      
      $qstart = $hsp->query->start if ($qstart > $hsp->query-start || $qstart == -1);
      $qend = $hsp->query->end if ($qend < $hsp->query->end);

      $evalue = $hsp->evalue if $hsp->evalue > $evalue;
      #print "querylocus       = " . $result->query_name. "\n";
      #print "targetlocus      = " . $hit->name . "\n";
      #print "targetdescription= " . $hit->description . "\n";;
      #print "significance     = " . $hsp->evalue . "\n";
      #print "percentalignment = " . 0 . "\n";
      #print "queryframe       = " . ($hsp->query->frame + 1) * $hsp->query->strand . "\n";
      #print "targetlength     = " . $hsp->sbjct->length . "\n";
      #print "targetstart      = " . $hsp->sbjct->start . "\n";
      #print "targetend        = " . $hsp->sbjct->end . "\n";
      #print "querylength      = " . $result->query_length . "\n";
      #print "querystart       = " . floor(($hsp->query->start)/3) . "\n";
      #print "queryend         = " . ceil(($hsp->query->end)/3) . "\n";
      #print "\n";
    }
    
    print $result->query_name . "\t" .
          $hit->name . "\t" .
          $hit->description . "\t" .
          ($evalue) . "\t" .
          ".\t" .
          ($frame) . "\t" .
          ($tend - $tstart + 1) . "\t" .
          ($tstart) . "\t" .
          ($tend) . "\t" .
          (ceil($result->query_length/3)) . "\t" .
          (floor($qstart/3)) . "\t" .
          (ceil($qend/3)) . "\n" unless ($evalue == -1);
  }
}
