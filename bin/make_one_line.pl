#!/usr/bin/perl

open(FILE, $ARGV[0]) || die;
while(<FILE>) {
	if(/>/) {
		print "\n";
		chop;
		print "$_\t";
	}	
	else {
		chop;
		print;
	}
}
close(FILE);

print "\n";
