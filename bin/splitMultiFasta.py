''' splitMultiFasta
Split a fasta file containing multiple records into separate files for  each record
Created on Oct 26, 2009
@author: ian
'''
import sys, os

if __name__ == '__main__':
    input = open(sys.argv[1])
    output_dir = sys.argv[2]
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    null_file = os.path.join(output_dir, 'null')
    output = open(null_file, 'w')
    for line in input:
        if line.startswith('>'):
            output.close()
            id = line.strip().split()[0][1:]
            id =id.replace('/', '_')
            output_filename = os.path.join(output_dir, id + '.fasta')
#            print output_filename
            output.close()
            output = open(output_filename, 'w')
        output.write(line)
    output.close()
    os.unlink(null_file)
#    print 'Done!'
