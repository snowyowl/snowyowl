#!/usr/bin/env perl

my ($SO, $AUGUSTUS);

BEGIN {
  $SO = $ENV{'SNOWYOWL'}
    or die "SNOWYOWL is not a defined environment variable\n";
  $AUGUSTUS = $ENV{'AUGUSTUS_CONFIG_PATH'}
    or die "AUGUSTUS_CONFIG_PATH is not a defined environment variable\n";
}

use strict;
use warnings;
use Getopt::Long;

my ($output_stem, $genome_seq, $rna_seq_reads, $tmp_location, $intron_max, $log_file);
GetOptions(
  "o=s"	        => \$output_stem,
  "g=s"		=> \$genome_seq,
  "rna=s"	=> \$rna_seq_reads,
  "tmp=s"       => \$tmp_location, 
  "imax=i"	=> \$intron_max,
  "l=s"         => \$log_file
  ); 

($output_stem && $genome_seq && $rna_seq_reads && $tmp_location && $intron_max && $log_file)
  or die "Align RNA-seq reads to genome with blat.\n\nUsage: $0 OPTIONS
  -o	Output filename stem (will be extended with .psl and .fs.psl)
  -g	Genome sequence file
  -rna	RNA-seq reads in FASTA format. To convert from FASTQ to FASTA, use $SO/bin/fq_all2std.pl
  -tmp  Directory for temporary files
  -imax	Largest allowed intron during alignment of RNA-seq reads to genome\n
  -l    Log file name";

open(LOG, ">> $log_file");

my $cmd = "blat -noHead -stepSize=5 -minIdentity=93 -maxIntron=$intron_max $genome_seq $rna_seq_reads $output_stem.psl";
print LOG "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: $cmd failed!\n"
};

$cmd = "cat $output_stem.psl | $AUGUSTUS/../scripts/filterPSL.pl --uniq 2> $output_stem.filterPSL.stderr | sort --temporary-directory=$tmp_location -k14,14 -k16,16n > $output_stem.fs.psl";
print LOG "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: $cmd failed!\n"
};

unlink("$output_stem.psl");
close LOG;
1;