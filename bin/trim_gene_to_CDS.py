__author__ = 'ian'

import sys, os

this_dir = os.path.dirname(__file__)
src = os.path.dirname(this_dir)
sys.path.append(src)
import argparse
from gff3Iterator import GFF3Iterator
from gff3Record import GFF3Gene

DESCRIPTION = 'Shorten gene models to include just the coding sequence'
VERSION = '0.1'

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,
        help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in', '-i', dest='input', type=argparse.FileType('r'), nargs='?', default=sys.stdin,
        help='Path to the input file; if omitted or -, input is read from stdin')
    argparser.add_argument('--out', '-o', type=argparse.FileType('w'), nargs='?', default=sys.stdout,
        help='Path to the output file; if omitted or -, output is written to stdout')
    return  argparser.parse_args()


if __name__ == '__main__':
    args = get_args()

    for gene in GFF3Iterator(args.input).genes():
        for transcript in gene.get_transcripts():
            CDS = transcript.extract_CDS()
            new_gene = GFF3Gene.fromRecord(CDS)
            new_gene.remove_attribute('Parent')
            new_gene.setID(gene.getID())
            new_gene.add_transcript(CDS)
            print >> args.out, new_gene


    args.out.close()
#    print >> sys.stderr, sys.argv[0], 'done.'
