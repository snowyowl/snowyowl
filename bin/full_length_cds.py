""" Check contigs for ORFs encompassing blastx hits and create headers for fix_exonerate_gff

Based on full_length_cds.pl by Nick O'Toole

"""
__author__ = 'ian'

import sys, os

this_dir = os.path.dirname(__file__)
src = os.path.dirname(this_dir)
sys.path.append(src)
import argparse
from Bio import Seq, SeqIO

DESCRIPTION = 'Check contigs for ORFs encompassing blastx hits and create headers for fix_exonerate_gff'
VERSION = '0.1'

MIN_PROT_LENGTH = 70

def get_args():
    argparser = argparse.ArgumentParser(description=DESCRIPTION)
    # standard options
    argparser.add_argument('--version', action='version', version='%(prog)s' + VERSION)
    argparser.add_argument('--verbose', '-v', action='count', default=0,
        help='Omit to see only fatal error messages; -v to see warnings; -vv to see warnings and progress messages')
    # options to customize
    argparser.add_argument('--in', '-i', dest='input', type=argparse.FileType('r'), nargs='?', default=sys.stdin,
        help='Path to the input file; if omitted or -, input is read from stdin')
    argparser.add_argument('--out', '-o', type=argparse.FileType('w'), nargs='?', default=sys.stdout,
        help='Path to the output file; if omitted or -, output is written to stdout')
    argparser.add_argument('--headers', type=argparse.FileType('w'), required=True,
        help='Path to the headers output file; required')
    argparser.add_argument('-p','--pad', type=int, default=20, help='Length of pad sequence at each end of CDS')
    return  argparser.parse_args()


if __name__ == '__main__':
    args = get_args()
    
    for line in args.input:
        try:
            seqid,seq,seqid2,hit_id,hit_description,evalue,pct_id,query_frame,hit_length,hit_beg,hit_end,query_length,que_beg,que_end = line.strip().split('\t')[:14]
            if len(seq) < MIN_PROT_LENGTH * 3:
                continue
            if int(que_end) < int(que_beg) + MIN_PROT_LENGTH:
                continue
            dna_seq = Seq.Seq(seq)
            if int(query_frame) < 0:
                dna_seq = dna_seq.reverse_complement()
            frame = abs(int(query_frame)) - 1
            prot_seq = str(dna_seq[frame:].translate())
            last_stop = max(0, prot_seq.rfind('*', 0, int(que_beg)))
            first_start = prot_seq.find('M', last_stop, int(que_beg)+1)
            if first_start < 0:
                continue
            first_stop = prot_seq.find('*', first_start)
            if first_stop < 0:
                continue # We want full CDS including a stop codon
            if first_start <= int(que_beg) and first_stop >= int(que_end):
                rna_start = 3 * first_start + frame
                rna_stop = 3 * first_stop + frame + 2
                left_cut = max(0, rna_start - args.pad)
                right_cut = min(len(seq)-1, rna_stop + args.pad)
                print >> args.out, seqid
                print >> args.out, str(dna_seq)[left_cut:right_cut]
                print >> args.headers, '%s\t%d\t%d' % (seqid, rna_start + 1 -left_cut, rna_stop + 1 -left_cut)

        except Exception, ex:
            print >> sys.stderr, ex

    args.out.close()
    args.headers.close()
#    print >> sys.stderr, sys.argv[0], 'done.'
