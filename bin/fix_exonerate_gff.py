"""Parse a .gff file from exonerate:est2genome and rewrite as standard gff3 with gene-mRNA-exon/CDS hierarchy and proper ID, Name, and Parent tags

Created: 2011-05-10
Author: ian
"""
import sys, os
from Bio.Data.CodonTable import TranslationError

this_dir = os.path.dirname(__file__)
src = os.path.dirname(this_dir)
sys.path.append(src)
from gff3Record import GFF3Exon,  GFF3Gene,  GFF3Record,  GFF3mRNA
from find_longest_ORFs import find_longest_ORF_in_transcript
from Bio import SeqIO, Alphabet
import argparse
from collections import defaultdict

stderr = sys.stderr

def output_curr_gene(out_gene,  out_strand,  output,  dna_index):
    if out_gene:
        out_gene.set_strand(out_strand)
        for out_transcript in out_gene.get_transcripts():
            out_transcript.set_strand(out_strand)
            for out_exon in out_transcript.get_exons():
                out_exon.set_strand(out_strand)
                out_exon.set_type('CDS')
            out_transcript = find_longest_ORF_in_transcript(out_transcript,  dna_index)
            orf_len = out_transcript.get_attribute('ORF_length')
            out_transcript.set_score(orf_len)
            out_gene.set_score(orf_len)
        if True:
            print >> output,  out_gene.toString_with_start_and_stop_codons()
        else:
            print >> output,  out_gene

def split_gene(old_gene,  cutpoint):
    if cutpoint <= old_gene.get_start()  or cutpoint >= old_gene.get_end():
        raise ValueError('[fix_exonerate_gff.split_gene] Cutpoint %d is outside gene %s' % (cutpoint,  old_gene.getID()))
    new_gene = GFF3Gene.fromRecord(old_gene)
    old_ID = old_gene.get_ID()
    new_ID = old_ID + '_2'
    new_gene.set_ID(new_ID)
    new_name = old_gene.get_name() + '_2'
    new_gene.set_name(new_name)
    old_mRNA = old_gene.get_transcripts()[0]
    old_gene.set_end(cutpoint)
    new_gene.set_start(cutpoint+1)
    old_exons = sorted(old_mRNA.get_exons())
    old_mRNA.clear_exons()
    old_mRNA.set_end(cutpoint)
    new_mRNA = GFF3mRNA.fromRecord(new_gene)
    new_mRNA.set_ID(new_gene.getID() + '.t1')
    for i in range(len(old_exons)):
        old_exons[i].clear_parents()
        if old_exons[i].get_end() <= cutpoint:
            old_mRNA.add_exon(old_exons[i])
        elif old_exons[i].get_start() > cutpoint:
            old_exons[i].set_name(new_name)
            new_mRNA.add_exon(old_exons[i])
        else:
            new_exon = GFF3Exon.fromRecord(old_exons[i])
            new_exon.set_start(cutpoint+1)
            new_exon.set_name(new_name)
            old_exons[i].set_end(cutpoint)
            old_mRNA.add_exon(old_exons[i])
            new_mRNA.add_exon(new_exon)
    new_gene.add_transcript(new_mRNA)
    return old_gene,  new_gene

def parse_gff_attributes(gff_attribute_string):
    attr_fields = gff_attribute_string.split(';')
    attr_dict = {}
    for attr_field in attr_fields:
        key,  val = attr_field.strip().split()[:2]
        if key in ['gene_orientation']:
            continue
        attr_dict[key] = val
    return attr_dict

def initialize_gene(gff3_record):
    new_gene = GFF3Gene.fromRecord(gff3_record)
    new_gene.add_attribute('ID',  new_gene.get_name())
    # Create a transcript for new gene
    mRNA = GFF3mRNA.fromRecord(new_gene)
    mRNA.set_ID(new_gene.getID() + '.t1')
    new_gene.add_transcript(mRNA)
    return  new_gene

def setup():
    parser = argparse.ArgumentParser(description='Parse a .gff file from exonerate:est2genome and rewrite as standard gff3 with gene-mRNA-exon/CDS hierarchy and proper ID, Name, and Parent tags')
    parser.add_argument('-s','--startstop', dest='startstop', action='store_true', default=False, help='force output of start and stop codons')
    parser.add_argument('--reject_faulty_CDS', dest='reject_faulty_CDS', action='store_true', default=False, help='require that contigs contain a well-formed full-length ORF')
    parser.add_argument('--genome',  '-g',  required=True,  help='Path to genome sequence file in fasta format; required')
    parser.add_argument('--headers',  required=True,  help='Path to file containing headers from in_file file; required')
    parser.add_argument('--in', '-i',  dest='in_file',  type=argparse.FileType('r'),  nargs='?',  default = sys.stdin,  help='Path to the in_file .gff file; if omitted or -, in_file is read from stdin')
    parser.add_argument('--out', '-o', type=argparse.FileType('w'),  nargs='?',  default = sys.stdout,  help='Path to the .gff3 output file; if omitted or -, output is written to stdout')
    parser.add_argument('--stderr', '-l', type=argparse.FileType('a'),  nargs='?',  default = sys.stderr,  help='Path to an error message output file; if omitted or -, messages are written to stderr')
    args = parser.parse_args()

#    in_file = open(args.in_file_file)
#    output = open(args.output_file,  'w')
    dna_index = SeqIO.to_dict(SeqIO.parse(open(args.genome), 'fasta', Alphabet.generic_dna))
    headers = open(args.headers)
    return args.in_file, args.out, dna_index, headers, args.reject_faulty_CDS, args.stderr

def load_headers(headers):
    header_dict = defaultdict(list)
    for line in headers:
        line = line.lstrip('>').rstrip()
        fields = line.split()
        header_dict[fields[0]].append((int(fields[1]),  int(fields[2])))
    return header_dict

def do_fix_exonerate_gff(in_file, output, dna_index, header_dict, reject_faulty_CDS=False):
    lines = []
    command = in_file.readline()
    host = in_file.readline()
    line = in_file.readline()
    while line:
        if line.startswith('#'):
            line = in_file.readline()
            continue
        if '\tgene\t' in line:
            if lines:
                try:
                    process1contig(lines, output, dna_index, header_dict, reject_faulty_CDS)
                except ValueError, ve:
                    print >> stderr, ve
                except TranslationError, te:
                    print >> stderr, te
            lines = []
        lines.append(line)
        line = in_file.readline()
    if lines:
        try:
            process1contig(lines, output, dna_index, header_dict)
        except ValueError, ve:
            print >> stderr, ve
        except TranslationError, te:
            print >> stderr, te



def process1contig(in_file, output, dna_index, header_dict, reject_faulty_CDS=False):
    '''Check gff for 1 contig and convert to correct gff3 gene(s)

    :param in_file: gff lines
    :type in_file: list of strings
    :param output: receives gff3 result
    :type output: file-like object open for writing
    :param dna_index: Genomic DNA sequence
    :type dna_index: dictionary of strings keyed by chromosome name
    :param header_dict: ID, start, stop for all contigs in file
    :type header_dict: dictionary with ID as key and (start, stop) as value
    :param reject_faulty_CDS: if True, contigs that do not contain a one valid CDS will raise an exception;
                              if False, the longest ORF in the mapped contig will be discovered
    :type reject_faulty_CDS: boolean
    :return: None
    :rtype:
    '''
    curr_gene = None
    curr_strand = None
    for line in in_file:
        fields = line.strip().split('\t')
        if len(fields) != 9:
            continue
        attr_fields = fields[8].split(';')
        attrs = parse_gff_attributes(fields[8])
        fields[8] = attrs
        rec = GFF3Record(*fields)
        if rec.has_attribute('sequence'):
            rec.add_attribute('Name',  rec.get_attribute('sequence'))
            rec.remove_attribute('sequence')
        if rec.getType() == 'gene':
            if curr_gene and curr_strand == '.':
                raise ValueError('Ambiguous orientation in ' + curr_gene.get_ID())
            if curr_gene:
                output_curr_gene(curr_gene,  curr_strand,  output,  dna_index)
                # Start a new gene
            if rec.has_attribute('gene_id') and int(rec.get_attribute('gene_id')) > 1:
                raise ValueError('Multiple hits for query' )
            curr_gene = initialize_gene(rec)
            mRNA = curr_gene.get_transcripts()[0]
            introns = []
            curr_intron = None
            curr_strand = '.'
            d_a = []
        elif curr_gene:
            if rec.getType() == 'exon':
                exon = GFF3Exon.fromRecord(rec)
                exon.set_name(curr_gene.get_name())
                if mRNA:
                    mRNA.add_exon(exon)
                d_a = []
            elif rec.getType() == 'start_codon':
                if mRNA:
                    mRNA.add_start_codon(rec)
            elif rec.getType() == 'stop_codon':
                if mRNA:
                    mRNA.add_stop_codon(rec)
            elif rec.getType() in ['splice3',  'splice5']:
                ss = str(dna_index[rec.get_seqID()][rec.get_start()-1:rec.get_end()].seq).upper()
                if rec.getType() == 'splice3':
                    d_a.append(ss)
                else:
                    d_a.insert(0,  ss)
                if curr_intron:
                    if curr_intron.get_end() < rec.get_end():
                        curr_intron.set_end(rec.get_end())
                    else:
                        curr_intron.set_start(rec.get_start())
                else:
                    curr_intron = rec
                if len(d_a) == 2:
                    if d_a in [['GT', 'AG'],  ['GC',  'AG'],  ['AT',  'AC'],  ['AG',  'GT'],  ['AG',  'GC'],  ['AC',  'AT']]:
                        strand = '+'
                    elif d_a in [['CT', 'AC'],  ['CT',  'GC'],  ['GT',  'AT'], ['AC', 'CT'], ['GC', 'CT'],  ['AT',  'GT']]:
                        strand = '-'
                    else:
                        raise ValueError('Unrecognized donor-acceptor pair: %s in %s' % (str(d_a), curr_gene.get_ID()))
                    curr_intron.set_strand(strand)
                    introns.append(curr_intron)
                    curr_intron = None
                    d_a = []
            elif rec.getType() == 'similarity': # alignment summary line
                segments = []
                alignment = []
                for a_f in attr_fields:
                    if a_f.strip().startswith('Align'):
                        align_fields = a_f.strip().split()
                        alignment.append([int(f) for f in align_fields[1:]])
                        seg_start = int(align_fields[2])
                        seg_len = int(align_fields[3])
                        seg_end = seg_start + seg_len
                        if segments and segments[-1][1] == seg_start:
                            segments[-1][1] = seg_end
                        else:
                            segments.append([seg_start,  seg_end])
                if len(segments) < 1:
                    raise ValueError('No alignment information for ' + curr_gene.get_ID() )
                    # Check that alignment is continuous and includes start and end of contig CDS
                header_list = header_dict[curr_gene.getID()]
                if len(header_list) == 0:
                    raise ValueError('No header found for gene ' + curr_gene.getID() )
                header_list.sort()
                cutpoints = []
                if len(header_list) > 1 :
                    # if header_list has more than one entry, it means that pieces of the contig align in different places
                    # check that segments don't overlap
                    for seg1,  seg2 in zip(header_list[:-1],  header_list[1:]):
                        if max(seg1) > min(seg2):
                            raise ValueError('Overlapping hits to ' + curr_gene.getID())
                        cutpoints.append((max(seg1) + min(seg2)) / 2)
                cutpoints.append(segments[-1][1])
                exons = mRNA.get_exons()
                exon_index = 0
                curr_strand = introns[0].get_strand() if introns else '+'
                seg_index = 0
                cutpoint = cutpoints.pop(0)
                for header in header_list:
                    CDS_start = min(header)
                    CDS_stop = max(header)
                    coord_adjustment = 0
                    transcript_len = 1
                    while segments[seg_index][1]  < CDS_start and seg_index < len(segments) - 1 :
                        transcript_len += segments[seg_index][1] - segments[seg_index][0]
                        seg_index += 1
                    if segments[seg_index][0] > CDS_start:
                        raise ValueError('Alignment missing start of ' + curr_gene.get_ID() )
#                    if segments[seg_index][0] > transcript_len:
#                        coord_adjustment = segments[seg_index][0] - transcript_len
#                    while segments[seg_index][0] < CDS_stop and seg_index < len(segments) - 1 :
#                        seg_index += 1
                    g_start = mRNA.get_CDS_start()
                    g_stop = mRNA.get_CDS_stop()
                    if segments[seg_index][1] <  CDS_stop :
                        if seg_index + 1 >= len(segments):
                            raise ValueError('Alignment missing end of ' + curr_gene.get_ID() )
                        else:
                            raise ValueError('Discontinuous alignment for ' + curr_gene.get_ID() )
                    for genomic_coord, transcript_coord, alen in alignment:
                        if transcript_coord <= CDS_start < transcript_coord + alen :
                            if mRNA.get_strand() == '-':
                                g_start = genomic_coord - (CDS_start - transcript_coord) - 1
                            else:
                                g_start = genomic_coord + (CDS_start - transcript_coord)
                        if transcript_coord <= CDS_stop < transcript_coord + alen :
                            break
                    if mRNA.get_strand() == '-':
                        g_stop = genomic_coord - (CDS_stop - transcript_coord) - 1
                    else:
                        g_stop = genomic_coord + (CDS_stop - transcript_coord)

                    mRNA.set_CDS_start(g_start)
                    mRNA.set_CDS_stop(g_stop)
                    if reject_faulty_CDS:
                        # this will raise an exception if the CDS is not a valid protein-coding ORF
                        transcript_seq = mRNA.extract_CDS().get_coding_sequence(dna_index).seq
                        protein = transcript_seq.translate(cds=True)
                    transcript_len = 0
                    while exon_index < len(exons) - 1:
                        if transcript_len + len(exons[exon_index]) > cutpoint or introns[exon_index].get_strand() != curr_strand:
                            # need to split gene; choose the split location
                            if reject_faulty_CDS:
                                raise ValueError('Contig contains more than one transcript')
                            if transcript_len + len(exons[exon_index]) > cutpoint:
                                split_point = min(cutpoint, segments[seg_index][1] )
                            else:
                                # middle of current exon
                                split_point = transcript_len + len(exons[exon_index]) / 2
                            genomic_cutpoint = exons[exon_index].get_start() + split_point - transcript_len
                            if curr_gene:
                                left_gene,  right_gene = split_gene(curr_gene,  genomic_cutpoint)
                                if introns[exon_index].get_strand() != curr_strand:
                                    if curr_strand == '+':
                                        right_gene.set_strand(introns[exon_index].get_strand())
                                    else:
                                        left_gene.set_strand(introns[exon_index].get_strand())
                                for gene in [left_gene, right_gene]:
                                    t = gene.get_transcripts()[0]
                                    if gene.get_strand() == '-':
                                        t.set_CDS_start(t.get_end())
                                        t.set_CDS_stop(t.get_start())
                                    else:
                                        t.set_CDS_start(t.get_start())
                                        t.set_CDS_stop(t.get_end())
                                if curr_strand == '-':
                                    output_curr_gene(right_gene,  curr_strand,  output,  dna_index)
                                    curr_gene = left_gene
                                else:
                                    output_curr_gene(left_gene,  curr_strand,  output,  dna_index)
                                    curr_gene = right_gene
                                if cutpoints:
                                    cutpoint = cutpoints.pop(0)
                                if introns[exon_index].get_strand() != curr_strand:
                                    curr_strand = introns[exon_index].get_strand()
                                    curr_gene.set_strand(curr_strand)
                            break
                        else:
                            transcript_len += len(exons[exon_index])
                            exon_index += 1
                    else: # while loop terminated without any break
                        output_curr_gene(curr_gene,  curr_strand,  output,  dna_index)
                        curr_gene = None
    if curr_gene:
        output_curr_gene(curr_gene,  curr_strand,  output,  dna_index)

if __name__ == '__main__':
    in_file, output, dna_index, headers,reject_faulty_CDS, stderr = setup()
    header_dict = load_headers(headers)
    do_fix_exonerate_gff(in_file, output, dna_index, header_dict,reject_faulty_CDS)
    output.close()
#    print >> stderr, sys.argv[0],  'done.'
