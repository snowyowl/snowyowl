#!/usr/bin/env perl

my ($SO, $AUGUSTUS);

BEGIN {
  $SO = $ENV{'SNOWYOWL'} || 
    die "SNOWYOWL is not a defined environment variable\n";
  $AUGUSTUS = $ENV{'AUGUSTUS_CONFIG_PATH'} || 
    die "AUGUSTUS_CONFIG_PATH is not a defined environment variable\n";
}

use strict;
use warnings;
use Getopt::Long;

my (
  $project_name, $project_dir, $blastout, $transcripts_renamed_fasta,
  $intron_min, $intron_max, $cmd, $log_file
);

GetOptions(
  "p=s"		=> \$project_name,
  "pdir=s"	=> \$project_dir,
  "l=s"     => \$log_file,
  "trf=s"   => \$transcripts_renamed_fasta,
  "btx=s"	=> \$blastout,
  "imin=i"	=> \$intron_min,
  "imax=i"	=> \$intron_max, 
);

($project_name && $project_dir && $transcripts_renamed_fasta && $blastout && $intron_min && $intron_max && $log_file)
  or die "Finish creating contig training set from assembled RNA-seq reads.\n\nUsage: $0 OPTIONS
  -p		  Project name
  -pdir		  Project directory
  -l          Log file name
  -trf        Renamed transcripts file
  -btx 		  Blastx output file
  -imin 	  Smallest allowed intron during alignment of transcripts to genome
  -imax 	  Largest allowed intron during alignment of transcripts to genome
  (All flags are mandatory and must be defined)\n";

open(LOG, ">>$log_file");

if (-d "$project_dir") {
  print LOG "SnowyOwl: Project exists, result files stored under project directory $project_dir/Contigs\n";
}
else {
  die "SnowyOwl ERROR: Project does not exist. Use make_SO_project to create a project first\n";
}

my $success_flag = "$project_dir/Contigs/make_Contig_training.success";

#my $best_blastx = $blastout ;
my $best_blastx = $blastout . ".best" ;
print LOG "\n-> Selecting best hits to each target protein\n";
$cmd = "$SO/bin/select_best_hits.sh $blastout $best_blastx";
print LOG "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: select best blastx hits failed!"
};


print LOG "\n-> Extracting IDs of transcripts with hits\n";
$cmd = "cut -f1 $best_blastx > $project_dir/Contigs/hit_ids.txt";
print LOG "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: cut -f1 $best_blastx failed!"
};

print LOG "\n-> Collapsing each transcript sequence into one line\n";
$cmd = "perl $SO/bin/make_one_line.pl $transcripts_renamed_fasta > $project_dir/Contigs/assembled_transcript_contigs.oneline";
print LOG "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: make_one_line.pl failed!"
};

print LOG "\n-> Extracting transcript sequences with hits and pasting hit info\n";
$cmd = "perl $SO/bin/rgrep.pl $project_dir/Contigs/hit_ids.txt $project_dir/Contigs/assembled_transcript_contigs.oneline | paste - $best_blastx > $project_dir/Contigs/transcripts_hits.txt";
print LOG "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: Extracting transcript sequences failed!"
};

print LOG "\n-> Building full-length CDS\n";
$cmd = "python $SO/bin/full_length_cds.py -i $project_dir/Contigs/transcripts_hits.txt -o $project_dir/Contigs/training_cds.fa --headers $project_dir/Contigs/training_cds.headers.txt";
print LOG "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: full_length_cds.pl failed!"
};

print LOG "\n-> Using exonerate to align contigs to genome. Contigs that do not align are not used for training Augustus\n";
$cmd = "exonerate  --model est2genome --bestn 1 --minintron $intron_min --maxintron $intron_max --geneseed 250 -D 5000 -M 5000 --showtargetgff yes --showalignment no --showvulgar no $project_dir/Contigs/training_cds.fa $project_dir/Sequences/Genome.fasta > $project_dir/Contigs/training_cds.e2g.gff";
print LOG "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: exonerate failed!"
};

print LOG "\n-> Converting exonerate GFF file to GFF3\n";
$cmd = "python $SO/bin/fix_exonerate_gff.py -s -g $project_dir/Sequences/Genome.fasta -i $project_dir/Contigs/training_cds.e2g.gff -o $project_dir/Contigs/training_cds.e2g.gff3 --headers $project_dir/Contigs/training_cds.headers.txt --reject_faulty_CDS -l $log_file";
print LOG "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: fix_exonerate_gff.py failed!"
};

print LOG "\n-> Changing \"exon\" to \"CDS\"\n";
$cmd = "sed -e 's/exon/CDS/g' $project_dir/Contigs/training_cds.e2g.gff3 > $project_dir/Contigs/training_cds.e2g.CDS.gff3";
print LOG "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: Changing \"exon\" to \"CDS\" failed!"
};

print LOG "\n-> Trimming off UTRs\n";
$cmd = "python $SO/bin/trim_gene_to_CDS.py -i $project_dir/Contigs/training_cds.e2g.CDS.gff3 -o $project_dir/Contigs/training_cds.CDS.gff3";
print LOG "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: Trimming off UTRs failed!"
};


my $count0 = `grep -c '^>' $project_dir/Contigs/training_cds.fa`;
chomp $count0;
my $count1 = `grep -c '	gene	' $project_dir/Contigs/training_cds.CDS.gff3`;
chomp $count1;
print LOG "\n$count1 contigs mapped out of $count0\n";

print LOG "\n-> Converting GFF3 to GenBank file format for Augustus training\n";
$cmd = "$AUGUSTUS/../scripts/gff2gbSmallDNA.pl $project_dir/Contigs/training_cds.CDS.gff3 $project_dir/Sequences/Genome.fasta 100 $project_dir/Contigs/training_cds.e2g.CDS.gb ";
print LOG "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: gff2gbSmallDNA.pl failed!"
};

print LOG "\n-> Training Augustus with $project_dir/Contigs/training_cds.e2g.CDS.gb\n";
$cmd = "etraining --species=$project_name\_Contig $project_dir/Contigs/training_cds.e2g.CDS.gb ";
print LOG "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: etraining failed!"
};

print LOG "\n-> Running Augustus gene predictions\n";
$cmd = "augustus --noInFrameStop=true --uniqueGeneId=true --gff3=on --species=$project_name"."_Contig $project_dir/Sequences/Genome.fasta --outfile=$project_dir/Contigs/Contig_models.gff ";
print LOG "$cmd\n";
system $cmd;
unless ( $? == 0 ) {
  die "SnowyOwl ERROR: $0: Augustus gene prediction failed!"
};

print "\nContig Training Set is complete. Result saved in $project_dir/Contigs/Contig_models.gff \n";
open(SFH,">$success_flag");
close(SFH);
close(LOG);