This directory contains the SnowyOwl gene prediction package.


===========
DIRECTORIES
===========

Projects: 
For each genome data set run through SnowyOwl, a directory for storing all intermediate and final results files is
created under Projects.
Alternatively, the -outdir option can be used to specify a different root directory for result files.


bin:
Contains required scripts and programs.

bin/Merge_bin:
Contains the scoring and merging phase of the pipeline.

========
HARDWARE
========

SnowyOwl is designed to run on multi-processor workstations or servers. At least 3 processors are required, 12 are
recommended, and more will shorten run time. SnowyOwl is not designed for clusters.

24 GB of RAM is adequate for fungal genomes.

SnowyOwl will use temporary disk space approximately equal to the size of the input files, and leave about 200 MB of
output files.

SnowyOwl will optionally use DeCypher TimeLogic boards for accelerated blast searching.

========
SOFTWARE
========

The following program packages, with the indicated or newer versions, are required to run SnowyOwl.
  - UNIX, with both bash and tcsh shells
  - Perl 5.16 (with modules BioPerl 1.6 , Forks::Super 0.63 )
  - Python 2.7 (with modules Biopython 1.59, pysam 0.6, paramiko 1.7.7.1)
  - Augustus 2.5.5
  - GeneMark-ES 2.3e
  - NCBI Blast+ 2.2.25
  - Exonerate
  - Blat
  - samtools
  - tabix
  - Cd-hit

SnowyOwl uses 1 or 2 protein databases for Blast searching. During development we have used the Uniprot/Swissprot
database for blastx searching and the NCBI Refseq Fungi database for blastp searching.

Perl is assumed to be installed in /usr/bin; if Perl has been installed in another location (e.g. /usr/local/bin),
a symbolic link from /usr/bin/perl to the actual location should be created.

Python should be on the system PATH.

The locations of the other program and data files can be set in the CONFIG file or given on the command line.

==================
CONFIGURATION FILE
==================

The values set in the file "CONFIG" are used as defaults. Any of these can be left empty and set on the command
line using the same name. If a name is set both in CONFIG and on the command line, the value given on the command line
overrides. If a required value (indicated by "required" on the comment line in CONFIG) is set neither in CONFIG nor on
the command line, SnowyOwl will complain and exit.

In place of the default CONFIG file, you can specify a personalized configuration file with the -c option on the
command line.

=================
RUNNING SNOWY OWL
=================

There are five required arguments to run SnowyOwl, and many more optional arguments. To run SnowyOwl, type:

snowyowl.pl <REQUIRED ARGUMENTS> [OPTIONS]

where <REQUIRED ARGUMENTS> is the list of five required arguments, and [OPTIONS] is the list of optional flags and
arguments. Each argument must be preceded by a flag, and the order of the arguments does not matter.
Note that flags for required arguments are single letters (-p, -g, -r, -t, -m), whereas most optional ones are longer.

The recognized flags and arguments are as follows:

<REQUIRED ARGUMENTS>
  -p Project_name
	Name of the project (usually a species abbreviation: e.g. Rhipu1)
  -g Genome_sequence_file
	FASTA file of sequences of the genome
  -r RNA-Seq_reads_file
	FASTA or FASTQ file of RNA-Seq reads for Augustus hinting
  -t Transcripts_file
	FASTA file of transcript contigs assembled from RNA-Seq reads
  -m RNA-Seq_mapping_directory
	Directory containing tabix-indexed RNA-Seq read mapping junctions and coverage files
	(tuque.coverage.wig.gz, classified.juncs.gz,
	tuque.coverage.wig.gz.tbi, classified.juncs.gz.tbi).
	The required files can be generated from a BAM file of mapped RNA-Seq reads with the included script
	BAM_to_juncs_and_coverage.sh

[OPTIONS]
  -n Masked_genome_sequence_file
        FASTA file of the genome sequence with N's masking regions
        where gene predictions are not wanted (used for Augustus and Genemark)
        [default: no masked genome sequence]
  -label Prefix_label
        Prefix label for names of predicted genes (e.g. Aspni3p5)
        [default: project name as given by -p]
  -outdir Root_directory
        Root directory of output files
        [default: /export/geno_new/git/SnowyOwl/Projects]
  -bdb BLAST_DB
        Protein database to use in contig training
  -hdb Homology_DB
        Protein database to use in homology scoring
  -imin Min_intron_size
        Smallest allowed intron size for alignment of transcripts or RNA-Seq reads to genome
        [default: 50]
  -imax Max_intron_size
        Largest allowed intron size for alignment of transcripts to genome
        [default: 2000]
  -np   Num_procs
        Maximum number of processes to use
        [default: 12]
  -clean
        Clean up project by removing all project directories
        (-p, and -outdir if previously used, are the only required arguments with -clean)

  -augustus AUGUSTUS_path 	Path to AUGUSTUS (top-level directory)
  -genemark GeneMark_path 	Path to GeneMark
  -blast BLAST_path 	    Path to NCBI BLAST+
  -exonerate Exonerate_path Path to exonerate
  -blat BLAT_path 	        Path to BLAT

[OPTIONS FOR DECYPHER BOARDS]
These options are only applicable if you are using hardware-accelerated Blast searching.
For remote access, you will need an account on a server with TimeLogic boards.
Copy your ssh public key onto the server so that you can login without a password prompt.
Create a subdirectory blast in your TimeLogic server home directory and copy
SnowyOwl/bin/Merge_bin/config/tera-blastp.refseq and tera-blastx into it. Consult the server's
documentation for directions on loading databases.

  -dc
        Use DeCypher boards locally for scoring and merging phase
        [default: do not use DeCypher boards]
  -dcremote
        Use DeCypher boards remotely for scoring and merging phase
        [default: do not use DeCypher boards]
  -dcserver DeCypher_server
        DeCypher server to access DeCypher boards remotely (ignored for local access)
        [default: coe01.ucalgary.ca]
  -dcuser DeCypher_user
        User name on DeCypher server  (ignored for local access)
        [default: the user who ran snowyowl.pl]
  -dcmach DeCypher_machine_name
        DeCypher machine name known to server (ignored for local access)
        [default: coedc06]
  -dcpath DeCypher_path
        DeCypher program path on server (ignored for local access)
        [default: /export/data/programs/decypher/bin]



In a graphical environment, you can use the command SnowyOwl.py to start a GUI that will facilitate setting the required
and optional arguments. Clicking the menu item Run | Execute will launch snowyowl.pl with the selected arguments.

SnowyOwl logs progress and error messages to the file SnowyOwl.log in the output directory. Its subprocesses also produce
more detailed logs. The console output contains messages from third-party programs called by SnowyOwl; this is usually
uninteresting, but may include useful clues when problems arise.

Gene models predicted by SnowyOwl can be found in Results/accepted.gff3.



Questions on the package can be directed to Ian Reid (ian_reid@gene.concordia.ca) at Concordia University's Centre for
Structural and Functional Genomics.
